Appendix 4: Top 1000 SSD Models
===============================

See more info on reliability test in the [README](https://github.com/linuxhw/SMART).

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | Rating |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Kingston  | SH100S3120G        | 120 GB | 2       | 1711  | 0     | 4.69   |
| OCZ       | VERTEX2            | 40 GB  | 2       | 1543  | 0     | 4.23   |
| Corsair   | Force 3 SSD        | 180 GB | 4       | 1926  | 1     | 4.19   |
| Mushkin   | MKNSSDCR240GB      | 240 GB | 1       | 1513  | 0     | 4.15   |
| Corsair   | Force 3 SSD        | 240 GB | 2       | 1270  | 0     | 3.48   |
| Kingston  | SKC300S37A480G     | 480 GB | 1       | 1231  | 0     | 3.37   |
| Toshiba   | THNS064GG2BNAA     | 64 GB  | 2       | 1215  | 0     | 3.33   |
| Mushkin   | MKNSSDCR120GB      | 120 GB | 1       | 1164  | 0     | 3.19   |
| Samsung   | SSD 850 EVO        | 2 TB   | 1       | 1163  | 0     | 3.19   |
| Samsung   | MZ7WD240HCFV-00003 | 240 GB | 1       | 1154  | 0     | 3.16   |
| OCZ       | VERTEX2 3.5        | 120 GB | 1       | 1040  | 0     | 2.85   |
| SanDisk   | SD7UB2Q512G1122    | 512 GB | 1       | 1017  | 0     | 2.79   |
| Intel     | SSDSC2BA100G3      | 100 GB | 4       | 1128  | 1     | 2.71   |
| Samsung   | SSD 830 Series     | 64 GB  | 4       | 962   | 0     | 2.64   |
| Crucial   | CT120M500SSD3      | 120 GB | 2       | 933   | 8     | 2.54   |
| OCZ       | AGILITY4           | 128 GB | 3       | 922   | 0     | 2.53   |
| Kingston  | SVP100S296G        | 96 GB  | 1       | 918   | 0     | 2.52   |
| Crucial   | M4-CT256M4SSD2     | 256 GB | 8       | 925   | 256   | 2.47   |
| Crucial   | M4-CT128M4SSD1     | 128 GB | 4       | 902   | 0     | 2.47   |
| Samsung   | SSD PM800 2.5"     | 256 GB | 1       | 871   | 0     | 2.39   |
| Kingston  | SE50S3480G         | 480 GB | 2       | 859   | 0     | 2.35   |
| Apple     | SSD SM0256F        | 256 GB | 1       | 858   | 0     | 2.35   |
| Corsair   | Force GS           | 180 GB | 2       | 854   | 0     | 2.34   |
| Samsung   | MZMPC032HBCD-000D1 | 32 GB  | 1       | 840   | 0     | 2.30   |
| ADATA     | SSD S510           | 120 GB | 4       | 824   | 0     | 2.26   |
| Samsung   | MZ7GE240HMGR-00003 | 240 GB | 1       | 822   | 0     | 2.25   |
| Crucial   | CT250MX200SSD3     | 250 GB | 1       | 816   | 0     | 2.24   |
| Crucial   | M4-CT128M4SSD2     | 128 GB | 20      | 828   | 51    | 2.22   |
| OCZ       | VERTEX PLUS R2     | 61 GB  | 2       | 802   | 0     | 2.20   |
| OCZ       | DENRSTE251M45-0... | 100 GB | 1       | 801   | 0     | 2.20   |
| OCZ       | VERTEX PLUS R2     | 247 GB | 1       | 797   | 0     | 2.18   |
| Intel     | SSDSA2SH032G1GN    | 32 GB  | 1       | 795   | 0     | 2.18   |
| Toshiba   | THNSNH060GMCT      | 64 GB  | 1       | 787   | 0     | 2.16   |
| Micron    | MTFDDAT128MAM-1J2  | 128 GB | 1       | 783   | 0     | 2.15   |
| Corsair   | Force GS           | 90 GB  | 1       | 776   | 0     | 2.13   |
| Samsung   | SSD 830 Series     | 512 GB | 2       | 769   | 0     | 2.11   |
| Kingston  | SVP200S37A480G     | 480 GB | 1       | 765   | 0     | 2.10   |
| Toshiba   | THNSNJ120PCSZ      | 120 GB | 2       | 758   | 0     | 2.08   |
| Lite-On   | LCS-256L9S-11 2... | 256 GB | 1       | 739   | 0     | 2.02   |
| SanDisk   | SD5SE2128G1002E    | 128 GB | 2       | 729   | 0     | 2.00   |
| Corsair   | Force 3 SSD        | 120 GB | 7       | 716   | 0     | 1.96   |
| OCZ       | NOCTI              | 64 GB  | 1       | 704   | 0     | 1.93   |
| Crucial   | M4-CT064M4SSD2     | 64 GB  | 9       | 702   | 0     | 1.93   |
| Kingston  | SVP100S2128G       | 128 GB | 1       | 698   | 0     | 1.91   |
| Kingston  | SVP200S390G        | 90 GB  | 1       | 686   | 0     | 1.88   |
| Corsair   | Force 3 SSD        | 64 GB  | 9       | 686   | 0     | 1.88   |
| Kingston  | SNVP325S2128GB     | 128 GB | 1       | 674   | 0     | 1.85   |
| Kingston  | RBU-SNS8152S3128GF | 128 GB | 1       | 665   | 0     | 1.82   |
| Corsair   | Force GT           | 120 GB | 8       | 798   | 1     | 1.82   |
| OCZ       | VERTEX2            | 64 GB  | 5       | 653   | 0     | 1.79   |
| Intel     | SSDSC2MH120A2      | 120 GB | 1       | 646   | 0     | 1.77   |
| Kingston  | SV200S364G         | 64 GB  | 2       | 645   | 0     | 1.77   |
| Samsung   | SSD 840 EVO 120... | 120 GB | 1       | 641   | 0     | 1.76   |
| OCZ       | VECTOR180          | 240 GB | 2       | 641   | 0     | 1.76   |
| Kingston  | SVP200S37A240G     | 240 GB | 1       | 623   | 0     | 1.71   |
| OCZ       | VERTEX2            | 120 GB | 4       | 623   | 0     | 1.71   |
| Intel     | SSDSA2M040G2GC     | 40 GB  | 2       | 1007  | 3     | 1.69   |
| Samsung   | MZMPA128HMFU-00000 | 128 GB | 1       | 616   | 0     | 1.69   |
| Toshiba   | THNSNH256GMCT      | 256 GB | 1       | 612   | 0     | 1.68   |
| Samsung   | SSD 850 EVO M.2    | 120 GB | 2       | 607   | 0     | 1.66   |
| Samsung   | SSD 850 PRO        | 1 TB   | 3       | 606   | 0     | 1.66   |
| Intel     | SSDSC2CT080A4      | 80 GB  | 3       | 600   | 0     | 1.64   |
| Corsair   | Force 3 SSD        | 480 GB | 1       | 598   | 0     | 1.64   |
| Corsair   | Force GT           | 180 GB | 2       | 594   | 0     | 1.63   |
| OCZ       | DENCSTE351M16-0... | 240 GB | 1       | 585   | 0     | 1.60   |
| OCZ       | VERTEX2            | 115 GB | 1       | 581   | 0     | 1.59   |
| Kingston  | SVP200S360G        | 64 GB  | 4       | 572   | 0     | 1.57   |
| OCZ       | AGILITY4           | 64 GB  | 1       | 567   | 0     | 1.56   |
| OCZ       | REVODRIVE3         | 64 GB  | 4       | 564   | 0     | 1.55   |
| Samsung   | MZMPC128HBFU-000L1 | 128 GB | 1       | 559   | 0     | 1.53   |
| Patriot   | Torch LE           | 240 GB | 1       | 558   | 0     | 1.53   |
| OCZ       | AGILITY3           | 120 GB | 25      | 652   | 111   | 1.53   |
| Samsung   | MZ7TE512HMHP-000L2 | 512 GB | 2       | 552   | 0     | 1.51   |
| Toshiba   | THNSNH128GCST      | 128 GB | 1       | 549   | 0     | 1.51   |
| SanDisk   | SD6SB1M256G1002    | 256 GB | 1       | 548   | 0     | 1.50   |
| Kingston  | SMS100S264G        | 64 GB  | 2       | 547   | 0     | 1.50   |
| SK hynix  | SC300 SATA         | 512 GB | 1       | 546   | 0     | 1.50   |
| Apple     | SSD SM1024F        | 1 TB   | 1       | 545   | 0     | 1.49   |
| Samsung   | MZ7PC128HAFU-000   | 128 GB | 1       | 545   | 0     | 1.49   |
| OCZ       | VERTEX2 3.5        | 115 GB | 1       | 541   | 0     | 1.48   |
| SK hynix  | SC311 SATA         | 256 GB | 2       | 541   | 0     | 1.48   |
| Toshiba   | THNSFC256GAMJ      | 256 GB | 1       | 539   | 0     | 1.48   |
| Samsung   | MZHPV512HDGL-00000 | 512 GB | 1       | 536   | 0     | 1.47   |
| Samsung   | SSD 840 PRO Series | 128 GB | 16      | 579   | 4     | 1.46   |
| Kingston  | SH100S3240G        | 240 GB | 2       | 1170  | 2     | 1.42   |
| OCZ       | D2RSTK251E19-0200  | 200 GB | 1       | 511   | 0     | 1.40   |
| Samsung   | MZ7PA128HMCD-010L1 | 128 GB | 1       | 504   | 0     | 1.38   |
| OCZ       | D2RSTK251E14-0400  | 400 GB | 1       | 502   | 0     | 1.38   |
| Corsair   | Force GT           | 90 GB  | 4       | 610   | 1     | 1.36   |
| Transcend | TS128GSSD320       | 128 GB | 1       | 493   | 0     | 1.35   |
| Samsung   | SSD 830 Series     | 256 GB | 7       | 559   | 145   | 1.33   |
| SPCC      | SSD110             | 64 GB  | 4       | 482   | 0     | 1.32   |
| OCZ       | VERTEX3            | 64 GB  | 24      | 554   | 2     | 1.32   |
| Intel     | SSDSC2BW240A3L     | 240 GB | 1       | 480   | 0     | 1.32   |
| Toshiba   | Q300 Pro           | 256 GB | 2       | 478   | 0     | 1.31   |
| ADATA     | SP600              | 256 GB | 2       | 477   | 0     | 1.31   |
| Crucial   | CT1050MX300SSD1    | 1 TB   | 2       | 476   | 0     | 1.31   |
| Intel     | SSDSA2CW080G3      | 80 GB  | 5       | 475   | 0     | 1.30   |
| SanDisk   | SDSSDH240GG25      | 240 GB | 1       | 473   | 0     | 1.30   |
| Toshiba   | THNSNF128GMCS      | 128 GB | 2       | 471   | 0     | 1.29   |
| Verbatim  | SATA-III SSD       | 128 GB | 1       | 470   | 0     | 1.29   |
| KingSpec  | SPK-SF12-M120      | 120 GB | 1       | 468   | 0     | 1.28   |
| Toshiba   | THNSNH060GCST      | 64 GB  | 1       | 467   | 0     | 1.28   |
| Plextor   | PX-64M2S           | 64 GB  | 2       | 467   | 0     | 1.28   |
| Intel     | SSDSA2CW120G3      | 120 GB | 6       | 466   | 0     | 1.28   |
| ADATA     | SSD S396           | 32 GB  | 1       | 462   | 0     | 1.27   |
| OCZ       | AGILITY3           | 64 GB  | 24      | 520   | 2     | 1.25   |
| Corsair   | Performance Pro    | 128 GB | 1       | 450   | 0     | 1.23   |
| Samsung   | SSD 840 PRO Series | 512 GB | 4       | 570   | 1     | 1.23   |
| Plextor   | PX-128M2S          | 128 GB | 1       | 447   | 0     | 1.23   |
| Foxline   | FLDMMS128G         | 128 GB | 1       | 443   | 0     | 1.22   |
| OCZ       | VERTEX2            | 80 GB  | 1       | 442   | 0     | 1.21   |
| Corsair   | Force GT           | 64 GB  | 5       | 690   | 7     | 1.21   |
| Kingston  | SVP200S37A120G     | 120 GB | 7       | 502   | 154   | 1.20   |
| OCZ       | REVODRIVE X2       | 25 GB  | 4       | 435   | 0     | 1.19   |
| SanDisk   | SD8SN8U256G1122    | 256 GB | 1       | 434   | 0     | 1.19   |
| OCZ       | VERTEX3            | 120 GB | 46      | 637   | 38    | 1.19   |
| OCZ       | VERTEX4            | 128 GB | 56      | 476   | 1     | 1.19   |
| Phison    | SSDS30256XQC800... | 240 GB | 1       | 433   | 0     | 1.19   |
| Kingston  | SV200S3128G        | 128 GB | 6       | 432   | 0     | 1.19   |
| Samsung   | SSD 850 PRO        | 512 GB | 12      | 498   | 87    | 1.18   |
| ADATA     | SSD S511           | 120 GB | 1       | 430   | 0     | 1.18   |
| Samsung   | MZ7TE256HMHP-00000 | 256 GB | 1       | 427   | 0     | 1.17   |
| Samsung   | SSD 850 EVO        | 1 TB   | 18      | 554   | 1     | 1.17   |
| Kingston  | SKC380S360G        | 64 GB  | 1       | 420   | 0     | 1.15   |
| Kingston  | SMS200S3240G       | 240 GB | 1       | 419   | 0     | 1.15   |
| OCZ       | SOLID3             | 120 GB | 1       | 418   | 0     | 1.15   |
| Samsung   | SSD PM830 2.5" 7mm | 256 GB | 2       | 417   | 0     | 1.14   |
| Samsung   | SSD 840 EVO        | 250 GB | 31      | 416   | 33    | 1.13   |
| SanDisk   | SD6SB2M128G1022I   | 128 GB | 1       | 412   | 0     | 1.13   |
| Toshiba   | Q300 Pro           | 512 GB | 1       | 411   | 0     | 1.13   |
| Samsung   | MMCRE28G5MXP-0VBH1 | 128 GB | 1       | 410   | 0     | 1.12   |
| OCZ       | AGILITY3           | 240 GB | 6       | 618   | 1     | 1.12   |
| Plextor   | PX-512M7VC         | 512 GB | 2       | 404   | 0     | 1.11   |
| SanDisk   | SDSSDH2256G        | 256 GB | 1       | 403   | 0     | 1.10   |
| OCZ       | VERTEX3            | 128 GB | 2       | 402   | 0     | 1.10   |
| Toshiba   | THNSNH060GBST      | 64 GB  | 2       | 400   | 0     | 1.10   |
| Samsung   | SSD 840 EVO        | 752 GB | 2       | 399   | 0     | 1.09   |
| SanDisk   | SDSSDXP240G        | 240 GB | 2       | 398   | 0     | 1.09   |
| Samsung   | MZNLN128HCGR-00000 | 128 GB | 1       | 396   | 0     | 1.09   |
| Micron    | 1100 SATA          | 256 GB | 1       | 784   | 1     | 1.08   |
| OCZ       | VERTEX3            | 240 GB | 7       | 448   | 6     | 1.07   |
| Toshiba   | THNSFJ256GDNU A    | 256 GB | 1       | 390   | 0     | 1.07   |
| Samsung   | SSD PM830 mSATA    | 32 GB  | 7       | 389   | 0     | 1.07   |
| Smartbuy  | SSD                | 64 GB  | 6       | 386   | 0     | 1.06   |
| Kingston  | SNV425S264GB       | 64 GB  | 1       | 386   | 0     | 1.06   |
| Samsung   | MZ7TD256HAFV-000L9 | 256 GB | 1       | 385   | 0     | 1.06   |
| Samsung   | SSD 840 PRO Series | 256 GB | 20      | 385   | 0     | 1.06   |
| Crucial   | CT256MX100SSD1     | 256 GB | 12      | 383   | 0     | 1.05   |
| Samsung   | MMCRE28GFMXP-MVB   | 128 GB | 2       | 383   | 0     | 1.05   |
| Samsung   | MZ7TE128HMGR-00000 | 128 GB | 2       | 382   | 0     | 1.05   |
| Kingston  | SM2280S3G2240G     | 240 GB | 2       | 380   | 0     | 1.04   |
| Samsung   | SSD 840 EVO        | 1 TB   | 4       | 375   | 0     | 1.03   |
| Samsung   | MZMPC032HBCD-00000 | 32 GB  | 5       | 372   | 0     | 1.02   |
| OCZ       | VERTEX3 MI         | 120 GB | 15      | 452   | 134   | 1.02   |
| OCZ       | VERTEX4            | 256 GB | 11      | 542   | 1     | 1.02   |
| Samsung   | MZ7TD256HAFV-000L7 | 256 GB | 2       | 370   | 0     | 1.02   |
| Samsung   | SSD 850 EVO mSATA  | 120 GB | 2       | 369   | 0     | 1.01   |
| Toshiba   | THNSNC128GCSJ      | 128 GB | 1       | 367   | 0     | 1.01   |
| Plextor   | PX-64M3            | 64 GB  | 3       | 668   | 346   | 1.00   |
| OCZ       | VERTEX3            | 90 GB  | 11      | 680   | 7     | 0.99   |
| Samsung   | MZMPC128HBFU-000H1 | 128 GB | 1       | 361   | 0     | 0.99   |
| KingShare | 230120SSD          | 128 GB | 1       | 358   | 0     | 0.98   |
| Kingston  | SH103S3120G        | 120 GB | 36      | 358   | 0     | 0.98   |
| Samsung   | SSD 840 EVO        | 120 GB | 43      | 356   | 0     | 0.98   |
| SanDisk   | SDSSDH2128G        | 128 GB | 1       | 356   | 0     | 0.98   |
| KingFast  | SSD                | 256 GB | 3       | 355   | 0     | 0.97   |
| KingSpec  | KSD-SA25.5-016MJ   | 16 GB  | 1       | 353   | 0     | 0.97   |
| BHT       | WR202HH032G E70... | 31 GB  | 1       | 351   | 0     | 0.96   |
| SPCC      | SSD110             | 120 GB | 4       | 459   | 254   | 0.96   |
| Samsung   | SSD 850 PRO        | 128 GB | 16      | 349   | 0     | 0.96   |
| Plextor   | PX-128M3           | 128 GB | 2       | 349   | 0     | 0.96   |
| Toshiba   | THNSNX024GMNT      | 24 GB  | 2       | 346   | 0     | 0.95   |
| Samsung   | SSD 840 EVO        | 500 GB | 12      | 346   | 0     | 0.95   |
| Kingston  | SVP200S37A60G      | 64 GB  | 6       | 345   | 0     | 0.95   |
| Advantech | SQF-S25M4-32G-S9E  | 32 GB  | 1       | 345   | 0     | 0.95   |
| SanDisk   | SDSSDHII480G       | 480 GB | 4       | 344   | 0     | 0.95   |
| Samsung   | MZ7TD128HAFV-000L1 | 128 GB | 1       | 340   | 0     | 0.93   |
| Intel     | SSDSCKJW120H6      | 120 GB | 1       | 339   | 0     | 0.93   |
| Toshiba   | THNS064GE4BBDC     | 64 GB  | 1       | 338   | 0     | 0.93   |
| China     | SATA SSD           | 20 GB  | 7       | 383   | 1     | 0.93   |
| OCZ       | VERTEX4            | 64 GB  | 7       | 338   | 1     | 0.92   |
| KingFast  | SSD                | 32 GB  | 1       | 332   | 0     | 0.91   |
| OCZ       | VERTEX PLUS R2     | 128 GB | 2       | 331   | 0     | 0.91   |
| Intel     | SSDSA2M080G2GC     | 80 GB  | 5       | 716   | 3     | 0.91   |
| Crucial   | C300-CTFDDAC128MAG | 128 GB | 3       | 330   | 0     | 0.91   |
| Samsung   | SSD 830 Series     | 128 GB | 7       | 329   | 0     | 0.90   |
| Samsung   | MZMPC032HBCD-000H1 | 32 GB  | 5       | 325   | 0     | 0.89   |
| Samsung   | MZMTD256HAGM-000L1 | 256 GB | 2       | 325   | 0     | 0.89   |
| Apple     | SSD TS128C         | 121 GB | 2       | 323   | 0     | 0.89   |
| Apple     | SSD SM0512G        | 500 GB | 1       | 323   | 0     | 0.89   |
| Intel     | SSDSC2CT240A4      | 240 GB | 5       | 321   | 0     | 0.88   |
| OCZ       | VERTEX3 LP         | 64 GB  | 1       | 320   | 0     | 0.88   |
| Intel     | SSDSA2BW160G3L     | 160 GB | 3       | 320   | 0     | 0.88   |
| PNY       | SSD2SC240G1CS17... | 240 GB | 1       | 317   | 0     | 0.87   |
| SanDisk   | SDSSDHP064G        | 64 GB  | 8       | 316   | 0     | 0.87   |
| Crucial   | CT275MX300SSD4     | 275 GB | 1       | 316   | 0     | 0.87   |
| Intel     | SSDSC2BA200G4      | 200 GB | 5       | 313   | 0     | 0.86   |
| SanDisk   | Ultra II           | 960 GB | 3       | 502   | 1     | 0.86   |
| Goodram   | C50                | 64 GB  | 1       | 313   | 0     | 0.86   |
| OCZ       | AGILITY3           | 128 GB | 2       | 313   | 0     | 0.86   |
| Crucial   | C300-CTFDDAC064MAG | 64 GB  | 1       | 307   | 0     | 0.84   |
| Kingston  | SVP200S3240G       | 240 GB | 2       | 307   | 0     | 0.84   |
| Corsair   | Force 3 SSD        | 90 GB  | 4       | 307   | 0     | 0.84   |
| Toshiba   | THNSNC128GBSJ      | 128 GB | 1       | 303   | 0     | 0.83   |
| Samsung   | SSD PM830 2.5" 7mm | 128 GB | 1       | 302   | 0     | 0.83   |
| OCZ       | AGILITY2           | 64 GB  | 2       | 302   | 0     | 0.83   |
| SanDisk   | SDSSDX120GG25      | 120 GB | 4       | 301   | 0     | 0.83   |
| Samsung   | MZ7TD128HAFV-00000 | 128 GB | 1       | 300   | 0     | 0.82   |
| OCZ       | SOLID3             | 64 GB  | 2       | 558   | 7     | 0.82   |
| Mushkin   | MKNSSDAT240GB-DX   | 240 GB | 1       | 299   | 0     | 0.82   |
| Plextor   | PX-256M5Pro        | 256 GB | 4       | 298   | 0     | 0.82   |
| SanDisk   | SSD U100           | 128 GB | 6       | 390   | 33    | 0.81   |
| SanDisk   | X300 2.5 7MM       | 256 GB | 1       | 297   | 0     | 0.81   |
| Ramaxel   | RDM-II XM020C024G  | 24 GB  | 2       | 295   | 0     | 0.81   |
| Kingston  | SNVP325S2256GB     | 256 GB | 1       | 293   | 0     | 0.80   |
| OCZ       | VECTOR             | 128 GB | 3       | 296   | 5     | 0.80   |
| Samsung   | MZMPA064HMDR-00000 | 64 GB  | 1       | 290   | 0     | 0.79   |
| Intel     | SSDSC2BW180A4      | 180 GB | 7       | 370   | 2     | 0.79   |
| OCZ       | AGILITY3           | 90 GB  | 3       | 476   | 1     | 0.79   |
| OCZ       | CACHE-SYNAPSE      | 32 GB  | 1       | 286   | 0     | 0.79   |
| Kingston  | SH103S3240G        | 240 GB | 10      | 467   | 213   | 0.78   |
| Transcend | TS128GSSD340       | 128 GB | 4       | 283   | 0     | 0.78   |
| OCZ       | VERTEX             | 64 GB  | 1       | 280   | 0     | 0.77   |
| Kingston  | SKC300S37A120G     | 120 GB | 12      | 280   | 0     | 0.77   |
| Kingston  | SMS100S232G        | 32 GB  | 1       | 279   | 0     | 0.76   |
| PNY       | CS2211 480GB SSD   | 480 GB | 1       | 277   | 0     | 0.76   |
| ADATA     | SP900              | 256 GB | 8       | 371   | 2     | 0.75   |
| Samsung   | SSD 840 Series     | 250 GB | 8       | 275   | 0     | 0.75   |
| Kingston  | SMS200S360G        | 64 GB  | 8       | 282   | 1     | 0.75   |
| Intel     | SSDSC2BW180A3L     | 180 GB | 3       | 275   | 0     | 0.75   |
| Toshiba   | THNSNJ128G8NU      | 128 GB | 2       | 274   | 0     | 0.75   |
| SanDisk   | SD6SB1M128G1001    | 128 GB | 1       | 271   | 0     | 0.74   |
| SK hynix  | SH920 2.5 7MM      | 256 GB | 1       | 271   | 0     | 0.74   |
| SanDisk   | SDSSDHP128G        | 128 GB | 17      | 285   | 1     | 0.74   |
| Samsung   | MZNLN256HCHP-000L7 | 256 GB | 3       | 270   | 0     | 0.74   |
| Intel     | SSDSC2BP240G4      | 240 GB | 2       | 269   | 0     | 0.74   |
| Goodram   | GOODRAM SSD        | 120 GB | 2       | 269   | 0     | 0.74   |
| SanDisk   | SD8SN8U512G1002    | 512 GB | 3       | 268   | 0     | 0.74   |
| Samsung   | SSD 850 EVO        | 120 GB | 41      | 268   | 0     | 0.74   |
| Goodram   | GOODRAM C40        | 120 GB | 3       | 266   | 0     | 0.73   |
| SanDisk   | iSSD P4            | 8 GB   | 3       | 350   | 1     | 0.73   |
| SK hynix  | HFS120G32TND-N1A2A | 120 GB | 1       | 264   | 0     | 0.73   |
| Kingston  | SKC400S37128G      | 128 GB | 2       | 264   | 0     | 0.72   |
| Crucial   | CT240M500SSD1      | 240 GB | 12      | 475   | 184   | 0.72   |
| Toshiba   | TR150              | 120 GB | 3       | 263   | 0     | 0.72   |
| Apacer    | A7202              | 64 GB  | 1       | 262   | 0     | 0.72   |
| Samsung   | SSD PM830 mSATA    | 128 GB | 1       | 261   | 0     | 0.72   |
| Crucial   | CT500MX200SSD1     | 500 GB | 6       | 260   | 0     | 0.71   |
| Samsung   | MMCQE28GFMUP-MVA   | 128 GB | 2       | 298   | 13    | 0.71   |
| Samsung   | SSD PM810 TM       | 64 GB  | 1       | 257   | 0     | 0.70   |
| SanDisk   | SD6SB1M128G        | 128 GB | 1       | 256   | 0     | 0.70   |
| Corsair   | Force GS           | 240 GB | 3       | 1272  | 6     | 0.70   |
| Corsair   | Force GS           | 128 GB | 14      | 256   | 0     | 0.70   |
| Samsung   | MZ7TE256HMHP-000L7 | 256 GB | 2       | 255   | 0     | 0.70   |
| Kingston  | SKC300S37A60G      | 64 GB  | 12      | 373   | 6     | 0.70   |
| Micron    | 1100 SATA          | 512 GB | 1       | 253   | 0     | 0.69   |
| Samsung   | SSD 840 Series     | 500 GB | 2       | 392   | 1     | 0.69   |
| Crucial   | CT512MX100SSD1     | 512 GB | 6       | 252   | 0     | 0.69   |
| Samsung   | SSD PM800 TH       | 64 GB  | 1       | 251   | 0     | 0.69   |
| OCZ       | TRION150           | 240 GB | 3       | 249   | 0     | 0.68   |
| ADATA     | SX900              | 64 GB  | 3       | 500   | 339   | 0.68   |
| Kingston  | SV300S37A60G       | 64 GB  | 94      | 250   | 1     | 0.68   |
| ADATA     | SP900              | 128 GB | 22      | 319   | 16    | 0.68   |
| Intel     | SSDSA2M080G2HP     | 80 GB  | 1       | 245   | 0     | 0.67   |
| Plextor   | PX-128M7VC         | 128 GB | 5       | 245   | 0     | 0.67   |
| Intel     | SSDSA1MH080G1GN    | 80 GB  | 1       | 244   | 0     | 0.67   |
| Corsair   | CSSD-V64GB2        | 64 GB  | 1       | 243   | 0     | 0.67   |
| SanDisk   | SDSSDP064G         | 64 GB  | 5       | 294   | 205   | 0.66   |
| HP        | VK0240GDJXU        | 240 GB | 1       | 238   | 0     | 0.65   |
| Plextor   | PX-512M5P          | 512 GB | 1       | 238   | 0     | 0.65   |
| Crucial   | M4-CT128M4SSD3     | 128 GB | 1       | 238   | 0     | 0.65   |
| Crucial   | CT240M500SSD3      | 240 GB | 3       | 342   | 11    | 0.65   |
| Samsung   | MZNLF128HCHP-00000 | 128 GB | 2       | 233   | 0     | 0.64   |
| SanDisk   | SSD i100           | 16 GB  | 5       | 233   | 0     | 0.64   |
| Apacer    | AST680S            | 128 GB | 2       | 232   | 0     | 0.64   |
| SanDisk   | SDSSDH31000G       | 1 TB   | 1       | 231   | 0     | 0.64   |
| Patriot   | Blaze              | 240 GB | 2       | 231   | 0     | 0.63   |
| Crucial   | CT120M500SSD1      | 120 GB | 9       | 436   | 8     | 0.63   |
| Kingston  | SM2280S3120G       | 120 GB | 3       | 230   | 0     | 0.63   |
| Toshiba   | A100               | 240 GB | 2       | 230   | 0     | 0.63   |
| SanDisk   | Z400s M.2 2280     | 256 GB | 1       | 229   | 0     | 0.63   |
| Kingston  | SV100S264G         | 64 GB  | 6       | 391   | 4     | 0.63   |
| Samsung   | MZNTY256HDHP-000L7 | 256 GB | 1       | 228   | 0     | 0.63   |
| Samsung   | SSD 850 PRO        | 256 GB | 41      | 227   | 0     | 0.62   |
| Samsung   | SSD 840 Series     | 120 GB | 13      | 239   | 1     | 0.62   |
| OCZ       | OCTANE S2          | 64 GB  | 1       | 224   | 0     | 0.61   |
| Kingston  | SVP200S37A90G      | 90 GB  | 2       | 223   | 0     | 0.61   |
| Crucial   | CT500MX200SSD4     | 500 GB | 2       | 222   | 0     | 0.61   |
| SanDisk   | SD5SG2256G1052E    | 256 GB | 2       | 222   | 0     | 0.61   |
| Kingston  | SUV300S37A480G     | 480 GB | 1       | 222   | 0     | 0.61   |
| SanDisk   | SSD U110           | 64 GB  | 1       | 222   | 0     | 0.61   |
| Team      | L3 SSD             | 120 GB | 2       | 221   | 0     | 0.61   |
| Samsung   | SSD 860 EVO mSATA  | 1 TB   | 1       | 221   | 0     | 0.61   |
| Kingston  | SV100S232G         | 32 GB  | 3       | 219   | 0     | 0.60   |
| Samsung   | MZNTE128HMGR-000SO | 128 GB | 2       | 379   | 773   | 0.60   |
| Samsung   | MMDPE56GFDXP-MVB   | 256 GB | 1       | 218   | 0     | 0.60   |
| Kingston  | SV200S3256G        | 256 GB | 3       | 676   | 3     | 0.59   |
| Samsung   | SSD 850 EVO        | 250 GB | 114     | 215   | 0     | 0.59   |
| Goodram   | GOODRAM C100       | 120 GB | 1       | 214   | 0     | 0.59   |
| OCZ       | TRION100           | 240 GB | 6       | 213   | 0     | 0.59   |
| Kingston  | SV300S37A120G      | 120 GB | 270     | 252   | 22    | 0.58   |
| Samsung   | SSD 850 EVO        | 500 GB | 71      | 212   | 0     | 0.58   |
| Intel     | SSDSC2BB300G4      | 304 GB | 1       | 211   | 0     | 0.58   |
| WDC       | WDS500G1B0B-00AS40 | 500 GB | 1       | 210   | 0     | 0.58   |
| OCZ       | VECTOR150          | 120 GB | 9       | 210   | 0     | 0.58   |
| Toshiba   | THNSNJ128GCST      | 128 GB | 3       | 208   | 0     | 0.57   |
| SanDisk   | X400 M.2 2280      | 256 GB | 2       | 208   | 0     | 0.57   |
| OCZ       | VERTEX2            | 50 GB  | 2       | 309   | 320   | 0.57   |
| Palit     | PH120 SSD          | 120 GB | 1       | 207   | 0     | 0.57   |
| Toshiba   | THNSNH128GBST      | 128 GB | 2       | 207   | 0     | 0.57   |
| SanDisk   | SD8SN8U128G1002    | 128 GB | 1       | 206   | 0     | 0.57   |
| Goodram   | GOODRAM C50        | 64 GB  | 1       | 204   | 0     | 0.56   |
| SPCC      | SSD B29            | 32 GB  | 1       | 204   | 0     | 0.56   |
| ADATA     | SP600              | 128 GB | 3       | 203   | 0     | 0.56   |
| Smartbuy  | m.2 S11-2280S      | 128 GB | 1       | 202   | 0     | 0.56   |
| Toshiba   | THNSNJ128GCSU      | 128 GB | 3       | 202   | 0     | 0.56   |
| SanDisk   | X400 M.2 2280      | 128 GB | 1       | 202   | 0     | 0.56   |
| Samsung   | MZRPA128HMCD-000SO | 64 GB  | 10      | 202   | 0     | 0.56   |
| SanDisk   | SD7TB6S256G1001    | 256 GB | 1       | 200   | 0     | 0.55   |
| SanDisk   | SD5SF2128G1014E    | 128 GB | 2       | 200   | 0     | 0.55   |
| Samsung   | MZ7LN512HMJP-000L7 | 512 GB | 1       | 198   | 0     | 0.54   |
| OCZ       | VERTEX PLUS        | 64 GB  | 1       | 396   | 1     | 0.54   |
| SanDisk   | SD6SF1M128G1022I   | 128 GB | 1       | 396   | 1     | 0.54   |
| Samsung   | MZNTD256HAGL-00000 | 256 GB | 1       | 196   | 0     | 0.54   |
| Samsung   | MZNTY256HDHP-000L2 | 256 GB | 1       | 195   | 0     | 0.54   |
| Samsung   | MZMPC032HBCD-000L1 | 32 GB  | 1       | 195   | 0     | 0.53   |
| Plextor   | PX-128S3G          | 128 GB | 1       | 194   | 0     | 0.53   |
| OCZ       | ARC100             | 480 GB | 2       | 197   | 1     | 0.53   |
| OCZ       | VERTEX4            | 512 GB | 1       | 573   | 2     | 0.52   |
| Kingston  | RBU-SNS8152S312... | 128 GB | 2       | 190   | 0     | 0.52   |
| WDC       | WDS250G1B0A-00H9H0 | 250 GB | 6       | 190   | 0     | 0.52   |
| Kingston  | SS100S216G         | 16 GB  | 1       | 190   | 0     | 0.52   |
| Plextor   | PH6-CE120-G        | 120 GB | 1       | 189   | 0     | 0.52   |
| Toshiba   | THNSNJ256GCSU      | 256 GB | 3       | 188   | 0     | 0.52   |
| Kingston  | SV100S2128G        | 128 GB | 2       | 359   | 2     | 0.52   |
| Smartbuy  | SSD                | 120 GB | 52      | 188   | 0     | 0.52   |
| Samsung   | SG9MSM6D024GPM00   | 22 GB  | 1       | 188   | 0     | 0.52   |
| ADATA     | SP900              | 64 GB  | 12      | 249   | 2     | 0.51   |
| Transcend | TS128GSSD720       | 128 GB | 2       | 187   | 0     | 0.51   |
| Toshiba   | THNSNH128GMCT      | 128 GB | 2       | 186   | 0     | 0.51   |
| SPCC      | SSD162             | 120 GB | 7       | 406   | 436   | 0.51   |
| Smartbuy  | mSata              | 64 GB  | 1       | 186   | 0     | 0.51   |
| Crucial   | M4-CT512M4SSD2     | 512 GB | 1       | 184   | 0     | 0.51   |
| Intel     | SSDSC2CT180A4      | 180 GB | 5       | 184   | 0     | 0.51   |
| Crucial   | CT2000MX500SSD1    | 2 TB   | 1       | 183   | 0     | 0.50   |
| Patriot   | Spark              | 128 GB | 6       | 181   | 0     | 0.50   |
| Plextor   | PX-256M7VC         | 256 GB | 3       | 181   | 0     | 0.50   |
| Transcend | TS128GMSA740       | 128 GB | 1       | 180   | 0     | 0.50   |
| ADATA     | SX900              | 128 GB | 7       | 705   | 584   | 0.49   |
| SPCC      | SSD                | 128 GB | 4       | 179   | 0     | 0.49   |
| Crucial   | CT960M500SSD1      | 960 GB | 4       | 445   | 517   | 0.49   |
| SanDisk   | SDSSDHII120G       | 120 GB | 17      | 178   | 0     | 0.49   |
| ADATA     | SSD S599           | 64 GB  | 1       | 178   | 0     | 0.49   |
| Samsung   | SSD PM800 Serie... | 256 GB | 1       | 178   | 0     | 0.49   |
| SanDisk   | SD7SB3Q256G1002    | 256 GB | 1       | 355   | 1     | 0.49   |
| KingDian  | S400 XT            | 240 GB | 1       | 177   | 0     | 0.49   |
| Smartbuy  | SSD                | 64 GB  | 20      | 175   | 0     | 0.48   |
| PNY       | CS900 120GB SSD    | 120 GB | 1       | 175   | 0     | 0.48   |
| SanDisk   | SDSSDP128G         | 128 GB | 20      | 174   | 1     | 0.48   |
| SK hynix  | HFS256G32TND-N210A | 256 GB | 1       | 172   | 0     | 0.47   |
| Plextor   | PX-128M7VG         | 128 GB | 1       | 171   | 0     | 0.47   |
| China     | SSD                | 64 GB  | 5       | 210   | 1     | 0.47   |
| SanDisk   | SDSSDP256G         | 256 GB | 2       | 169   | 0     | 0.46   |
| Intel     | SSDSA2CT040G3      | 40 GB  | 3       | 167   | 0     | 0.46   |
| Kingston  | SUV400S37480G      | 480 GB | 1       | 164   | 0     | 0.45   |
| Corsair   | Force LE200 SSD    | 120 GB | 1       | 164   | 0     | 0.45   |
| SanDisk   | SD7SB6S256G1122    | 256 GB | 1       | 163   | 0     | 0.45   |
| Kingston  | SM2280S3G2120G     | 120 GB | 2       | 162   | 0     | 0.44   |
| Apacer    | 256GB SATA Flas... | 256 GB | 1       | 162   | 0     | 0.44   |
| Crucial   | M4-CT512M4SSD1     | 512 GB | 1       | 161   | 0     | 0.44   |
| Intel     | SSDSCMMW180A3L     | 180 GB | 1       | 160   | 0     | 0.44   |
| Samsung   | MZNLF128HCHP-00004 | 128 GB | 3       | 160   | 0     | 0.44   |
| Kingston  | SMS200S3120G       | 120 GB | 7       | 167   | 110   | 0.44   |
| Crucial   | CT250MX200SSD1     | 250 GB | 4       | 159   | 0     | 0.44   |
| Patriot   | Flare              | 64 GB  | 1       | 157   | 0     | 0.43   |
| SPCC      | SSD                | 64 GB  | 63      | 169   | 49    | 0.43   |
| Samsung   | MMCRE64GFMPP-MVA   | 64 GB  | 2       | 155   | 0     | 0.43   |
| Plextor   | PX-256S2C          | 256 GB | 2       | 153   | 0     | 0.42   |
| Samsung   | MZNLN128HCGR-000L2 | 128 GB | 1       | 153   | 0     | 0.42   |
| Samsung   | MZNTY128HDHP-000L1 | 128 GB | 1       | 153   | 0     | 0.42   |
| Samsung   | SSD 850 EVO M.2    | 250 GB | 8       | 153   | 0     | 0.42   |
| Samsung   | SSD 840 EVO 500... | 500 GB | 1       | 152   | 0     | 0.42   |
| Patriot   | Blast              | 120 GB | 4       | 152   | 0     | 0.42   |
| SanDisk   | SSD i100           | 24 GB  | 12      | 156   | 3     | 0.41   |
| OCZ       | ARC100             | 120 GB | 8       | 150   | 0     | 0.41   |
| SanDisk   | SDSSDX240GG25      | 240 GB | 1       | 1348  | 8     | 0.41   |
| China     | SATA SSD           | 128 GB | 4       | 149   | 0     | 0.41   |
| Samsung   | SSD 750 EVO        | 120 GB | 23      | 148   | 0     | 0.41   |
| Kingston  | SHSS37A120G        | 120 GB | 6       | 148   | 0     | 0.41   |
| Intel     | SSDMCEAC180B3      | 180 GB | 2       | 148   | 0     | 0.41   |
| Corsair   | Neutron GTX SSD    | 240 GB | 4       | 180   | 74    | 0.41   |
| Smartbuy  | m.2 S10-2280T      | 128 GB | 1       | 147   | 0     | 0.40   |
| SPCC      | SSD                | 120 GB | 90      | 182   | 114   | 0.40   |
| SanDisk   | SDSSDHII240G       | 240 GB | 9       | 146   | 0     | 0.40   |
| Samsung   | SSD 750 EVO        | 500 GB | 3       | 146   | 0     | 0.40   |
| Goodram   | GOODRAM CX100      | 120 GB | 6       | 152   | 2     | 0.40   |
| Samsung   | MZMPA024HMCD-000L1 | 24 GB  | 2       | 198   | 10    | 0.40   |
| Samsung   | MZMTD128HAFV-000L1 | 128 GB | 7       | 168   | 21    | 0.39   |
| Intel     | SSDSC2BB120G4      | 120 GB | 1       | 143   | 0     | 0.39   |
| QUMO      | SSD                | 64 GB  | 1       | 143   | 0     | 0.39   |
| Patriot   | Blast              | 240 GB | 4       | 143   | 0     | 0.39   |
| Corsair   | CSSD-F60GB2        | 64 GB  | 3       | 1108  | 246   | 0.39   |
| KingSpec  | KSD-SA25.5-064MJ   | 64 GB  | 1       | 141   | 0     | 0.39   |
| Intel     | SSDSC2BW240H6      | 240 GB | 3       | 139   | 0     | 0.38   |
| Team      | L3 EVO SSD         | 120 GB | 2       | 139   | 0     | 0.38   |
| ADATA     | SX930              | 120 GB | 1       | 138   | 0     | 0.38   |
| Crucial   | CT750MX300SSD1     | 752 GB | 4       | 137   | 0     | 0.38   |
| Intel     | SSDSC2BB080G4      | 80 GB  | 1       | 135   | 0     | 0.37   |
| SanDisk   | SDSSDHP256G        | 256 GB | 5       | 135   | 0     | 0.37   |
| Kingston  | SV300S37A240G      | 240 GB | 52      | 135   | 1     | 0.37   |
| SK hynix  | HFS128G32MND-3210A | 128 GB | 1       | 135   | 0     | 0.37   |
| Toshiba   | VX500              | 256 GB | 1       | 134   | 0     | 0.37   |
| Transcend | TS256GMTS400       | 256 GB | 3       | 132   | 0     | 0.36   |
| Kingston  | SMS200S330G        | 32 GB  | 1       | 132   | 0     | 0.36   |
| OCZ       | AGILITY4           | 256 GB | 4       | 142   | 61    | 0.36   |
| Toshiba   | THNSNF064GMCS      | 64 GB  | 2       | 129   | 0     | 0.35   |
| SanDisk   | SDSSDXPS240G       | 240 GB | 4       | 136   | 259   | 0.35   |
| ADATA     | SP600              | 32 GB  | 4       | 127   | 0     | 0.35   |
| OCZ       | VECTOR150          | 240 GB | 2       | 127   | 0     | 0.35   |
| Goodram   | GOODRAM            | 120 GB | 17      | 126   | 0     | 0.35   |
| Plextor   | PX-128M5S          | 128 GB | 40      | 128   | 1     | 0.35   |
| Chiprex   | S10T3120GB         | 120 GB | 1       | 126   | 0     | 0.35   |
| Apple     | SSD TS256C         | 256 GB | 2       | 126   | 0     | 0.35   |
| KingFast  | SSD                | 29 GB  | 1       | 125   | 0     | 0.34   |
| Samsung   | SSD 750 EVO        | 250 GB | 15      | 125   | 0     | 0.34   |
| Crucial   | CT256M550SSD1      | 256 GB | 5       | 378   | 7     | 0.34   |
| Kingston  | SUV400S37240G      | 240 GB | 39      | 131   | 27    | 0.34   |
| OCZ       | ARC100             | 240 GB | 5       | 123   | 0     | 0.34   |
| Apple     | SSD SM512E         | 500 GB | 1       | 123   | 0     | 0.34   |
| OCZ       | VERTEX450          | 128 GB | 1       | 121   | 0     | 0.33   |
| ADATA     | SSD S511           | 64 GB  | 3       | 450   | 679   | 0.33   |
| Samsung   | SSD 850 EVO M.2    | 500 GB | 7       | 120   | 0     | 0.33   |
| ADATA     | SP310              | 128 GB | 1       | 120   | 0     | 0.33   |
| KingDian  | S400               | 120 GB | 6       | 120   | 0     | 0.33   |
| Kingston  | RBU-SNS8152S312... | 128 GB | 1       | 120   | 0     | 0.33   |
| ADATA     | SSD SX900 512GB... | 512 GB | 1       | 120   | 0     | 0.33   |
| Kingston  | SHSS37A480G        | 480 GB | 5       | 119   | 0     | 0.33   |
| Apacer    | AS340              | 120 GB | 1       | 119   | 0     | 0.33   |
| Kingston  | SUV300S37A240G     | 240 GB | 7       | 118   | 0     | 0.32   |
| ADATA     | XM13               | 32 GB  | 1       | 116   | 0     | 0.32   |
| Crucial   | CT1024MX200SSD1    | 1 TB   | 1       | 116   | 0     | 0.32   |
| Kingston  | SVP200S3120G       | 120 GB | 4       | 370   | 499   | 0.32   |
| Samsung   | MZMPC128HBFU-000   | 128 GB | 1       | 114   | 0     | 0.31   |
| Corsair   | Force LE SSD       | 120 GB | 2       | 114   | 0     | 0.31   |
| OCZ       | TRION100           | 120 GB | 4       | 141   | 3     | 0.31   |
| OCZ       | D2CSTK181M11-0180  | 180 GB | 3       | 114   | 0     | 0.31   |
| Transcend | TS128GMTS400S      | 128 GB | 1       | 113   | 0     | 0.31   |
| OCZ       | VERTEX2            | 90 GB  | 1       | 113   | 0     | 0.31   |
| Micron    | MTFDDAK512MAM-1K1  | 512 GB | 1       | 113   | 0     | 0.31   |
| Crucial   | CT240BX300SSD1     | 240 GB | 1       | 113   | 0     | 0.31   |
| Toshiba   | Q300               | 120 GB | 2       | 112   | 0     | 0.31   |
| Kingston  | RBUSNS8280S3128GH2 | 128 GB | 2       | 112   | 0     | 0.31   |
| Kingston  | SUV300S37A120G     | 120 GB | 18      | 112   | 0     | 0.31   |
| Samsung   | MZNLN256HMHQ-000H1 | 256 GB | 2       | 112   | 0     | 0.31   |
| SanDisk   | SD6SB1M128G1002    | 128 GB | 1       | 110   | 0     | 0.30   |
| Team      | L5 LITE SSD        | 120 GB | 2       | 110   | 0     | 0.30   |
| Intenso   | SSD                | 128 GB | 3       | 219   | 1     | 0.30   |
| ADATA     | SP920SS            | 128 GB | 4       | 306   | 4     | 0.30   |
| Samsung   | MZNLF128HCHP-000H1 | 128 GB | 1       | 109   | 0     | 0.30   |
| SanDisk   | Ultra II           | 480 GB | 2       | 109   | 0     | 0.30   |
| SanDisk   | SD8TN8U256G1001    | 256 GB | 1       | 109   | 0     | 0.30   |
| SanDisk   | SD7SB6S-128G-1006  | 128 GB | 1       | 108   | 0     | 0.30   |
| Crucial   | CT512M550SSD3      | 512 GB | 2       | 109   | 193   | 0.30   |
| Mushkin   | MKNSSDRE1TB        | 1 TB   | 1       | 107   | 0     | 0.30   |
| Lite-On   | IT L8T-128L9G      | 128 GB | 1       | 105   | 0     | 0.29   |
| Smartbuy  | SSD                | 240 GB | 13      | 120   | 1     | 0.29   |
| SanDisk   | SSD U100           | 16 GB  | 1       | 105   | 0     | 0.29   |
| Toshiba   | TR200              | 480 GB | 2       | 104   | 0     | 0.29   |
| Samsung   | MZ7PD256HCGM-000H7 | 256 GB | 1       | 104   | 0     | 0.29   |
| Samsung   | MZNTD128HAGM-00000 | 128 GB | 2       | 104   | 0     | 0.29   |
| ADATA     | SP600              | 64 GB  | 7       | 113   | 2     | 0.28   |
| Plextor   | PX-128M5M          | 128 GB | 7       | 103   | 0     | 0.28   |
| Lite-On   | CV5-8Q128          | 128 GB | 1       | 103   | 0     | 0.28   |
| Transcend | TS120GSSD25D-M     | 128 GB | 1       | 309   | 2     | 0.28   |
| Samsung   | SSD 860 EVO M.2    | 1 TB   | 1       | 102   | 0     | 0.28   |
| Toshiba   | THNSNH256GBST      | 256 GB | 1       | 102   | 0     | 0.28   |
| Smartbuy  | mSata              | 256 GB | 1       | 102   | 0     | 0.28   |
| KingFast  | SSD                | 32 GB  | 1       | 102   | 0     | 0.28   |
| SanDisk   | Ultra II           | 240 GB | 6       | 100   | 0     | 0.28   |
| OCZ       | VERTEX460          | 120 GB | 2       | 363   | 9     | 0.27   |
| Samsung   | MZ7LN512HCHP-000L1 | 512 GB | 1       | 99    | 0     | 0.27   |
| Kingston  | SKC400S37512G      | 512 GB | 1       | 99    | 0     | 0.27   |
| Toshiba   | THNSNJ128G8NY      | 128 GB | 1       | 98    | 0     | 0.27   |
| SanDisk   | SD8SN8U512G1122    | 512 GB | 1       | 98    | 0     | 0.27   |
| SPCC      | SSD A20            | 64 GB  | 1       | 97    | 0     | 0.27   |
| Intenso   | SSD                | 120 GB | 1       | 97    | 0     | 0.27   |
| Anobit    | Gen2A400 118032738 | 400 GB | 1       | 96    | 0     | 0.27   |
| SanDisk   | SD7SB3Q128G1002    | 128 GB | 1       | 387   | 3     | 0.27   |
| Toshiba   | THNSNJ512GDNU A    | 512 GB | 1       | 96    | 0     | 0.26   |
| SPCC      | SSD170             | 120 GB | 3       | 245   | 341   | 0.26   |
| Corsair   | Force LS SSD       | 120 GB | 8       | 212   | 505   | 0.26   |
| WDC       | WDS250G1B0B-00AS40 | 250 GB | 3       | 94    | 0     | 0.26   |
| SanDisk   | SSD U100           | 256 GB | 1       | 94    | 0     | 0.26   |
| Patriot   | Blaze              | 64 GB  | 4       | 94    | 0     | 0.26   |
| Intel     | SSDSC2BW120A4      | 120 GB | 22      | 104   | 1     | 0.26   |
| Lite-On   | PH2-CJ120          | 120 GB | 1       | 92    | 0     | 0.25   |
| OCZ       | VERTEX460A         | 120 GB | 4       | 92    | 0     | 0.25   |
| Kingston  | SUV400S37120G      | 120 GB | 41      | 105   | 9     | 0.25   |
| Plextor   | PX-256M5S          | 256 GB | 9       | 91    | 0     | 0.25   |
| Faspeed   | H5-60G PLUS        | 64 GB  | 1       | 90    | 0     | 0.25   |
| Kingston  | RBU-SNS8151S396GG  | 96 GB  | 1       | 88    | 0     | 0.24   |
| Samsung   | MZ7LN256HCHP-000L7 | 256 GB | 2       | 87    | 0     | 0.24   |
| Plextor   | PX-128M5Pro        | 128 GB | 50      | 87    | 0     | 0.24   |
| Kingston  | SKC380S3120G       | 120 GB | 1       | 86    | 0     | 0.24   |
| Mushkin   | MKNSSDEC120GB      | 120 GB | 1       | 86    | 0     | 0.24   |
| WDC       | WDBNCE2500PNC      | 250 GB | 1       | 85    | 0     | 0.23   |
| SK hynix  | SC311 SATA         | 512 GB | 2       | 85    | 0     | 0.23   |
| Plextor   | PX-256M3           | 256 GB | 1       | 256   | 2     | 0.23   |
| SanDisk   | SD7SN3Q128G1002    | 128 GB | 1       | 85    | 0     | 0.23   |
| Kingmax   | SSD                | 120 GB | 17      | 195   | 421   | 0.23   |
| PNY       | CS1311 240GB SSD   | 240 GB | 1       | 84    | 0     | 0.23   |
| Transcend | TS256GSSD320       | 256 GB | 2       | 84    | 0     | 0.23   |
| Apple     | SSD SM0256G        | 256 GB | 1       | 83    | 0     | 0.23   |
| Samsung   | SSD 840 EVO 250... | 250 GB | 3       | 83    | 0     | 0.23   |
| Kingston  | SV300S37A480G      | 480 GB | 7       | 91    | 144   | 0.23   |
| Apple     | SSD TS064C         | 60 GB  | 1       | 81    | 0     | 0.22   |
| OCZ       | VECTOR180          | 120 GB | 2       | 81    | 0     | 0.22   |
| Intel     | SSDSA2M160G2LE     | 160 GB | 1       | 1872  | 22    | 0.22   |
| Corsair   | Force LS SSD       | 480 GB | 1       | 80    | 0     | 0.22   |
| Kingston  | RBU-SNS8152S325... | 256 GB | 1       | 80    | 0     | 0.22   |
| SK hynix  | SC308 SATA         | 128 GB | 2       | 80    | 0     | 0.22   |
| Lite-On   | LAT-256M2S         | 256 GB | 1       | 239   | 2     | 0.22   |
| Fordisk   | S860 256G 6Gbps    | 256 GB | 1       | 79    | 0     | 0.22   |
| Seagate   | ST120HM000-1G5142  | 120 GB | 1       | 79    | 0     | 0.22   |
| Toshiba   | Q300 Pro           | 128 GB | 1       | 79    | 0     | 0.22   |
| ADATA     | SU650              | 480 GB | 1       | 77    | 0     | 0.21   |
| SanDisk   | SSD i100           | 8 GB   | 2       | 77    | 0     | 0.21   |
| Corsair   | Neutron XTI SSD    | 240 GB | 1       | 77    | 0     | 0.21   |
| Kingston  | SHSS37A240G        | 240 GB | 12      | 77    | 0     | 0.21   |
| Transcend | TS256GSSD340       | 256 GB | 2       | 76    | 0     | 0.21   |
| China     | SATA SSD           | 64 GB  | 10      | 76    | 0     | 0.21   |
| Samsung   | SSD 650            | 120 GB | 3       | 75    | 0     | 0.21   |
| Corsair   | Neutron SSD        | 64 GB  | 2       | 174   | 6     | 0.21   |
| Intel     | SSDSC2BW240A4      | 240 GB | 7       | 74    | 0     | 0.21   |
| OCZ       | ONYX               | 32 GB  | 2       | 105   | 1     | 0.21   |
| ADATA     | SP580              | 120 GB | 6       | 74    | 0     | 0.20   |
| Lite-On   | CV3-8D128-11 SATA  | 128 GB | 2       | 73    | 0     | 0.20   |
| KingDian  | S400               | 480 GB | 1       | 73    | 0     | 0.20   |
| WDC       | WDS500G2B0A-00SM50 | 500 GB | 3       | 72    | 0     | 0.20   |
| SanDisk   | SD8SB8U256G1122    | 256 GB | 2       | 72    | 0     | 0.20   |
| Crucial   | CT128M550SSD3      | 128 GB | 3       | 212   | 11    | 0.20   |
| SPCC      | SSD                | 55 GB  | 7       | 236   | 436   | 0.20   |
| OCZ       | VERTEX460A         | 240 GB | 2       | 71    | 0     | 0.19   |
| Crucial   | CT525MX300SSD1     | 528 GB | 5       | 77    | 4     | 0.19   |
| Kingston  | SS200S330G         | 32 GB  | 1       | 70    | 0     | 0.19   |
| OCZ       | SABER1000          | 480 GB | 1       | 70    | 0     | 0.19   |
| Toshiba   | TR150              | 480 GB | 2       | 70    | 0     | 0.19   |
| PNY       | CS1311 120GB SSD   | 120 GB | 3       | 69    | 0     | 0.19   |
| Plextor   | PX-256M6S          | 256 GB | 8       | 73    | 127   | 0.19   |
| Transcend | TS256GMTS800       | 256 GB | 2       | 69    | 0     | 0.19   |
| China     | 64GB SSD           | 64 GB  | 10      | 68    | 0     | 0.19   |
| SanDisk   | SD5SE2256G1002E    | 256 GB | 1       | 68    | 0     | 0.19   |
| SanDisk   | PLUS               | 480 GB | 2       | 68    | 0     | 0.19   |
| Kingston  | SHFS37A240G        | 240 GB | 12      | 96    | 424   | 0.19   |
| Intel     | SSDSC2BW120H6      | 120 GB | 7       | 68    | 0     | 0.19   |
| Samsung   | MZNTY256HDHP-000H1 | 256 GB | 1       | 204   | 2     | 0.19   |
| WDC       | WDS240G1G0A-00SS50 | 240 GB | 8       | 68    | 0     | 0.19   |
| ADATA     | S596               | 128 GB | 1       | 68    | 0     | 0.19   |
| SK hynix  | SC308 SATA         | 256 GB | 2       | 67    | 0     | 0.19   |
| Plextor   | PX-128M6M          | 128 GB | 4       | 67    | 0     | 0.19   |
| Toshiba   | TR150              | 240 GB | 7       | 67    | 0     | 0.19   |
| PNY       | SSD2SC240G1LC70... | 240 GB | 1       | 67    | 0     | 0.18   |
| WDC       | WDS500G1B0A-00H9H0 | 500 GB | 3       | 67    | 0     | 0.18   |
| ADATA     | SP900              | 512 GB | 2       | 66    | 0     | 0.18   |
| Samsung   | SSD 860 EVO M.2    | 500 GB | 1       | 66    | 0     | 0.18   |
| SanDisk   | SD8TB8U512G1001    | 512 GB | 1       | 66    | 0     | 0.18   |
| China     | 128GB SSD          | 128 GB | 5       | 66    | 0     | 0.18   |
| Kingston  | RBU-SNS8100S3256GD | 256 GB | 1       | 66    | 0     | 0.18   |
| Corsair   | Force LS SSD       | 64 GB  | 16      | 202   | 195   | 0.18   |
| Corsair   | CSSD-F120GB3-BK    | 120 GB | 1       | 65    | 0     | 0.18   |
| SPCC      | SSD                | 240 GB | 26      | 156   | 340   | 0.18   |
| Intel     | SSDSC2BW480A4      | 480 GB | 1       | 65    | 0     | 0.18   |
| Lite-On   | LMT-32L3M-HP       | 32 GB  | 2       | 65    | 0     | 0.18   |
| Transcend | TS64GMSA370        | 64 GB  | 2       | 65    | 0     | 0.18   |
| Corsair   | Force LE200 SSD    | 240 GB | 1       | 64    | 0     | 0.18   |
| WDC       | WDS250G2B0B-00YS70 | 250 GB | 1       | 64    | 0     | 0.18   |
| Intel     | SSDSA2M160G2GC     | 160 GB | 1       | 578   | 8     | 0.18   |
| KingSpec  | NT-256             | 256 GB | 3       | 64    | 0     | 0.18   |
| Apple     | SSD SM0128G        | 121 GB | 1       | 64    | 0     | 0.18   |
| Kingston  | SKC400S37256G      | 256 GB | 1       | 64    | 0     | 0.18   |
| WDC       | WDS240G1G0B-00RC30 | 240 GB | 4       | 63    | 0     | 0.17   |
| Intel     | SSDSC2BW080A4      | 80 GB  | 1       | 63    | 0     | 0.17   |
| Transcend | TS128GSSD370       | 128 GB | 6       | 63    | 0     | 0.17   |
| KingSpec  | Q-180              | 180 GB | 2       | 63    | 0     | 0.17   |
| SanDisk   | SSD U110           | 16 GB  | 9       | 63    | 0     | 0.17   |
| Crucial   | CT128M550SSD1      | 128 GB | 3       | 88    | 30    | 0.17   |
| China     | 120GB SSD          | 120 GB | 25      | 62    | 0     | 0.17   |
| Toshiba   | Q200 EX            | 240 GB | 1       | 61    | 0     | 0.17   |
| SanDisk   | SDSSDA240G         | 240 GB | 40      | 61    | 0     | 0.17   |
| Plextor   | PX-128M6S          | 128 GB | 22      | 66    | 51    | 0.17   |
| PHISON    | 128GB PS3109-S9    | 128 GB | 1       | 121   | 1     | 0.17   |
| SanDisk   | SSD U100           | 24 GB  | 15      | 70    | 19    | 0.17   |
| Samsung   | MZNLN256HCHP-00000 | 256 GB | 1       | 60    | 0     | 0.17   |
| Kingston  | SHPM2280P2H-480G   | 480 GB | 1       | 180   | 2     | 0.16   |
| Goodram   | GOODRAM IR-SSDP... | 120 GB | 2       | 59    | 0     | 0.16   |
| Patriot   | Burst              | 480 GB | 3       | 59    | 0     | 0.16   |
| Micron    | 1100_MTFDDAV512TBN | 512 GB | 4       | 59    | 0     | 0.16   |
| TEKET     | SA18-032M-4F       | 32 GB  | 2       | 59    | 0     | 0.16   |
| OCZ       | VERTEX2            | 180 GB | 1       | 58    | 0     | 0.16   |
| Toshiba   | THNSNK128GVN8      | 128 GB | 2       | 58    | 0     | 0.16   |
| Plextor   | PX-512M6Pro        | 512 GB | 1       | 58    | 0     | 0.16   |
| SanDisk   | SDSSDH2064G        | 64 GB  | 1       | 58    | 0     | 0.16   |
| Goldkey   | GKH84-64GB         | 64 GB  | 1       | 57    | 0     | 0.16   |
| Kingston  | SUV500480G         | 480 GB | 1       | 57    | 0     | 0.16   |
| ADATA     | SP610              | 128 GB | 2       | 57    | 0     | 0.16   |
| ADATA     | SP900NS38          | 128 GB | 3       | 124   | 339   | 0.16   |
| Samsung   | MZNTY256HDHP-00000 | 256 GB | 2       | 56    | 0     | 0.16   |
| OCZ       | VERTEX460          | 240 GB | 1       | 56    | 0     | 0.15   |
| Radeon    | R7                 | 120 GB | 1       | 56    | 0     | 0.15   |
| Samsung   | SSD 860 EVO M.2    | 250 GB | 5       | 55    | 0     | 0.15   |
| Samsung   | SSD 860 EVO        | 1 TB   | 7       | 55    | 0     | 0.15   |
| Kingston  | SHFS37A120G        | 120 GB | 52      | 131   | 412   | 0.15   |
| Samsung   | SSD 860 EVO        | 500 GB | 17      | 55    | 0     | 0.15   |
| Transcend | TS64GSSD340        | 64 GB  | 1       | 54    | 0     | 0.15   |
| Crucial   | CT250BX100SSD1     | 250 GB | 4       | 54    | 0     | 0.15   |
| China     | SSD 120G           | 120 GB | 2       | 54    | 0     | 0.15   |
| Lite-On   | L8H-256V2G-HP      | 256 GB | 2       | 54    | 0     | 0.15   |
| AMD       | R5SL240G           | 240 GB | 2       | 54    | 0     | 0.15   |
| SanDisk   | SSD PLUS 240 GB    | 240 GB | 5       | 53    | 0     | 0.15   |
| Samsung   | MZMTD128HAFV-000H1 | 128 GB | 1       | 53    | 0     | 0.15   |
| Zheino    | CHN-25SATAA3-480   | 480 GB | 2       | 53    | 0     | 0.15   |
| Samsung   | MZMPC128HBFU-000MV | 128 GB | 1       | 53    | 0     | 0.15   |
| SanDisk   | SDSSDA960G         | 960 GB | 1       | 52    | 0     | 0.14   |
| Team      | L5 LITE SSD        | 64 GB  | 1       | 52    | 0     | 0.14   |
| Intel     | SSDSC2BB240G4      | 240 GB | 1       | 52    | 0     | 0.14   |
| Team      | L7 EVO SSD         | 64 GB  | 1       | 51    | 0     | 0.14   |
| Lite-On   | LMT-64M6M-HP       | 64 GB  | 1       | 51    | 0     | 0.14   |
| Lite-On   | LMT-19nmBGA-128G   | 128 GB | 1       | 51    | 0     | 0.14   |
| Lite-On   | CV3-8D128          | 128 GB | 2       | 50    | 0     | 0.14   |
| Samsung   | MZMTD128HAFV-000   | 128 GB | 3       | 50    | 0     | 0.14   |
| Mushkin   | MKNSSDSR250GB      | 250 GB | 1       | 50    | 0     | 0.14   |
| Samsung   | SSD PB22-CS3 FD... | 256 GB | 1       | 50    | 0     | 0.14   |
| OCZ       | INTREPID 3800      | 400 GB | 1       | 50    | 0     | 0.14   |
| Samsung   | MZ7LF128HCHP-00004 | 128 GB | 1       | 50    | 0     | 0.14   |
| Kingston  | SA400S37240G       | 240 GB | 26      | 67    | 5     | 0.14   |
| Corsair   | Neutron GTX SSD    | 480 GB | 1       | 49    | 0     | 0.14   |
| KingDian  | S200               | 120 GB | 2       | 49    | 0     | 0.13   |
| Samsung   | MZNTY128HDHP-00000 | 128 GB | 2       | 48    | 0     | 0.13   |
| Transcend | TS256GSSD230S      | 256 GB | 1       | 48    | 0     | 0.13   |
| SK hynix  | HFS128G32MND-3212A | 128 GB | 1       | 48    | 0     | 0.13   |
| Plextor   | PX-128S3C          | 128 GB | 8       | 47    | 0     | 0.13   |
| GeIL      | R3 120G            | 120 GB | 1       | 47    | 0     | 0.13   |
| China     | 80GB SSD           | 80 GB  | 2       | 47    | 0     | 0.13   |
| Micron    | 1100_MTFDDAK256TBN | 256 GB | 4       | 47    | 0     | 0.13   |
| SPCC      | M.2 SSD            | 120 GB | 3       | 107   | 2     | 0.13   |
| Plextor   | PX-256M8VG         | 256 GB | 1       | 46    | 0     | 0.13   |
| oyunkey   | SSD                | 120 GB | 1       | 46    | 0     | 0.13   |
| Micron    | 1100_MTFDDAV256TBN | 256 GB | 7       | 51    | 242   | 0.13   |
| SanDisk   | SD5SF2032G1010E    | 32 GB  | 1       | 45    | 0     | 0.13   |
| Crucial   | CT275MX300SSD1     | 275 GB | 10      | 45    | 1     | 0.13   |
| Samsung   | SSD 860 EVO        | 250 GB | 27      | 45    | 0     | 0.13   |
| Crucial   | CT1000MX500SSD1    | 1 TB   | 2       | 45    | 0     | 0.12   |
| Kingston  | RBU-SC152S37128GG2 | 128 GB | 1       | 45    | 0     | 0.12   |
| Plextor   | PX-256M5M          | 256 GB | 1       | 45    | 0     | 0.12   |
| Goodram   | GOODRAM IR-SSDP... | 240 GB | 1       | 44    | 0     | 0.12   |
| PRETEC    | G2000 SSD          | 29 GB  | 1       | 44    | 0     | 0.12   |
| ADATA     | SU700              | 120 GB | 3       | 44    | 0     | 0.12   |
| Plextor   | PX-512M3           | 512 GB | 1       | 1402  | 31    | 0.12   |
| WDC       | WDS120G1G0A-00SS50 | 120 GB | 14      | 43    | 0     | 0.12   |
| KingDian  | S280-240GB         | 240 GB | 7       | 43    | 0     | 0.12   |
| China     | SSD128G            | 128 GB | 1       | 42    | 0     | 0.12   |
| KingSpec  | T-64               | 64 GB  | 2       | 59    | 3     | 0.12   |
| China     | 240GB SSD          | 240 GB | 1       | 41    | 0     | 0.11   |
| ADATA     | SP920SS            | 256 GB | 4       | 138   | 8     | 0.11   |
| China     | SATA SSD           | 120 GB | 11      | 39    | 0     | 0.11   |
| ADATA     | SP550              | 240 GB | 6       | 39    | 0     | 0.11   |
| WDC       | WDS100T1B0A-00H9H0 | 1 TB   | 2       | 39    | 0     | 0.11   |
| Kingston  | SA400S37120G       | 120 GB | 68      | 40    | 1     | 0.11   |
| Apacer    | AS330              | 120 GB | 2       | 39    | 0     | 0.11   |
| WDC       | WDS120G2G0B-00EPW0 | 120 GB | 1       | 78    | 1     | 0.11   |
| Plextor   | PX-AG128M6e        | 128 GB | 3       | 186   | 2     | 0.11   |
| WDC       | WDS250G2B0A-00SM50 | 250 GB | 2       | 38    | 0     | 0.11   |
| Kingston  | SH103S3480G        | 480 GB | 1       | 38    | 0     | 0.11   |
| Kingston  | RBUSNS8180S3128GI1 | 128 GB | 1       | 38    | 0     | 0.11   |
| SanDisk   | SDSSDH3512G        | 512 GB | 1       | 38    | 0     | 0.10   |
| Intel     | SSDSC2KW256G8      | 256 GB | 9       | 37    | 0     | 0.10   |
| Samsung   | SSD 850 EVO mSATA  | 250 GB | 2       | 37    | 0     | 0.10   |
| Crucial   | CT120BX300SSD1     | 120 GB | 2       | 37    | 0     | 0.10   |
| SanDisk   | SD6SB1M128G1022I   | 128 GB | 2       | 37    | 0     | 0.10   |
| SanDisk   | SSD PLUS 120 GB    | 120 GB | 7       | 37    | 0     | 0.10   |
| Samsung   | MZNTY128HDHP-000H1 | 128 GB | 2       | 37    | 0     | 0.10   |
| WDC       | WDS240G2G0A-00JH30 | 240 GB | 12      | 37    | 0     | 0.10   |
| Corsair   | CSSD-V60GB2        | 64 GB  | 1       | 332   | 8     | 0.10   |
| Crucial   | CT120BX500SSD1     | 120 GB | 10      | 36    | 0     | 0.10   |
| Micron    | MTFDDAK512MAY-1... | 512 GB | 1       | 624   | 16    | 0.10   |
| Intel     | SSDSA1M080G2HP     | 80 GB  | 1       | 476   | 12    | 0.10   |
| Transcend | TS256GMTS430S      | 256 GB | 2       | 36    | 0     | 0.10   |
| AMD       | R3SL60G            | 64 GB  | 2       | 35    | 0     | 0.10   |
| PNY       | CS900 240GB SSD    | 240 GB | 2       | 35    | 0     | 0.10   |
| Samsung   | MZNLN128HAHQ-00000 | 128 GB | 1       | 35    | 0     | 0.10   |
| Plextor   | PX-256S3C          | 256 GB | 1       | 34    | 0     | 0.10   |
| Plextor   | PX-64M5M           | 64 GB  | 1       | 34    | 0     | 0.10   |
| Patriot   | Burst              | 240 GB | 3       | 34    | 0     | 0.10   |
| ZOTAC     | SATA SSD           | 120 GB | 2       | 34    | 0     | 0.09   |
| China     | SATA SSD           | 240 GB | 5       | 34    | 0     | 0.09   |
| AMD       | R3SL120G           | 120 GB | 19      | 34    | 0     | 0.09   |
| ADATA     | SX950              | 240 GB | 1       | 34    | 0     | 0.09   |
| AMD       | R5S240GBSF         | 240 GB | 1       | 33    | 0     | 0.09   |
| AMD       | R5SL120G           | 120 GB | 7       | 53    | 1     | 0.09   |
| Intel     | SSDSA1MH080G1HP    | 80 GB  | 1       | 33    | 0     | 0.09   |
| KingDian  | S180               | 64 GB  | 10      | 38    | 114   | 0.09   |
| SanDisk   | SD6SB1M-032G-1006  | 32 GB  | 1       | 33    | 0     | 0.09   |
| Plextor   | PX-128M6Pro        | 128 GB | 8       | 33    | 0     | 0.09   |
| China     | SSD                | 120 GB | 4       | 32    | 0     | 0.09   |
| Smartbuy  | mSata              | 128 GB | 2       | 32    | 0     | 0.09   |
| SPCC      | SSD                | 256 GB | 4       | 72    | 26    | 0.09   |
| Kingmax   | SSD                | 64 GB  | 18      | 223   | 524   | 0.09   |
| Samsung   | SSD PM851          | 128 GB | 1       | 32    | 0     | 0.09   |
| KingSpec  | ACSC4M512mSA       | 506 GB | 1       | 32    | 0     | 0.09   |
| OCZ       | VERTEX4            | 79 GB  | 1       | 129   | 3     | 0.09   |
| Lite-On   | L8T-128L6G-HP      | 128 GB | 2       | 32    | 640   | 0.09   |
| SanDisk   | SDSSDXP120G        | 120 GB | 1       | 32    | 0     | 0.09   |
| Micron    | C400-MTFDDAK256MAM | 256 GB | 1       | 32    | 0     | 0.09   |
| Toshiba   | THNSNB062GMCJ      | 64 GB  | 1       | 32    | 0     | 0.09   |
| KingSpec  | Q-360              | 360 GB | 2       | 31    | 0     | 0.09   |
| Samsung   | MZMTD512HAGL-000L1 | 512 GB | 2       | 84    | 49    | 0.09   |
| Samsung   | SSD 850            | 120 GB | 17      | 31    | 0     | 0.09   |
| SanDisk   | SDSSDA480G         | 480 GB | 3       | 31    | 0     | 0.09   |
| OCZ       | TRION150           | 480 GB | 1       | 31    | 0     | 0.09   |
| KingSpec  | KSD-SA25.7-016MJ   | 16 GB  | 2       | 30    | 0     | 0.08   |
| Toshiba   | VT180              | 480 GB | 1       | 30    | 0     | 0.08   |
| SanDisk   | SSD i100           | 32 GB  | 2       | 30    | 0     | 0.08   |
| ADATA     | SP900NS34          | 128 GB | 1       | 30    | 0     | 0.08   |
| Micron    | MTFDDAK256MAM-1K12 | 256 GB | 3       | 71    | 1010  | 0.08   |
| Samsung   | MZ7LN256HAJQ-00000 | 256 GB | 1       | 30    | 0     | 0.08   |
| PNY       | SSD2SC120G1CS17... | 120 GB | 2       | 30    | 0     | 0.08   |
| SanDisk   | SSD PLUS           | 240 GB | 4       | 30    | 0     | 0.08   |
| SanDisk   | SDSSDA120G         | 120 GB | 27      | 33    | 4     | 0.08   |
| SK hynix  | HFS128G39MNC-3510A | 128 GB | 1       | 30    | 0     | 0.08   |
| Micron    | MTFDDAK128MAM-1J1  | 128 GB | 1       | 29    | 0     | 0.08   |
| SanDisk   | SDSA5DK-016G-1006  | 16 GB  | 1       | 29    | 0     | 0.08   |
| KingPower | 1108 SSD           | 64 GB  | 1       | 29    | 0     | 0.08   |
| Apple     | SSD SD0128F        | 121 GB | 1       | 29    | 0     | 0.08   |
| Samsung   | MZ7TY256HDHP-00000 | 256 GB | 1       | 29    | 0     | 0.08   |
| Transcend | TS128GSSD370S      | 128 GB | 14      | 29    | 0     | 0.08   |
| ADATA     | SU800NS38          | 256 GB | 1       | 29    | 0     | 0.08   |
| Intel     | SSDSA1M160G2HP     | 160 GB | 3       | 90    | 5     | 0.08   |
| SK hynix  | HFS128G39TND-N210A | 128 GB | 9       | 29    | 0     | 0.08   |
| Samsung   | SSD Thin uSATA ... | 128 GB | 1       | 378   | 12    | 0.08   |
| Kingston  | RBUSNS8180S3128GJ  | 128 GB | 1       | 29    | 0     | 0.08   |
| Transcend | TS128GMTS400       | 128 GB | 1       | 29    | 0     | 0.08   |
| KingSpec  | MT-128             | 128 GB | 4       | 29    | 0     | 0.08   |
| Patriot   | Spark              | 256 GB | 2       | 28    | 0     | 0.08   |
| ADATA     | SU800              | 128 GB | 18      | 30    | 5     | 0.08   |
| Samsung   | SSD 860 EVO        | 2 TB   | 3       | 28    | 0     | 0.08   |
| Toshiba   | THNSNF256GMCS      | 256 GB | 1       | 28    | 0     | 0.08   |
| Crucial   | CT500MX500SSD4     | 500 GB | 2       | 28    | 0     | 0.08   |
| LDLC      | SSD                | 120 GB | 2       | 27    | 0     | 0.08   |
| Samsung   | SSD 860 PRO        | 512 GB | 6       | 27    | 0     | 0.08   |
| Toshiba   | TR200              | 240 GB | 9       | 27    | 0     | 0.08   |
| Samsung   | MZ7TY128HDHP-000L1 | 128 GB | 2       | 27    | 0     | 0.08   |
| SenDisk   | C3-60G             | 64 GB  | 1       | 27    | 0     | 0.08   |
| Team      | TEAML5Lite3D120G   | 120 GB | 1       | 27    | 0     | 0.07   |
| KingSpec  | CHA-M2B7-M256      | 256 GB | 2       | 27    | 0     | 0.07   |
| Transcend | TS64GSSD720        | 64 GB  | 1       | 26    | 0     | 0.07   |
| Transcend | TS64GSSD320        | 64 GB  | 1       | 26    | 0     | 0.07   |
| Intel     | SSDSC2BB240G7      | 240 GB | 2       | 25    | 0     | 0.07   |
| KingSpec  | T-120              | 120 GB | 1       | 25    | 0     | 0.07   |
| SK hynix  | HFS512G39TNF-N3A0A | 512 GB | 1       | 25    | 0     | 0.07   |
| Toshiba   | THNSNH128G8NT      | 128 GB | 1       | 25    | 0     | 0.07   |
| Plextor   | PX-512M5Pro        | 512 GB | 1       | 25    | 0     | 0.07   |
| Apacer    | AS350              | 120 GB | 4       | 25    | 0     | 0.07   |
| Crucial   | CT480BX500SSD1     | 480 GB | 2       | 25    | 0     | 0.07   |
| Intel     | SSDSC2BW180A3H     | 180 GB | 1       | 25    | 0     | 0.07   |
| Apacer    | AS350              | 240 GB | 5       | 25    | 0     | 0.07   |
| SK hynix  | HFS256G32MND-2900A | 256 GB | 1       | 531   | 20    | 0.07   |
| Transcend | TS240GSSD220S      | 240 GB | 4       | 25    | 0     | 0.07   |
| Lite-On   | CV5-8Q256          | 256 GB | 1       | 24    | 0     | 0.07   |
| Corsair   | CSSD-V32GB2        | 32 GB  | 1       | 99    | 3     | 0.07   |
| Kingston  | SA400S37480G       | 480 GB | 9       | 30    | 10    | 0.07   |
| Plextor   | PX-128S2C          | 128 GB | 2       | 24    | 0     | 0.07   |
| Toshiba   | Q300               | 480 GB | 1       | 24    | 0     | 0.07   |
| ADATA     | SU650              | 120 GB | 7       | 34    | 2     | 0.07   |
| PNY       | SSD2SC120G1SA75... | 120 GB | 1       | 23    | 0     | 0.07   |
| Crucial   | CT250MX500SSD1     | 250 GB | 3       | 23    | 0     | 0.06   |
| Londisk   | SSD                | 120 GB | 3       | 23    | 0     | 0.06   |
| Intel     | SSDSA2BW160G3H     | 160 GB | 2       | 98    | 4     | 0.06   |
| Transcend | TS256GSSD370S      | 256 GB | 2       | 23    | 0     | 0.06   |
| Kingrich  | SSD 120G           | 120 GB | 1       | 23    | 0     | 0.06   |
| GLOWAY    | FER240GS3-S7       | 240 GB | 1       | 23    | 0     | 0.06   |
| Toshiba   | THNSNS060GBSP      | 64 GB  | 1       | 22    | 0     | 0.06   |
| Plextor   | PX-128M6V          | 128 GB | 1       | 22    | 0     | 0.06   |
| Kingston  | SKC300S37A180G     | 180 GB | 2       | 166   | 1025  | 0.06   |
| e2e4      | SSD                | 120 GB | 2       | 22    | 0     | 0.06   |
| Lite-On   | CV3-DE256          | 256 GB | 2       | 22    | 0     | 0.06   |
| Plextor   | PX-256M6M          | 256 GB | 3       | 29    | 1     | 0.06   |
| Plextor   | PX-256M8VC         | 256 GB | 1       | 21    | 0     | 0.06   |
| OCZ       | INTREPID 3600      | 400 GB | 1       | 21    | 0     | 0.06   |
| Transcend | TS120GSSD220S      | 120 GB | 5       | 21    | 0     | 0.06   |
| Samsung   | SSD 860 EVO mSATA  | 250 GB | 5       | 21    | 0     | 0.06   |
| Lite-On   | CV1-8B128          | 128 GB | 3       | 21    | 0     | 0.06   |
| Transcend | TS128GSSD230S      | 128 GB | 6       | 20    | 0     | 0.06   |
| ADATA     | SU900              | 256 GB | 4       | 21    | 1     | 0.06   |
| Goldkey   | GKH84-256GB        | 256 GB | 1       | 20    | 0     | 0.06   |
| KingDian  | S280               | 240 GB | 7       | 20    | 0     | 0.06   |
| Lenovo    | SSD SL700 480G     | 480 GB | 1       | 20    | 0     | 0.06   |
| AMD       | R3SL240G           | 240 GB | 4       | 44    | 1     | 0.06   |
| KingDian  | S280               | 120 GB | 4       | 20    | 0     | 0.06   |
| XUNZHE    | XUNZHE XUNZHE80... | 120 GB | 1       | 20    | 0     | 0.05   |
| Samsung   | MZYLF128HCHP-000L2 | 128 GB | 1       | 20    | 0     | 0.05   |
| Netac     | SSD 120G           | 120 GB | 1       | 19    | 0     | 0.05   |
| SanDisk   | SD8SN8U-128G-1006  | 128 GB | 10      | 40    | 3     | 0.05   |
| Intel     | SSDSCKGF256A5 SATA | 256 GB | 1       | 19    | 0     | 0.05   |
| Transcend | TS128GMTS800       | 128 GB | 3       | 19    | 0     | 0.05   |
| Kingston  | SUV500120G         | 120 GB | 4       | 18    | 0     | 0.05   |
| SanDisk   | SSD U100           | 64 GB  | 5       | 18    | 0     | 0.05   |
| SanDisk   | SDSSDH3250G        | 250 GB | 1       | 18    | 0     | 0.05   |
| SK hynix  | SC210 2.5 7MM      | 128 GB | 1       | 18    | 0     | 0.05   |
| Seagate   | BarraCuda SSD Z... | 1 TB   | 1       | 18    | 0     | 0.05   |
| Corsair   | Neutron SSD        | 240 GB | 1       | 2126  | 114   | 0.05   |
| ADATA     | SP610              | 256 GB | 1       | 18    | 0     | 0.05   |
| Hyperdisk | SDOM               | 16 GB  | 1       | 18    | 0     | 0.05   |
| SK hynix  | HFS256G39TND-N210A | 256 GB | 2       | 18    | 0     | 0.05   |
| SanDisk   | SD8SN8U1T001122    | 1 TB   | 1       | 17    | 0     | 0.05   |
| China     | 60GB SSD           | 64 GB  | 1       | 17    | 0     | 0.05   |
| China     | RTMMB256VBV4KFY    | 256 GB | 1       | 17    | 0     | 0.05   |
| China     | SSD                | 240 GB | 6       | 36    | 1     | 0.05   |
| Kingrich  | 64GB K9 SATA3 SSD  | 63 GB  | 2       | 17    | 0     | 0.05   |
| AMD       | R3S60GBSM          | 64 GB  | 2       | 17    | 0     | 0.05   |
| LDNDISK   | SSD                | 240 GB | 1       | 16    | 0     | 0.05   |
| SanDisk   | SD6SB1M-128G-1006  | 128 GB | 1       | 16    | 0     | 0.05   |
| Platinet  | SSD                | 120 GB | 1       | 16    | 0     | 0.04   |
| Intel     | SSDSA2M080G2GN     | 80 GB  | 1       | 245   | 14    | 0.04   |
| Intel     | SSDMCEAW080A4      | 80 GB  | 1       | 16    | 0     | 0.04   |
| China     | SSD                | 128 GB | 4       | 16    | 0     | 0.04   |
| KingFast  | SSD                | 128 GB | 8       | 15    | 0     | 0.04   |
| Apple     | SSD SM256E         | 256 GB | 1       | 15    | 0     | 0.04   |
| Plextor   | PX-256M6Pro        | 256 GB | 1       | 15    | 0     | 0.04   |
| SanDisk   | SD8SBAT128G1122    | 128 GB | 1       | 15    | 0     | 0.04   |
| FASTDISK  | FASTDISK FASTDI... | 120 GB | 1       | 15    | 0     | 0.04   |
| GLOWAY    | FER120GS3-S7       | 120 GB | 2       | 15    | 0     | 0.04   |
| WDC       | WDS120G2G0A-00JH30 | 120 GB | 22      | 15    | 0     | 0.04   |
| Transcend | TS32GSSD370S       | 32 GB  | 4       | 15    | 0     | 0.04   |
| Apacer    | AS510S             | 64 GB  | 2       | 15    | 0     | 0.04   |
| FORESEE   | 240GB SSD          | 240 GB | 1       | 15    | 0     | 0.04   |
| Crucial   | CT2050MX300SSD1    | 2 TB   | 2       | 29    | 1     | 0.04   |
| Transcend | TS64GSSD25S-M      | 64 GB  | 2       | 15    | 0     | 0.04   |
| SPCC      | SSD                | 480 GB | 1       | 15    | 0     | 0.04   |
| Samsung   | SSD 850 EVO mSATA  | 500 GB | 3       | 14    | 0     | 0.04   |
| FASTDISK  | FASTDISK 60G       | 64 GB  | 1       | 14    | 0     | 0.04   |
| Crucial   | CT240BX200SSD1     | 240 GB | 3       | 14    | 0     | 0.04   |
| Goodram   | GOODRAM SSDPR-C... | 128 GB | 2       | 14    | 0     | 0.04   |
| Seagate   | ST480FP0021        | 480 GB | 1       | 14    | 0     | 0.04   |
| QUMO      | SSD                | 120 GB | 2       | 303   | 509   | 0.04   |
| Apple     | SSD TS128E         | 121 GB | 1       | 128   | 8     | 0.04   |
| Micron    | M600_MTFDDAK1T0MBF | 1 TB   | 1       | 13    | 0     | 0.04   |
| ADATA     | SU800              | 1 TB   | 1       | 13    | 0     | 0.04   |
| Transcend | TS128GSSD360S      | 128 GB | 6       | 13    | 0     | 0.04   |
| OCZ       | VECTOR180          | 960 GB | 1       | 13    | 0     | 0.04   |
| Samsung   | MZYTY128HDHP-000L2 | 128 GB | 1       | 13    | 0     | 0.04   |
| Samsung   | SSD 860 QVO        | 1 TB   | 1       | 13    | 0     | 0.04   |
| FORESEE   | 64GB SSD           | 64 GB  | 2       | 13    | 0     | 0.04   |
| BIWIN     | SSD                | 120 GB | 1       | 13    | 0     | 0.04   |
| KingSpec  | P3-256             | 256 GB | 2       | 13    | 0     | 0.04   |
| Transcend | TS128GMTS600       | 128 GB | 1       | 13    | 0     | 0.04   |
| TCSUNBOW  | X3                 | 120 GB | 1       | 13    | 0     | 0.04   |
| SanDisk   | SSD PLUS           | 120 GB | 3       | 12    | 0     | 0.04   |
| SK hynix  | SH920 mSATA        | 256 GB | 1       | 680   | 52    | 0.04   |
| Transcend | TS512GMSA370       | 512 GB | 1       | 12    | 0     | 0.03   |
| SanDisk   | SSD P4             | 32 GB  | 9       | 92    | 134   | 0.03   |
| Crucial   | CT500MX500SSD1     | 500 GB | 6       | 12    | 0     | 0.03   |
| KingSpec  | P3-512             | 512 GB | 4       | 30    | 197   | 0.03   |
| China     | SSD                | 480 GB | 2       | 11    | 0     | 0.03   |
| HP        | SSD S700           | 250 GB | 1       | 11    | 0     | 0.03   |
| ADATA     | IM2S3138E-128GM-B  | 128 GB | 1       | 11    | 0     | 0.03   |
| Goodram   | GOODRAM SSDPR-C... | 120 GB | 1       | 11    | 0     | 0.03   |
| Crucial   | V4-CT128V4SSD2     | 128 GB | 1       | 11    | 0     | 0.03   |
| Faspeed   | M3-360G            | 360 GB | 1       | 11    | 0     | 0.03   |
| Crucial   | CT480BX200SSD1     | 480 GB | 2       | 11    | 0     | 0.03   |
| ADATA     | SP550              | 120 GB | 18      | 12    | 1     | 0.03   |
| Team      | T2535T120G         | 120 GB | 1       | 11    | 0     | 0.03   |
| Micron    | M600_MTFDDAV512MBF | 512 GB | 1       | 10    | 0     | 0.03   |
| Intel     | SSDSCKKW240H6      | 240 GB | 3       | 18    | 1     | 0.03   |
| ADATA     | SU650              | 240 GB | 6       | 10    | 0     | 0.03   |
| ADATA     | SX300              | 128 GB | 1       | 10    | 0     | 0.03   |
| Crucial   | CT240BX500SSD1     | 240 GB | 4       | 10    | 0     | 0.03   |
| Plextor   | PX-AG256M6e        | 256 GB | 1       | 10    | 0     | 0.03   |
| Lite-On   | LCH-256V2S-11 2... | 256 GB | 1       | 10    | 0     | 0.03   |
| Intel     | SSDSA2CW160G3      | 160 GB | 1       | 10    | 0     | 0.03   |
| Samsung   | SSD 860 QVO        | 2 TB   | 1       | 10    | 0     | 0.03   |
| KingSpec  | P3-128             | 128 GB | 2       | 9     | 0     | 0.03   |
| Toshiba   | TL100              | 240 GB | 2       | 9     | 0     | 0.03   |
| Crucial   | CT480M500SSD1      | 480 GB | 1       | 166   | 16    | 0.03   |
| SPCC      | SSD162             | 240 GB | 1       | 203   | 20    | 0.03   |
| LDLC      | SSD                | 128 GB | 3       | 9     | 0     | 0.03   |
| Faspeed   | H5-120G PLUS       | 120 GB | 1       | 9     | 0     | 0.03   |
| KingFast  | SSD                | 120 GB | 4       | 9     | 0     | 0.03   |
| KingSpec  | ACSC2M064mSA       | 63 GB  | 1       | 9     | 0     | 0.03   |
| KingDian  | S280-120GB         | 120 GB | 2       | 9     | 0     | 0.03   |
| Patriot   | Blaze              | 120 GB | 3       | 9     | 0     | 0.03   |
| ADATA     | SU900              | 128 GB | 1       | 9     | 0     | 0.02   |
| Intel     | SSDMCEAW180A4      | 180 GB | 1       | 9     | 0     | 0.02   |
| KingDian  | S280               | 480 GB | 1       | 9     | 0     | 0.02   |
| SK hynix  | HFS128G3BTND-N210A | 128 GB | 1       | 9     | 0     | 0.02   |
| Londisk   | SSD                | 240 GB | 1       | 8     | 0     | 0.02   |
| Teclast   | 128GB MS550        | 128 GB | 1       | 8     | 0     | 0.02   |
| KingDian  | N400 60G           | 64 GB  | 1       | 8     | 0     | 0.02   |
| SanDisk   | SD7SN6S-128G-1006  | 128 GB | 1       | 8     | 0     | 0.02   |
| SPCC      | SSD                | 512 GB | 2       | 8     | 0     | 0.02   |
| Londisk   | SSD                | 480 GB | 1       | 8     | 0     | 0.02   |
| WDC       | WDS240G2G0B-00EPW0 | 240 GB | 4       | 8     | 0     | 0.02   |
| Kingston  | RBU-SNS8100S312... | 128 GB | 2       | 8     | 0     | 0.02   |
| Plextor   | PH6-CE120          | 120 GB | 1       | 8     | 0     | 0.02   |
| PNY       | SSD2SC256GM1P3D... | 256 GB | 1       | 7     | 0     | 0.02   |
| Lite-On   | CV1-8B256          | 256 GB | 3       | 7     | 0     | 0.02   |
| FASTDISK  | FASTDISK 120G      | 120 GB | 1       | 7     | 0     | 0.02   |
| SK hynix  | SC311 SATA         | 128 GB | 3       | 7     | 0     | 0.02   |
| Apacer    | AST280             | 240 GB | 1       | 7     | 0     | 0.02   |
| SanDisk   | SD8SNAT256G1002    | 256 GB | 2       | 7     | 0     | 0.02   |
| KingSpec  | T-60               | 64 GB  | 3       | 7     | 0     | 0.02   |
| KingSpec  | ACSC2M128mSA       | 128 GB | 1       | 7     | 0     | 0.02   |
| Toshiba   | THNSNS128GMCP      | 128 GB | 1       | 7     | 0     | 0.02   |
| Zheino    | CHN 25SATA01M 060  | 64 GB  | 1       | 6     | 0     | 0.02   |
| KingDian  | S200               | 64 GB  | 4       | 6     | 0     | 0.02   |
| Intel     | SSDSCKJW180H6      | 180 GB | 1       | 6     | 0     | 0.02   |
| Patriot   | Ignite             | 240 GB | 1       | 6     | 0     | 0.02   |
| SanDisk   | SD8SBAT256G1122    | 256 GB | 3       | 6     | 0     | 0.02   |
| Transcend | TS240GMTS820S      | 240 GB | 1       | 6     | 0     | 0.02   |
| Samsung   | SSD PM871b 2.5 7mm | 128 GB | 1       | 6     | 0     | 0.02   |
| Integral  | V Series SATA SSD  | 240 GB | 1       | 6     | 0     | 0.02   |
| SK hynix  | SC210 mSATA        | 128 GB | 2       | 123   | 32    | 0.02   |
| Lite-On   | CV5-8Q512-HP       | 512 GB | 1       | 6     | 0     | 0.02   |
| Toshiba   | A100               | 120 GB | 2       | 6     | 0     | 0.02   |
| SK hynix  | SC210 mSATA        | 256 GB | 2       | 195   | 28    | 0.02   |
| Goodram   | GOODRAM SSDPR_C... | 120 GB | 1       | 6     | 0     | 0.02   |
| Samsung   | SSD 860 PRO        | 256 GB | 4       | 6     | 0     | 0.02   |
| Intel     | SSDSCKKF256G8 SATA | 256 GB | 1       | 6     | 0     | 0.02   |
| Patriot   | Burst              | 120 GB | 6       | 6     | 0     | 0.02   |
| Lite-On   | L8H-256V2G-11 M... | 256 GB | 1       | 6     | 0     | 0.02   |
| Smartbuy  | SSD                | 240 GB | 1       | 6     | 0     | 0.02   |
| GeIL      | Zenith A3-PRO      | 240 GB | 1       | 6     | 0     | 0.02   |
| GeIL      | Zenith A3          | 120 GB | 1       | 6     | 0     | 0.02   |
| Plextor   | PX-G256M6e         | 256 GB | 1       | 5     | 0     | 0.02   |
| Samsung   | SSD 860 QVO        | 4 TB   | 1       | 5     | 0     | 0.02   |
| ADATA     | SU800              | 256 GB | 2       | 37    | 6     | 0.02   |
| Dell      | WR202KD128G E70... | 128 GB | 1       | 5     | 0     | 0.02   |
| BIWIN     | SSD                | 128 GB | 2       | 5     | 0     | 0.02   |
| SanDisk   | SD8SNAT128G1002    | 128 GB | 2       | 5     | 0     | 0.02   |
| BIWIN     | G2242              | 64 GB  | 1       | 5     | 0     | 0.01   |
| Intel     | SSDSC2KW128G8      | 128 GB | 2       | 5     | 0     | 0.01   |
| KingSpec  | MT-256             | 256 GB | 2       | 5     | 0     | 0.01   |
| Vaseky    | V800-128G          | 128 GB | 1       | 5     | 0     | 0.01   |
| Drevo     | X1 SSD             | 120 GB | 2       | 314   | 34    | 0.01   |
| MicroData | MD500 120G         | 120 GB | 1       | 5     | 0     | 0.01   |
| Mushkin   | MKNSSDCG480GB      | 480 GB | 2       | 116   | 508   | 0.01   |
| Smartbuy  | m.2 S11-2280       | 128 GB | 1       | 4     | 0     | 0.01   |
| KingSpec  | ACSC2M128S25       | 128 GB | 1       | 4     | 0     | 0.01   |
| GeIL      | ZENITH R3_120GB    | 120 GB | 2       | 4     | 0     | 0.01   |
| Samsung   | SSD 850 EVO M.2    | 1 TB   | 1       | 4     | 0     | 0.01   |
| SanDisk   | SD8SB8U512G1122    | 512 GB | 1       | 4     | 0     | 0.01   |
| Transcend | TS64GSSD370        | 64 GB  | 2       | 4     | 0     | 0.01   |
| Crucial   | CT525MX300SSD4     | 528 GB | 1       | 4     | 0     | 0.01   |
| Corsair   | Neutron GTX SSD    | 128 GB | 1       | 1173  | 291   | 0.01   |
| GeIL      | ZENITH-A3 120G     | 120 GB | 1       | 3     | 0     | 0.01   |
| Intenso   | SSD Sata III       | 256 GB | 1       | 3     | 0     | 0.01   |
| Intel     | SSDSA1M080G2LE     | 80 GB  | 1       | 121   | 31    | 0.01   |
| Samsung   | MZ7LN128HAHQ-000L1 | 128 GB | 1       | 3     | 0     | 0.01   |
| SanDisk   | SATAIII            | 16 GB  | 1       | 3     | 0     | 0.01   |
| Crucial   | CT512M550SSD1      | 512 GB | 1       | 63    | 16    | 0.01   |
| KingSpec  | ACJC2M032mSH       | 32 GB  | 1       | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | 1       | 3     | 0     | 0.01   |
| Toshiba   | Q300               | 240 GB | 1       | 32    | 8     | 0.01   |
| FORESEE   | 128GB SSD          | 128 GB | 2       | 3     | 0     | 0.01   |
| Intel     | SSDSC2KW512G8      | 512 GB | 1       | 3     | 0     | 0.01   |
| OCZ       | REVODRIVE X2       | 64 GB  | 4       | 1204  | 516   | 0.01   |
| SPCC      | SSDB27             | 32 GB  | 1       | 23    | 6     | 0.01   |
| ShineDisk | M667 128G          | 120 GB | 1       | 3     | 0     | 0.01   |
| i-Flas... | K8                 | 63 GB  | 1       | 3     | 0     | 0.01   |
| Apacer    | 16GB SATA Flash... | 16 GB  | 1       | 33    | 10    | 0.01   |
| Gigabyte  | GP-GSTFS31120GNTD  | 120 GB | 1       | 3     | 0     | 0.01   |
| Transcend | TS256GSSD360S      | 256 GB | 2       | 2     | 0     | 0.01   |
| Samsung   | MZ7LN256HMJP-000H1 | 256 GB | 2       | 2     | 0     | 0.01   |
| Goodram   | GOODRAM IR_SSDP... | 120 GB | 1       | 2     | 0     | 0.01   |
| SanDisk   | SSD i110           | 128 GB | 1       | 2     | 0     | 0.01   |
| Dell      | WR202KD032G E70... | 31 GB  | 1       | 2     | 0     | 0.01   |
| Samsung   | SSD 860 EVO mSATA  | 500 GB | 1       | 2     | 0     | 0.01   |
| Fordisk   | SATA SSD           | 120 GB | 1       | 2     | 0     | 0.01   |
| Kingch... | SSD                | 64 GB  | 1       | 2     | 0     | 0.01   |
| KingSpec  | NT-512             | 512 GB | 1       | 24    | 8     | 0.01   |
| Dell      | WR202KD032G E70... | 31 GB  | 1       | 2     | 0     | 0.01   |
| Samsung   | MZNLN512HCJH-000H1 | 512 GB | 1       | 2     | 0     | 0.01   |
| SanDisk   | SDSA5GK-016G-1006  | 16 GB  | 1       | 276   | 106   | 0.01   |
| Gigastone | SS6200-256GB       | 256 GB | 1       | 2     | 0     | 0.01   |
| Intel     | SSDSC2KW240H6      | 240 GB | 2       | 14    | 6     | 0.01   |
| SanDisk   | SSD i110           | 16 GB  | 2       | 2     | 0     | 0.01   |
| SanDisk   | SSD i110           | 24 GB  | 1       | 2     | 0     | 0.01   |
| Transcend | TS64GMTS800        | 64 GB  | 1       | 2     | 0     | 0.01   |
| OCZ       | VERTEX3 LP         | 240 GB | 1       | 2268  | 1019  | 0.01   |
| ACPI      | M2SCF-6M           | 63 GB  | 1       | 2     | 0     | 0.01   |
| SK hynix  | SH920 2.5 7MM      | 128 GB | 1       | 54    | 25    | 0.01   |
| Seagate   | BarraCuda SSD Z... | 2 TB   | 1       | 2     | 0     | 0.01   |
| SanDisk   | SDSSDH120GG25      | 120 GB | 1       | 112   | 55    | 0.01   |
| Goodram   | GOODRAM SSDPR-C... | 240 GB | 1       | 2     | 0     | 0.01   |
| Lenovo    | SSD SL700 120G     | 120 GB | 1       | 2     | 0     | 0.01   |
| China     | SSD60G             | 64 GB  | 2       | 1     | 0     | 0.01   |
| Intel     | SSDSC2BP480G4      | 480 GB | 1       | 1     | 0     | 0.01   |
| Transcend | TS32GMSA370        | 32 GB  | 1       | 1     | 0     | 0.01   |
| Gigabyte  | GP-GSTFS31240GNTD  | 240 GB | 2       | 1     | 0     | 0.01   |
| Kingston  | SKC300S37A240G     | 240 GB | 2       | 1     | 0     | 0.01   |

HGST Hard Drives
================

This is a list of all tested HGST hard drive models and their ratings. See more
info on reliability test in the [README](https://github.com/linuxhw/SMART).

Contents
--------

1. [ HDD by Model  ](#hdd-by-model)
2. [ HDD by Family ](#hdd-by-family)

HDD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | Rating |
|-----------|--------------------|--------|---------|-------|-------|--------|
| HGST      | HUS724020ALE640    | 2 TB   | 7       | 1736  | 0     | 4.76   |
| HGST      | HTE541515A9E630    | 1.5 TB | 1       | 1702  | 0     | 4.66   |
| HGST      | MB6000GEBTP        | 6 TB   | 1       | 1046  | 0     | 2.87   |
| HGST      | HDN724040ALE640    | 4 TB   | 8       | 906   | 0     | 2.48   |
| HGST      | HTE541010A9E680    | 1 TB   | 2       | 913   | 509   | 2.43   |
| HGST      | HUS726040ALA610    | 4 TB   | 2       | 810   | 0     | 2.22   |
| HGST      | HDN726060ALE610    | 6 TB   | 1       | 787   | 0     | 2.16   |
| HGST      | HUS724030ALA640    | 3 TB   | 6       | 896   | 4     | 1.95   |
| HGST      | HUH721212ALN600    | 12 TB  | 1       | 534   | 0     | 1.46   |
| HGST      | HDN726060ALE614    | 6 TB   | 2       | 490   | 0     | 1.34   |
| HGST      | HUS726040ALE611    | 4 TB   | 1       | 401   | 0     | 1.10   |
| HGST      | HUS722T2TALA604    | 2 TB   | 1       | 395   | 0     | 1.08   |
| HGST      | HUS724020ALA640    | 2 TB   | 6       | 335   | 0     | 0.92   |
| HGST      | HCC545050A7E380    | 500 GB | 1       | 523   | 1     | 0.72   |
| HGST      | HTS545032A7E680    | 320 GB | 14      | 309   | 21    | 0.71   |
| HGST      | HTS721075A9E630    | 752 GB | 6       | 240   | 0     | 0.66   |
| HGST      | HDN724030ALE640    | 3 TB   | 2       | 216   | 0     | 0.59   |
| HGST      | HTS541075A9E680    | 752 GB | 22      | 278   | 418   | 0.56   |
| HGST      | HTS721010A9E630    | 1 TB   | 131     | 243   | 81    | 0.55   |
| HGST      | HTS541010A9E680    | 1 TB   | 119     | 293   | 261   | 0.52   |
| HGST      | HUS724040ALA640    | 4 TB   | 1       | 180   | 0     | 0.49   |
| HGST      | HTS725032A7E630    | 320 GB | 7       | 211   | 439   | 0.45   |
| HGST      | HTS725050A7E630    | 500 GB | 98      | 222   | 120   | 0.44   |
| HGST      | HTS545050A7E380    | 500 GB | 115     | 312   | 242   | 0.40   |
| HGST      | HUS726040ALE614    | 4 TB   | 1       | 147   | 0     | 0.40   |
| HGST      | HTS545032A7E380    | 320 GB | 18      | 234   | 62    | 0.37   |
| HGST      | HTS541010A7E630    | 1 TB   | 11      | 132   | 1     | 0.33   |
| HGST      | HTS545050A7E660    | 500 GB | 10      | 181   | 4     | 0.27   |
| HGST      | HTS545050A7E680    | 500 GB | 172     | 168   | 342   | 0.23   |
| HGST      | HUS726T4TALN6L4    | 4 TB   | 1       | 83    | 0     | 0.23   |
| HGST      | HTE725050A7E630    | 500 GB | 2       | 68    | 0     | 0.19   |
| HGST      | HTS541075A7E630    | 752 GB | 7       | 90    | 145   | 0.17   |
| HGST      | HTS545025A7E330    | 250 GB | 1       | 60    | 0     | 0.17   |
| HGST      | HTE721010A9E630    | 1 TB   | 3       | 50    | 0     | 0.14   |
| HGST      | HTS541010B7E610    | 1 TB   | 10      | 46    | 0     | 0.13   |
| HGST      | HTS545050B7E660    | 500 GB | 13      | 45    | 0     | 0.12   |
| HGST      | HTS541515A9E630    | 1.5 TB | 2       | 541   | 14    | 0.10   |
| HGST      | HTS541050A9E680    | 500 GB | 3       | 33    | 0     | 0.09   |
| HGST      | HUS726020ALE614    | 2 TB   | 1       | 31    | 0     | 0.09   |
| HGST      | HUS722T1TALA604    | 1 TB   | 3       | 15    | 0     | 0.04   |
| HGST      | TPH00100500GB 0... | 500 GB | 1       | 8     | 10    | 0.00   |
| HGST      | HTS725050B7E630    | 500 GB | 2       | 0     | 0     | 0.00   |
| HGST      | HUS726020ALA610    | 2 TB   | 1       | 0     | 0     | 0.00   |

HDD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | Rating |
|-----------|------------------------|--------|---------|-------|-------|--------|
| HGST      | Ultrastar 7K4000       | 4      | 20      | 986   | 2     | 2.55   |
| HGST      | Deskstar NAS           | 4      | 13      | 726   | 0     | 1.99   |
| HGST      | Travelstar 5K1500      | 2      | 3       | 928   | 10    | 1.62   |
| HGST      | Ultrastar DC HC520 ... | 1      | 1       | 534   | 0     | 1.46   |
| HGST      | Ultrastar 7K6000       | 7      | 8       | 416   | 0     | 1.14   |
| HGST      | CinemaStar Z5K500      | 1      | 1       | 523   | 1     | 0.72   |
| HGST      | Travelstar 7K1000      | 3      | 140     | 239   | 76    | 0.54   |
| HGST      | Travelstar 5K1000      | 5      | 147     | 292   | 280   | 0.54   |
| HGST      | Travelstar Z7K500      | 3      | 107     | 219   | 139   | 0.43   |
| HGST      | Travelstar Z5K500      | 5      | 329     | 228   | 268   | 0.32   |
| HGST      | Ultrastar 7K2          | 2      | 4       | 110   | 0     | 0.30   |
| HGST      | Travelstar Z5K1000     | 2      | 18      | 116   | 57    | 0.27   |
| HGST      | Travelstar Z5K1        | 1      | 10      | 46    | 0     | 0.13   |
| HGST      | Travelstar Z5K500.B    | 1      | 13      | 45    | 0     | 0.12   |
| HGST      | Unknown                | 1      | 1       | 8     | 10    | 0.00   |
| HGST      | Travelstar Z7K500.B    | 1      | 2       | 0     | 0     | 0.00   |

Toshiba Hard Drives
===================

This is a list of all tested Toshiba hard drive models and their ratings. See more
info on reliability test in the [README](https://github.com/linuxhw/SMART).

Contents
--------

1. [ HDD by Model  ](#hdd-by-model)
2. [ HDD by Family ](#hdd-by-family)

HDD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | Rating |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Toshiba   | MK8017GSG          | 80 GB  | 1       | 1162  | 0     | 3.18   |
| Toshiba   | MG03ACA200         | 2 TB   | 1       | 1134  | 0     | 3.11   |
| Toshiba   | MC04ACA200E        | 2 TB   | 1       | 1008  | 0     | 2.76   |
| Toshiba   | MQ01ABF050H        | 500 GB | 2       | 873   | 0     | 2.39   |
| Toshiba   | DT01ABA050V        | 500 GB | 1       | 644   | 0     | 1.77   |
| Toshiba   | DT01ABA200V        | 2 TB   | 1       | 639   | 0     | 1.75   |
| Toshiba   | MK1032GSX          | 100 GB | 4       | 579   | 0     | 1.59   |
| Toshiba   | MQ01ABC150         | 1.5 TB | 1       | 574   | 0     | 1.57   |
| Toshiba   | MK2559GSXP         | 250 GB | 3       | 506   | 0     | 1.39   |
| Toshiba   | MK2556GSYF         | 250 GB | 1       | 503   | 0     | 1.38   |
| Toshiba   | HDWE150            | 5 TB   | 1       | 454   | 0     | 1.25   |
| Toshiba   | MK1234GAX          | 120 GB | 2       | 448   | 0     | 1.23   |
| Toshiba   | MK1031GAS          | 100 GB | 2       | 647   | 4     | 1.10   |
| Toshiba   | MK3261GSYG         | 320 GB | 1       | 354   | 0     | 0.97   |
| Toshiba   | MK8032GSX          | 80 GB  | 8       | 464   | 161   | 0.96   |
| Toshiba   | MK6465GSXN         | 640 GB | 6       | 529   | 197   | 0.95   |
| Toshiba   | MQ03UBB200         | 2 TB   | 2       | 339   | 0     | 0.93   |
| Toshiba   | MK6459GSXP         | 640 GB | 8       | 421   | 373   | 0.90   |
| Toshiba   | DT01ACA300         | 3 TB   | 34      | 375   | 61    | 0.89   |
| Toshiba   | DT01ACA200         | 2 TB   | 67      | 343   | 61    | 0.87   |
| Toshiba   | MK1234GSX          | 120 GB | 7       | 600   | 5     | 0.83   |
| Toshiba   | MK3029GAC          | 32 GB  | 1       | 287   | 0     | 0.79   |
| Toshiba   | MK4025GAS          | 40 GB  | 1       | 571   | 1     | 0.78   |
| Toshiba   | HDWA130            | 3 TB   | 1       | 285   | 0     | 0.78   |
| Toshiba   | MK4058GSX          | 400 GB | 3       | 452   | 3     | 0.78   |
| Toshiba   | MK6461GSY          | 640 GB | 3       | 281   | 13    | 0.77   |
| Toshiba   | MQ01UBD100         | 1 TB   | 3       | 274   | 0     | 0.75   |
| Toshiba   | MQ01ABD075         | 752 GB | 73      | 336   | 8     | 0.73   |
| Toshiba   | MK5075GSX          | 500 GB | 10      | 378   | 238   | 0.72   |
| Toshiba   | DT01ACA100         | 1 TB   | 179     | 290   | 12    | 0.71   |
| Toshiba   | DT01ABA200         | 2 TB   | 2       | 254   | 0     | 0.70   |
| Toshiba   | MD04ACA400         | 4 TB   | 1       | 253   | 0     | 0.69   |
| Toshiba   | MK3263GSX          | 320 GB | 5       | 618   | 20    | 0.68   |
| Toshiba   | MQ01UBB200         | 2 TB   | 1       | 242   | 0     | 0.66   |
| Toshiba   | MK6034GAX          | 64 GB  | 2       | 879   | 5     | 0.65   |
| Toshiba   | MK2035GSS          | 200 GB | 10      | 478   | 27    | 0.64   |
| Toshiba   | Generic L200 Ha... | 2 TB   | 2       | 229   | 0     | 0.63   |
| Toshiba   | MK5076GSXN         | 500 GB | 1       | 224   | 0     | 0.62   |
| Toshiba   | MQ01ABD032         | 320 GB | 61      | 286   | 64    | 0.61   |
| Toshiba   | MK5055GSXN         | 500 GB | 2       | 392   | 4     | 0.59   |
| Toshiba   | DT01ACA050         | 500 GB | 234     | 243   | 28    | 0.58   |
| Toshiba   | HDWJ110            | 1 TB   | 3       | 207   | 0     | 0.57   |
| Toshiba   | MK5065GSXF         | 500 GB | 7       | 340   | 5     | 0.53   |
| Toshiba   | MK2555GSXF         | 250 GB | 4       | 192   | 0     | 0.53   |
| Toshiba   | MQ02ABD100H        | 1 TB   | 5       | 185   | 0     | 0.51   |
| Toshiba   | HDWE140            | 4 TB   | 7       | 184   | 0     | 0.51   |
| Toshiba   | MG03ACA300         | 3 TB   | 2       | 322   | 5     | 0.50   |
| Toshiba   | MK8009GAH          | 80 GB  | 2       | 360   | 43    | 0.49   |
| Toshiba   | MQ01ABD100         | 1 TB   | 161     | 219   | 49    | 0.49   |
| Toshiba   | MK8046GSX          | 80 GB  | 2       | 299   | 2     | 0.47   |
| Toshiba   | MK8032GAX          | 80 GB  | 1       | 166   | 0     | 0.46   |
| Toshiba   | MK8007GAH          | 80 GB  | 2       | 210   | 7     | 0.44   |
| Toshiba   | MK3265GSXN         | 320 GB | 12      | 428   | 298   | 0.44   |
| Toshiba   | MQ01ABD050V        | 500 GB | 1       | 157   | 0     | 0.43   |
| Toshiba   | MK3276GSXN         | 320 GB | 1       | 156   | 0     | 0.43   |
| Toshiba   | MK1655GSX          | 160 GB | 11      | 331   | 33    | 0.41   |
| Toshiba   | MD03ACA400V        | 4 TB   | 1       | 1037  | 6     | 0.41   |
| Toshiba   | MK2555GSX          | 250 GB | 24      | 402   | 91    | 0.40   |
| Toshiba   | HDWN160            | 6 TB   | 1       | 143   | 0     | 0.39   |
| Toshiba   | MK5059GSXP         | 500 GB | 24      | 389   | 467   | 0.39   |
| Toshiba   | MG03ACA100         | 1 TB   | 8       | 165   | 1     | 0.39   |
| Toshiba   | HDWA120            | 2 TB   | 3       | 138   | 0     | 0.38   |
| Toshiba   | MK8037GSX          | 80 GB  | 15      | 456   | 158   | 0.37   |
| Toshiba   | HDWD110            | 1 TB   | 74      | 140   | 7     | 0.37   |
| Toshiba   | MK6475GSX          | 640 GB | 17      | 300   | 395   | 0.37   |
| Toshiba   | HDWD120            | 2 TB   | 11      | 135   | 0     | 0.37   |
| Toshiba   | MK7575GSX          | 752 GB | 17      | 507   | 585   | 0.37   |
| Toshiba   | MK5076GSX -63      | 500 GB | 2       | 134   | 0     | 0.37   |
| Toshiba   | MQ01ABD050         | 500 GB | 82      | 304   | 327   | 0.37   |
| Toshiba   | MQ01ABF032         | 320 GB | 17      | 180   | 61    | 0.37   |
| Toshiba   | MK7559GSXP         | 752 GB | 13      | 428   | 411   | 0.36   |
| Toshiba   | MQ01ACF050         | 500 GB | 6       | 157   | 173   | 0.36   |
| Toshiba   | MK1034GSX          | 100 GB | 4       | 418   | 464   | 0.35   |
| Toshiba   | MK1216GSG          | 120 GB | 3       | 272   | 4     | 0.34   |
| Toshiba   | MK3252GSX          | 320 GB | 16      | 573   | 246   | 0.34   |
| Toshiba   | MK3259GSXP         | 320 GB | 42      | 294   | 122   | 0.33   |
| Toshiba   | MK6025GAS          | 64 GB  | 2       | 166   | 9     | 0.33   |
| Toshiba   | MK1633GSG          | 160 GB | 2       | 116   | 0     | 0.32   |
| Toshiba   | MQ01ABF050         | 500 GB | 210     | 138   | 86    | 0.32   |
| Toshiba   | MK2546GSX          | 250 GB | 12      | 520   | 41    | 0.32   |
| Toshiba   | MK1237GSX          | 120 GB | 10      | 353   | 28    | 0.32   |
| Toshiba   | MK2565GSX          | 250 GB | 15      | 250   | 223   | 0.31   |
| Toshiba   | MK8034GSX          | 80 GB  | 8       | 324   | 38    | 0.30   |
| Toshiba   | MK1637GSX          | 160 GB | 26      | 517   | 59    | 0.30   |
| Toshiba   | HDWD130            | 3 TB   | 5       | 108   | 0     | 0.30   |
| Toshiba   | MK3276GSX -63      | 320 GB | 5       | 178   | 1     | 0.29   |
| Toshiba   | MD04ACA500         | 5 TB   | 2       | 105   | 0     | 0.29   |
| Toshiba   | MK1059GSMP         | 1 TB   | 12      | 472   | 308   | 0.29   |
| Toshiba   | MK6006GAH          | 64 GB  | 1       | 103   | 0     | 0.28   |
| Toshiba   | HDWN180            | 8 TB   | 2       | 99    | 0     | 0.27   |
| Toshiba   | MG04ACA200E        | 2 TB   | 3       | 105   | 3     | 0.27   |
| Toshiba   | MK3265GSX          | 320 GB | 48      | 595   | 248   | 0.26   |
| Toshiba   | MK5065GSX          | 500 GB | 14      | 312   | 217   | 0.26   |
| Toshiba   | MK1665GSX H        | 160 GB | 1       | 93    | 0     | 0.26   |
| Toshiba   | MK1246GSX          | 120 GB | 13      | 396   | 53    | 0.25   |
| Toshiba   | MK6465GSX          | 640 GB | 19      | 624   | 706   | 0.24   |
| Toshiba   | MQ04UBF100         | 1 TB   | 2       | 83    | 0     | 0.23   |
| Toshiba   | MK3021GAS          | 32 GB  | 1       | 495   | 5     | 0.23   |
| Toshiba   | HDWD105            | 500 GB | 43      | 84    | 46    | 0.21   |
| Toshiba   | MG04ACA400E        | 4 TB   | 2       | 76    | 0     | 0.21   |
| Toshiba   | MK6476GSXN         | 640 GB | 3       | 122   | 601   | 0.21   |
| Toshiba   | MK3263GSXN         | 320 GB | 7       | 558   | 26    | 0.19   |
| Toshiba   | MK1011GAH          | 100 GB | 4       | 378   | 9     | 0.18   |
| Toshiba   | MK1665GSX          | 160 GB | 14      | 221   | 250   | 0.17   |
| Toshiba   | MK1646GSX          | 160 GB | 6       | 564   | 68    | 0.16   |
| Toshiba   | MK2552GSX          | 250 GB | 6       | 507   | 43    | 0.14   |
| Toshiba   | MK1629GSG          | 160 GB | 2       | 409   | 9     | 0.14   |
| Toshiba   | MK3265GSXF         | 320 GB | 3       | 155   | 5     | 0.14   |
| Toshiba   | MK1059GSM          | 1 TB   | 18      | 342   | 992   | 0.13   |
| Toshiba   | MK4055GSX          | 400 GB | 2       | 285   | 3     | 0.13   |
| Toshiba   | HDWJ105            | 500 GB | 4       | 46    | 0     | 0.13   |
| Toshiba   | MK3265GSX H        | 320 GB | 1       | 45    | 0     | 0.12   |
| Toshiba   | MQ01ABF050M        | 500 GB | 4       | 44    | 0     | 0.12   |
| Toshiba   | MK6008GAH          | 64 GB  | 2       | 399   | 8     | 0.12   |
| Toshiba   | MK1652GSX          | 160 GB | 12      | 369   | 74    | 0.12   |
| Toshiba   | MK3276GSX H        | 320 GB | 1       | 43    | 0     | 0.12   |
| Toshiba   | MK5056GSY          | 500 GB | 2       | 204   | 2     | 0.12   |
| Toshiba   | HDWL110            | 1 TB   | 1       | 42    | 0     | 0.12   |
| Toshiba   | MK3275GSX          | 320 GB | 11      | 216   | 254   | 0.11   |
| Toshiba   | HDWQ140            | 4 TB   | 2       | 39    | 0     | 0.11   |
| Toshiba   | MK1252GSX          | 120 GB | 6       | 332   | 128   | 0.11   |
| Toshiba   | MK2046GSX          | 200 GB | 9       | 408   | 51    | 0.10   |
| Toshiba   | MQ01UBD050         | 500 GB | 1       | 35    | 0     | 0.10   |
| Toshiba   | MQ04ABF100         | 1 TB   | 13      | 34    | 0     | 0.10   |
| Toshiba   | MK5055GSX          | 500 GB | 7       | 553   | 200   | 0.09   |
| Toshiba   | HDWM105            | 500 GB | 1       | 32    | 0     | 0.09   |
| Toshiba   | MQ03UBB300         | 3 TB   | 2       | 35    | 4     | 0.09   |
| Toshiba   | MQ01ABD100M        | 1 TB   | 1       | 27    | 0     | 0.08   |
| Toshiba   | MK5059GSX          | 500 GB | 1       | 932   | 36    | 0.07   |
| Toshiba   | HDWK105            | 500 GB | 2       | 24    | 0     | 0.07   |
| Toshiba   | MK6032GAX          | 64 GB  | 1       | 23    | 0     | 0.06   |
| Toshiba   | MK6465GSXW         | 640 GB | 1       | 552   | 23    | 0.06   |
| Toshiba   | MK2529GSG          | 250 GB | 2       | 313   | 1127  | 0.06   |
| Toshiba   | MK1032GAX          | 100 GB | 2       | 108   | 4     | 0.06   |
| Toshiba   | MK5076GSX          | 500 GB | 25      | 52    | 192   | 0.05   |
| Toshiba   | MK6034GSX          | 64 GB  | 3       | 320   | 23    | 0.04   |
| Toshiba   | MK2565GSXN         | 250 GB | 1       | 502   | 36    | 0.04   |
| Toshiba   | MK2565GSXV         | 250 GB | 2       | 275   | 34    | 0.03   |
| Toshiba   | HDWM110            | 1 TB   | 2       | 11    | 0     | 0.03   |
| Toshiba   | MK1656GSY          | 160 GB | 2       | 190   | 9     | 0.03   |
| Toshiba   | MK2016GAP          | 20 GB  | 1       | 349   | 36    | 0.03   |
| Toshiba   | MQ01ACF032         | 320 GB | 1       | 6     | 0     | 0.02   |
| Toshiba   | MK1214GAH          | 120 GB | 2       | 28    | 209   | 0.02   |
| Toshiba   | MK8052GSX          | 80 GB  | 3       | 180   | 58    | 0.02   |
| Toshiba   | MQ02ABF050H-SSH... | 500 GB | 1       | 5     | 0     | 0.01   |
| Toshiba   | MK2576GSX          | 250 GB | 1       | 4     | 0     | 0.01   |
| Toshiba   | MK5061GSYN         | 500 GB | 11      | 104   | 278   | 0.01   |
| Toshiba   | MK7559GSXF         | 752 GB | 2       | 117   | 637   | 0.01   |
| Toshiba   | MK6459GSX          | 640 GB | 4       | 481   | 839   | 0.01   |
| Toshiba   | MK3259GSX          | 320 GB | 1       | 192   | 82    | 0.01   |
| Toshiba   | MK6476GSX          | 640 GB | 11      | 6     | 202   | 0.01   |
| Toshiba   | MK3276GSX          | 320 GB | 18      | 6     | 225   | 0.01   |
| Toshiba   | MQ02ABF100         | 1 TB   | 1       | 1     | 0     | 0.01   |
| Toshiba   | MK2556GSY          | 250 GB | 3       | 6     | 352   | 0.00   |
| Toshiba   | MK2533GSG          | 250 GB | 1       | 1671  | 948   | 0.00   |
| Toshiba   | MK3261GSYN         | 320 GB | 5       | 7     | 169   | 0.00   |
| Toshiba   | MK5065GSXN         | 500 GB | 6       | 521   | 976   | 0.00   |
| Toshiba   | MK7559GSM          | 752 GB | 1       | 1015  | 847   | 0.00   |
| Toshiba   | MK5061GSY          | 500 GB | 1       | 1     | 0     | 0.00   |
| Toshiba   | MK3256GSY          | 320 GB | 4       | 5     | 282   | 0.00   |
| Toshiba   | MK8025GAS          | 80 GB  | 1       | 22    | 58    | 0.00   |
| Toshiba   | MQ01ABB200         | 2 TB   | 1       | 152   | 704   | 0.00   |
| Toshiba   | MK8025GAL          | 80 GB  | 1       | 78    | 1017  | 0.00   |
| Toshiba   | MK1676GSX          | 160 GB | 1       | 22    | 893   | 0.00   |

HDD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | Rating |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Toshiba   | 1.8" HDD MK..17GSG     | 1      | 1       | 1162  | 0     | 3.18   |
| Toshiba   | 3.5" HDD               | 1      | 1       | 1008  | 0     | 2.76   |
| Toshiba   | 2.5" HDD MQ01ABF..H    | 1      | 2       | 873   | 0     | 2.39   |
| Toshiba   | 3.5" HDD DT01ABA..V    | 2      | 2       | 642   | 0     | 1.76   |
| Toshiba   | 2.5" HDD MK..32GSX     | 1      | 4       | 579   | 0     | 1.59   |
| Toshiba   | 2.5" HDD MQ01ABC       | 1      | 1       | 574   | 0     | 1.57   |
| Toshiba   | 2.5" HDD MK..59GSXP... | 1      | 3       | 506   | 0     | 1.39   |
| Toshiba   | 2.5" HDD MK..61GSY     | 1      | 1       | 354   | 0     | 0.97   |
| Toshiba   | 2.5" HDD MK..58GSX     | 1      | 3       | 452   | 3     | 0.78   |
| Toshiba   | 3.5" DT01ABA Deskto... | 1      | 2       | 254   | 0     | 0.70   |
| Toshiba   | 3.5" HDD DT01ACA       | 4      | 514     | 281   | 29    | 0.68   |
| Toshiba   | 2.5" HDD MQ01UBB       | 1      | 1       | 242   | 0     | 0.66   |
| Toshiba   | 2.5" HDD               | 15     | 44      | 456   | 41    | 0.66   |
| Toshiba   | 3.5" MG03ACAxxx(Y) ... | 3      | 11      | 281   | 2     | 0.65   |
| Toshiba   | X300                   | 2      | 8       | 218   | 0     | 0.60   |
| Toshiba   | 2.5" HDD MQ01UBD       | 2      | 4       | 214   | 0     | 0.59   |
| Toshiba   | 2.5" HDD MK..55GSX     | 1      | 2       | 392   | 4     | 0.59   |
| Toshiba   | 2.5" HDD MQ01ABD       | 5      | 378     | 271   | 104   | 0.53   |
| Toshiba   | 2.5" HDD MQ02ABD..H    | 1      | 5       | 185   | 0     | 0.51   |
| Toshiba   | 2.5" HDD MQ03UBB       | 2      | 4       | 187   | 2     | 0.51   |
| Toshiba   | 3.5" HDD E300          | 2      | 4       | 175   | 0     | 0.48   |
| Toshiba   | L200                   | 2      | 3       | 166   | 0     | 0.46   |
| Toshiba   | 3.5" MD04ACA Enterp... | 2      | 3       | 155   | 0     | 0.42   |
| Toshiba   | Enterprise Surveill... | 1      | 1       | 1037  | 6     | 0.41   |
| Toshiba   | 2.5" HDD MK..63GSX     | 2      | 12      | 583   | 23    | 0.39   |
| Toshiba   | 2.5" HDD MK..75GSX     | 4      | 55      | 361   | 397   | 0.38   |
| Toshiba   | 2.5" HDD MK..59GSXP    | 7      | 93      | 362   | 303   | 0.38   |
| Toshiba   | 2.5" HDD MK..37GSX     | 1      | 15      | 456   | 158   | 0.37   |
| Toshiba   | 2.5" HDD MK..55GSX     | 5      | 48      | 385   | 82    | 0.36   |
| Toshiba   | 1.8" HDD MK..16GSG     | 1      | 3       | 272   | 4     | 0.34   |
| Toshiba   | 1.8" HDD               | 4      | 7       | 292   | 17    | 0.34   |
| Toshiba   | 2.5" HDD MQ01ABF       | 2      | 21      | 154   | 49    | 0.32   |
| Toshiba   | 2.5" HDD MK..34GSX     | 2      | 12      | 356   | 180   | 0.32   |
| Toshiba   | P300                   | 4      | 133     | 120   | 19    | 0.32   |
| Toshiba   | 2.5" HDD MQ01ABF       | 1      | 210     | 138   | 86    | 0.32   |
| Toshiba   | 2.5" HDD L200          | 2      | 7       | 115   | 0     | 0.32   |
| Toshiba   | N300                   | 2      | 3       | 113   | 0     | 0.31   |
| Toshiba   | 2.5" HDD MK..76GSX     | 6      | 13      | 150   | 139   | 0.31   |
| Toshiba   | 2.5" HDD MQ01ACF       | 2      | 7       | 135   | 149   | 0.31   |
| Toshiba   | 2.5" HDD MK..37GSX     | 2      | 36      | 471   | 50    | 0.31   |
| Toshiba   | 2.5" HDD MK..65GSX     | 13     | 147     | 453   | 312   | 0.30   |
| Toshiba   | 2.5" HDD MK..59GSM     | 1      | 12      | 472   | 308   | 0.29   |
| Toshiba   | 2.5" HDD MK..46GSX     | 4      | 33      | 466   | 48    | 0.27   |
| Toshiba   | 3.5" MG04ACA Enterp... | 2      | 5       | 93    | 2     | 0.24   |
| Toshiba   | 2.5" HDD MQ04UBF       | 1      | 2       | 83    | 0     | 0.23   |
| Toshiba   | 1.8" HDD MK..33GSG     | 2      | 3       | 634   | 316   | 0.21   |
| Toshiba   | 2.5" HDD MK..52GSX     | 5      | 43      | 446   | 140   | 0.20   |
| Toshiba   | 2.5" HDD MK..56GSY     | 5      | 12      | 110   | 184   | 0.14   |
| Toshiba   | 2.5" HDD MK..59GSM     | 2      | 19      | 378   | 984   | 0.13   |
| Toshiba   | 2.5" HDD MK..61GSY[N]  | 4      | 20      | 101   | 197   | 0.12   |
| Toshiba   | 3.5" HDD N300          | 1      | 2       | 39    | 0     | 0.11   |
| Toshiba   | 1.8" HDD               | 3      | 7       | 235   | 210   | 0.11   |
| Toshiba   | 1.8" HDD MK..29GSG     | 2      | 4       | 361   | 568   | 0.10   |
| Toshiba   | 2.5" HDD MK..46GSX     | 1      | 9       | 408   | 51    | 0.10   |
| Toshiba   | 2.5" HDD MQ04ABF       | 1      | 13      | 34    | 0     | 0.10   |
| Toshiba   | 2.5" HDD MQ01ABD       | 1      | 1       | 27    | 0     | 0.08   |
| Toshiba   | 2.5" HDD L200 Slim     | 1      | 2       | 24    | 0     | 0.07   |
| Toshiba   | 2.5" HDD               | 1      | 1       | 23    | 0     | 0.06   |
| Toshiba   | 2.5" HDD H200          | 2      | 3       | 18    | 0     | 0.05   |
| Toshiba   | 2.5" HDD MK..65GSX     | 2      | 3       | 368   | 31    | 0.04   |
| Toshiba   | 2.5" HDD MK..76GSX     | 5      | 56      | 27    | 214   | 0.02   |
| Toshiba   | 2.5" HDD MQ02ABF..H    | 1      | 1       | 5     | 0     | 0.01   |
| Toshiba   | 2.5" HDD MK..59GSX     | 1      | 2       | 117   | 637   | 0.01   |
| Toshiba   | 2.5" HDD MQ02ABF       | 1      | 1       | 1     | 0     | 0.01   |
| Toshiba   | 2.5" HDD MQ01ABB       | 1      | 1       | 152   | 704   | 0.00   |

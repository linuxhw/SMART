Seagate Hard Drives
===================

This is a list of all tested Seagate hard drive models and their ratings. See more
info on reliability test in the [README](https://github.com/linuxhw/SMART).

Contents
--------

1. [ HDD by Model  ](#hdd-by-model)
2. [ HDD by Family ](#hdd-by-family)

HDD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | Rating |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Seagate   | ST3750641NS        | 752 GB | 1       | 2169  | 0     | 5.94   |
| Seagate   | ST3500641AS        | 500 GB | 1       | 1996  | 0     | 5.47   |
| Seagate   | ST960813AS         | 64 GB  | 1       | 1729  | 0     | 4.74   |
| Seagate   | ST3160828AS        | 160 GB | 1       | 1198  | 0     | 3.28   |
| Seagate   | ST3300631A         | 304 GB | 1       | 1185  | 0     | 3.25   |
| Seagate   | ST3320620NS        | 320 GB | 3       | 1312  | 1     | 3.20   |
| Seagate   | ST3250823AS        | 250 GB | 10      | 1321  | 25    | 3.16   |
| Seagate   | ST3400832AS        | 400 GB | 1       | 3418  | 2     | 3.12   |
| Seagate   | ST160LM003 HN-M... | 160 GB | 1       | 1139  | 0     | 3.12   |
| Seagate   | ST3320820AS_Q      | 320 GB | 1       | 1125  | 0     | 3.08   |
| Seagate   | ST9500620NS 81Y... | 500 GB | 1       | 1083  | 0     | 2.97   |
| Seagate   | ST3400620A         | 400 GB | 3       | 1802  | 61    | 2.88   |
| Seagate   | ST340823A          | 40 GB  | 1       | 1034  | 0     | 2.83   |
| Seagate   | ST320LT023-1AF142  | 320 GB | 1       | 1007  | 0     | 2.76   |
| Seagate   | ST3250820SCE       | 250 GB | 2       | 960   | 0     | 2.63   |
| Seagate   | ST250LT020-1AE14C  | 250 GB | 1       | 936   | 0     | 2.57   |
| Seagate   | ST3300622A         | 304 GB | 1       | 912   | 0     | 2.50   |
| Seagate   | ST3400833AS        | 400 GB | 1       | 905   | 0     | 2.48   |
| Seagate   | ST4000VN000-1H4168 | 4 TB   | 15      | 997   | 135   | 2.48   |
| Seagate   | ST3300831AS        | 304 GB | 2       | 1451  | 4     | 2.48   |
| Seagate   | ST3802110AS        | 80 GB  | 2       | 899   | 0     | 2.46   |
| Seagate   | ST91000640NS 81... | 1 TB   | 1       | 883   | 0     | 2.42   |
| Seagate   | ST360021A          | 64 GB  | 3       | 868   | 0     | 2.38   |
| Seagate   | ST3500630A         | 500 GB | 7       | 1381  | 33    | 2.33   |
| Seagate   | ST4000VN0001-1S... | 4 TB   | 1       | 836   | 0     | 2.29   |
| Seagate   | ST3750840AS        | 752 GB | 1       | 828   | 0     | 2.27   |
| Seagate   | ST3120213AS        | 120 GB | 1       | 806   | 0     | 2.21   |
| Seagate   | ST3320413AS        | 320 GB | 7       | 1178  | 4     | 2.21   |
| Seagate   | ST6000DM001-1XY17Z | 6 TB   | 1       | 783   | 0     | 2.15   |
| Seagate   | ST6000VN0021-1Z... | 6 TB   | 1       | 783   | 0     | 2.15   |
| Seagate   | ST91603110CS       | 160 GB | 1       | 781   | 0     | 2.14   |
| Seagate   | ST1000VX001-1Z4102 | 1 TB   | 1       | 725   | 0     | 1.99   |
| Seagate   | ST8000AS0002-1N... | 8 TB   | 1       | 718   | 0     | 1.97   |
| Seagate   | ST310211A          | 10 GB  | 1       | 716   | 0     | 1.96   |
| Seagate   | ST32000644NS       | 2 TB   | 2       | 716   | 1     | 1.96   |
| Seagate   | ST1000VM002-9ZL162 | 1 TB   | 1       | 709   | 0     | 1.95   |
| Seagate   | ST3250620NS        | 250 GB | 15      | 1016  | 327   | 1.91   |
| Seagate   | ST3300620AS        | 304 GB | 1       | 692   | 0     | 1.90   |
| Seagate   | ST2000VM003-1CT164 | 2 TB   | 4       | 845   | 1     | 1.89   |
| Seagate   | ST340215AS         | 40 GB  | 1       | 686   | 0     | 1.88   |
| Seagate   | ST31500541AS       | 1.5 TB | 8       | 918   | 46    | 1.87   |
| Seagate   | ST4000DM000-1F2168 | 4 TB   | 18      | 689   | 1     | 1.86   |
| Seagate   | ST4000VM000-1F3168 | 4 TB   | 5       | 663   | 0     | 1.82   |
| Seagate   | ST3320413CS        | 320 GB | 8       | 730   | 141   | 1.82   |
| Seagate   | ST3400620AS        | 400 GB | 21      | 1023  | 8     | 1.80   |
| Seagate   | ST3000DM003-1F216N | 3 TB   | 1       | 652   | 0     | 1.79   |
| Seagate   | ST980812AS         | 80 GB  | 1       | 647   | 0     | 1.77   |
| Seagate   | ST32000641AS       | 2 TB   | 9       | 821   | 179   | 1.77   |
| Seagate   | ST3320620AS        | 320 GB | 97      | 1087  | 251   | 1.77   |
| Seagate   | ST3160812AS        | 160 GB | 34      | 987   | 346   | 1.72   |
| Seagate   | ST9750422AS        | 752 GB | 2       | 618   | 0     | 1.69   |
| Seagate   | ST3320418AS        | 320 GB | 65      | 824   | 77    | 1.69   |
| Seagate   | ST31000526SV       | 1 TB   | 3       | 769   | 343   | 1.67   |
| Seagate   | ST980411ASG        | 80 GB  | 2       | 831   | 7     | 1.65   |
| Seagate   | ST9200420AS        | 200 GB | 2       | 601   | 0     | 1.65   |
| Seagate   | ST2000DL003-9VT166 | 2 TB   | 59      | 781   | 99    | 1.64   |
| Seagate   | ST3250820AS        | 250 GB | 33      | 897   | 289   | 1.64   |
| Seagate   | ST3160316AS        | 160 GB | 10      | 730   | 8     | 1.62   |
| Seagate   | ST1000DL002-9TT153 | 1 TB   | 25      | 976   | 363   | 1.61   |
| Seagate   | ST3500630AS        | 500 GB | 25      | 937   | 320   | 1.59   |
| Seagate   | ST340014A          | 40 GB  | 73      | 784   | 40    | 1.58   |
| Seagate   | ST3160212AS        | 160 GB | 2       | 572   | 0     | 1.57   |
| Seagate   | ST1000VX000-9YW162 | 1 TB   | 4       | 570   | 0     | 1.56   |
| Seagate   | ST31000525SV       | 1 TB   | 2       | 938   | 2     | 1.56   |
| Seagate   | ST3250620AS        | 250 GB | 44      | 892   | 378   | 1.54   |
| Seagate   | ST3160212A         | 160 GB | 9       | 802   | 90    | 1.53   |
| Seagate   | ST380815AS         | 80 GB  | 113     | 776   | 237   | 1.52   |
| Seagate   | ST32000645NS       | 2 TB   | 4       | 553   | 0     | 1.52   |
| Seagate   | ST3160815AS        | 160 GB | 138     | 790   | 400   | 1.51   |
| Seagate   | ST360015A          | 64 GB  | 2       | 1494  | 5     | 1.50   |
| Seagate   | ST3500320NS        | 500 GB | 13      | 920   | 433   | 1.50   |
| Seagate   | ST3200827A         | 200 GB | 1       | 544   | 0     | 1.49   |
| Seagate   | ST3200820AS        | 200 GB | 21      | 844   | 506   | 1.49   |
| Seagate   | ST3000VN000-1HJ166 | 3 TB   | 1       | 540   | 0     | 1.48   |
| Seagate   | ST4000NM0033-9Z... | 4 TB   | 2       | 538   | 0     | 1.47   |
| Seagate   | ST3750640NS        | 752 GB | 6       | 1275  | 524   | 1.45   |
| Seagate   | ST3320620A         | 320 GB | 21      | 685   | 5     | 1.44   |
| Seagate   | ST3160215ACE       | 160 GB | 3       | 809   | 72    | 1.41   |
| Seagate   | ST5000DM000-1FK178 | 5 TB   | 4       | 520   | 2     | 1.41   |
| Seagate   | ST640LM000 HM641JI | 640 GB | 2       | 511   | 0     | 1.40   |
| Seagate   | ST3120026A         | 120 GB | 19      | 881   | 161   | 1.40   |
| Seagate   | ST3000VX000-1CU166 | 3 TB   | 3       | 509   | 0     | 1.40   |
| Seagate   | ST3250624AS        | 250 GB | 12      | 787   | 110   | 1.39   |
| Seagate   | ST380011A          | 80 GB  | 139     | 785   | 92    | 1.39   |
| Seagate   | ST3160023A         | 160 GB | 16      | 827   | 320   | 1.37   |
| Seagate   | ST380012ACE        | 80 GB  | 4       | 498   | 0     | 1.37   |
| Seagate   | ST320LM000 HM321HI | 320 GB | 9       | 520   | 2     | 1.36   |
| Seagate   | ST3500630NS        | 500 GB | 5       | 684   | 529   | 1.36   |
| Seagate   | ST2000NM0033-9Z... | 2 TB   | 6       | 489   | 0     | 1.34   |
| Seagate   | ST98823A           | 80 GB  | 4       | 633   | 323   | 1.33   |
| Seagate   | ST9500420ASG       | 500 GB | 2       | 572   | 1     | 1.33   |
| Seagate   | ST1000VM002-1CT162 | 1 TB   | 3       | 484   | 0     | 1.33   |
| Seagate   | ST340015A          | 40 GB  | 5       | 835   | 33    | 1.32   |
| Seagate   | ST3750525AS        | 752 GB | 11      | 632   | 3     | 1.31   |
| Seagate   | ST9120821A         | 120 GB | 4       | 709   | 20    | 1.30   |
| Seagate   | ST3500413AS        | 500 GB | 100     | 617   | 70    | 1.29   |
| Seagate   | ST32000646NS       | 2 TB   | 1       | 467   | 0     | 1.28   |
| Seagate   | ST3808110AS        | 80 GB  | 29      | 916   | 418   | 1.27   |
| Seagate   | ST380012A          | 80 GB  | 1       | 456   | 0     | 1.25   |
| Seagate   | ST2000VX003-1HH164 | 2 TB   | 2       | 454   | 0     | 1.25   |
| Seagate   | ST3120026AS        | 120 GB | 23      | 1113  | 8     | 1.24   |
| Seagate   | ST380215AS         | 80 GB  | 19      | 865   | 707   | 1.24   |
| Seagate   | ST3250620A         | 250 GB | 12      | 670   | 53    | 1.24   |
| Seagate   | ST1000NM0011       | 1 TB   | 6       | 705   | 45    | 1.23   |
| Seagate   | ST380013AS         | 80 GB  | 29      | 1249  | 94    | 1.23   |
| Seagate   | ST3200820A         | 200 GB | 5       | 491   | 61    | 1.22   |
| Seagate   | ST3200826AS        | 200 GB | 11      | 839   | 87    | 1.22   |
| Seagate   | ST3000DM001-1ER166 | 3 TB   | 16      | 435   | 0     | 1.19   |
| Seagate   | ST380817AS         | 80 GB  | 37      | 685   | 8     | 1.19   |
| Seagate   | ST3160318AS        | 160 GB | 43      | 669   | 83    | 1.19   |
| Seagate   | ST1000DM005 HD1... | 1 TB   | 11      | 529   | 2     | 1.19   |
| Seagate   | ST3160215A         | 160 GB | 17      | 652   | 180   | 1.17   |
| Seagate   | ST3250312AS        | 250 GB | 18      | 649   | 59    | 1.16   |
| Seagate   | ST3750640AS        | 752 GB | 9       | 1073  | 318   | 1.16   |
| Seagate   | ST3320820AS        | 320 GB | 8       | 530   | 385   | 1.15   |
| Seagate   | ST3160812A         | 160 GB | 24      | 666   | 391   | 1.15   |
| Seagate   | ST3160815A         | 160 GB | 35      | 699   | 248   | 1.15   |
| Seagate   | ST3500411SV        | 500 GB | 2       | 694   | 37    | 1.13   |
| Seagate   | ST94813AS          | 40 GB  | 2       | 412   | 0     | 1.13   |
| Seagate   | ST3120814A         | 120 GB | 14      | 808   | 246   | 1.12   |
| Seagate   | ST3120022A         | 120 GB | 33      | 633   | 22    | 1.12   |
| Seagate   | ST3500418AS        | 500 GB | 280     | 738   | 190   | 1.11   |
| Seagate   | ST3000VX000-9YW166 | 3 TB   | 1       | 405   | 0     | 1.11   |
| Seagate   | ST2000LM003 HN-... | 2 TB   | 17      | 432   | 4     | 1.09   |
| Seagate   | ST5000LM000-2AN170 | 5 TB   | 1       | 395   | 0     | 1.08   |
| Seagate   | ST3250318AS        | 250 GB | 86      | 724   | 117   | 1.08   |
| Seagate   | ST320DM000-1BC14C  | 320 GB | 15      | 511   | 15    | 1.07   |
| Seagate   | ST2000DM001-1CH164 | 2 TB   | 90      | 495   | 133   | 1.06   |
| Seagate   | ST3802110A         | 80 GB  | 33      | 651   | 472   | 1.06   |
| Seagate   | ST31000520AS       | 1 TB   | 12      | 985   | 474   | 1.06   |
| Seagate   | ST3250823A         | 250 GB | 5       | 888   | 527   | 1.06   |
| Seagate   | ST3160215AS        | 160 GB | 14      | 572   | 612   | 1.05   |
| Seagate   | ST3250820ACE       | 250 GB | 3       | 520   | 698   | 1.05   |
| Seagate   | ST1500DL003-9VT16L | 1.5 TB | 25      | 821   | 197   | 1.05   |
| Seagate   | ST2000DM001-9YN164 | 2 TB   | 34      | 706   | 323   | 1.05   |
| Seagate   | ST250DM001 HD253GJ | 250 GB | 6       | 382   | 0     | 1.05   |
| Seagate   | ST1000NM0033-9Z... | 1 TB   | 20      | 391   | 51    | 1.05   |
| Seagate   | ST1500DM003-1CH16G | 1.5 TB | 7       | 416   | 2     | 1.05   |
| Seagate   | ST3500514NS        | 500 GB | 7       | 1229  | 128   | 1.04   |
| Seagate   | ST3250824AS        | 250 GB | 17      | 775   | 747   | 1.04   |
| Seagate   | ST340212AS         | 40 GB  | 2       | 1049  | 105   | 1.04   |
| Seagate   | ST2000NC001-1DY164 | 2 TB   | 5       | 378   | 0     | 1.04   |
| Seagate   | ST3160827AS        | 160 GB | 20      | 1015  | 264   | 1.02   |
| Seagate   | ST9402112A         | 40 GB  | 1       | 1117  | 2     | 1.02   |
| Seagate   | ST1000LM044 HN-... | 1 TB   | 2       | 559   | 404   | 1.02   |
| Seagate   | ST1000NC001-1DY162 | 1 TB   | 3       | 365   | 0     | 1.00   |
| Seagate   | ST3200827AS        | 200 GB | 25      | 757   | 319   | 1.00   |
| Seagate   | ST1000VX000-1CU162 | 1 TB   | 22      | 379   | 46    | 1.00   |
| Seagate   | ST500DM002-1BC142  | 500 GB | 44      | 530   | 136   | 1.00   |
| Seagate   | ST9640423AS        | 640 GB | 4       | 949   | 380   | 0.99   |
| Seagate   | ST3200822AS        | 200 GB | 12      | 751   | 178   | 0.99   |
| Seagate   | ST3120827AS        | 120 GB | 42      | 709   | 11    | 0.98   |
| Seagate   | ST3500830AS        | 500 GB | 5       | 580   | 1     | 0.98   |
| Seagate   | ST3250310NS        | 250 GB | 3       | 737   | 755   | 0.98   |
| Seagate   | ST500VT000-1DK142  | 500 GB | 2       | 354   | 0     | 0.97   |
| Seagate   | ST3250310AS        | 250 GB | 124     | 819   | 219   | 0.96   |
| Seagate   | ST500DM005 HD502HJ | 500 GB | 30      | 438   | 12    | 0.94   |
| Seagate   | ST380811AS         | 80 GB  | 51      | 570   | 549   | 0.94   |
| Seagate   | ST3160811AS        | 160 GB | 76      | 787   | 447   | 0.94   |
| Seagate   | ST3250820A         | 250 GB | 10      | 874   | 535   | 0.93   |
| Seagate   | ST2000VX000-1ES164 | 2 TB   | 5       | 336   | 0     | 0.92   |
| Seagate   | ST3250410AS        | 250 GB | 145     | 807   | 271   | 0.91   |
| Seagate   | ST9250410AS        | 250 GB | 32      | 479   | 185   | 0.91   |
| Seagate   | ST1000DX001-1NS162 | 1 TB   | 4       | 375   | 3     | 0.91   |
| Seagate   | ST32000542AS       | 2 TB   | 18      | 824   | 435   | 0.90   |
| Seagate   | ST2000VN000-1HJ164 | 2 TB   | 2       | 326   | 0     | 0.90   |
| Seagate   | ST1000DM003-1CH162 | 1 TB   | 234     | 426   | 39    | 0.89   |
| Seagate   | ST250DM000-1BC141  | 250 GB | 7       | 410   | 14    | 0.88   |
| Seagate   | ST31000524NS       | 1 TB   | 3       | 767   | 506   | 0.88   |
| Seagate   | ST2000VN000-1H3164 | 2 TB   | 1       | 321   | 0     | 0.88   |
| Seagate   | ST380215A          | 80 GB  | 24      | 467   | 123   | 0.87   |
| Seagate   | ST750LX003-1AC154  | 752 GB | 10      | 371   | 106   | 0.85   |
| Seagate   | ST2000VX002-1AH166 | 2 TB   | 1       | 311   | 0     | 0.85   |
| Seagate   | ST9320328CS        | 320 GB | 5       | 656   | 342   | 0.84   |
| Seagate   | ST31000524AS       | 1 TB   | 144     | 565   | 225   | 0.84   |
| Seagate   | ST1000DM003-1ER162 | 1 TB   | 129     | 302   | 2     | 0.83   |
| Seagate   | ST1500LM006 HN-... | 1.5 TB | 1       | 302   | 0     | 0.83   |
| Seagate   | ST1000DM003-9YN162 | 1 TB   | 83      | 518   | 293   | 0.83   |
| Seagate   | ST320011A          | 20 GB  | 6       | 555   | 5     | 0.82   |
| Seagate   | ST940210AS         | 40 GB  | 1       | 591   | 1     | 0.81   |
| Seagate   | ST31000528AS       | 1 TB   | 142     | 646   | 225   | 0.81   |
| Seagate   | ST3320820SCE       | 320 GB | 1       | 294   | 0     | 0.81   |
| Seagate   | ST3160211AS        | 160 GB | 8       | 743   | 1099  | 0.80   |
| Seagate   | ST9640320AS        | 640 GB | 8       | 489   | 255   | 0.80   |
| Seagate   | ST4000LM024-2AN17V | 4 TB   | 1       | 292   | 0     | 0.80   |
| Seagate   | ST4000DM000-2AE166 | 4 TB   | 1       | 291   | 0     | 0.80   |
| Seagate   | OOS2000G           | 2 TB   | 1       | 289   | 0     | 0.79   |
| Seagate   | ST320014A          | 20 GB  | 5       | 629   | 16    | 0.78   |
| Seagate   | ST3160023AS        | 160 GB | 10      | 695   | 47    | 0.77   |
| Seagate   | ST2000DM001-1ER164 | 2 TB   | 39      | 280   | 0     | 0.77   |
| Seagate   | STM3500418AS       | 500 GB | 18      | 805   | 420   | 0.76   |
| Seagate   | STM3320418AS       | 320 GB | 10      | 644   | 140   | 0.75   |
| Seagate   | ST3000DM001-1CH166 | 3 TB   | 17      | 387   | 252   | 0.75   |
| Seagate   | ST340016A          | 40 GB  | 25      | 567   | 29    | 0.75   |
| Seagate   | ST1000DX001-1CM162 | 1 TB   | 22      | 360   | 63    | 0.74   |
| Seagate   | ST9160411ASG       | 160 GB | 2       | 676   | 4     | 0.73   |
| Seagate   | ST2000DX001-1NS164 | 2 TB   | 3       | 263   | 0     | 0.72   |
| Seagate   | ST3160021A         | 160 GB | 9       | 508   | 677   | 0.72   |
| Seagate   | ST750LM022 HN-M... | 752 GB | 77      | 350   | 31    | 0.72   |
| Seagate   | ST500DM002-1BD142  | 500 GB | 382     | 424   | 100   | 0.71   |
| Seagate   | ST330013A          | 32 GB  | 1       | 259   | 0     | 0.71   |
| Seagate   | ST91208220AS       | 120 GB | 2       | 461   | 507   | 0.71   |
| Seagate   | ST250DM000-1BD141  | 250 GB | 66      | 402   | 121   | 0.71   |
| Seagate   | ST320DM000-1BD14C  | 320 GB | 31      | 393   | 76    | 0.70   |
| Seagate   | ST3120211AS        | 120 GB | 4       | 619   | 15    | 0.69   |
| Seagate   | ST980813AS         | 80 GB  | 1       | 246   | 0     | 0.67   |
| Seagate   | ST4000VM000-2AF166 | 4 TB   | 1       | 240   | 0     | 0.66   |
| Seagate   | ST380819AS         | 80 GB  | 2       | 799   | 1023  | 0.66   |
| Seagate   | ST3120813AS        | 120 GB | 16      | 857   | 717   | 0.65   |
| Seagate   | ST320410A          | 20 GB  | 3       | 1020  | 17    | 0.65   |
| Seagate   | ST250LT003-9YG14C  | 250 GB | 3       | 235   | 0     | 0.65   |
| Seagate   | ST500LM011 HM501II | 500 GB | 2       | 277   | 1     | 0.64   |
| Seagate   | ST2000VX000-1CU164 | 2 TB   | 10      | 320   | 405   | 0.63   |
| Seagate   | ST3160212ACE       | 160 GB | 4       | 512   | 27    | 0.63   |
| Seagate   | ST9160821A         | 160 GB | 3       | 412   | 12    | 0.63   |
| Seagate   | ST9160411AS        | 160 GB | 2       | 474   | 14    | 0.63   |
| Seagate   | ST9750420AS        | 752 GB | 39      | 404   | 91    | 0.62   |
| Seagate   | ST320LM001 HN-M... | 320 GB | 31      | 247   | 1     | 0.62   |
| Seagate   | ST6000VN0041-2E... | 6 TB   | 1       | 224   | 0     | 0.62   |
| Seagate   | ST3000VX000-1ES166 | 3 TB   | 1       | 222   | 0     | 0.61   |
| Seagate   | ST9320423AS        | 320 GB | 30      | 450   | 346   | 0.60   |
| Seagate   | ST3160813AS        | 160 GB | 25      | 822   | 171   | 0.60   |
| Seagate   | ST33000651AS       | 3 TB   | 4       | 219   | 0     | 0.60   |
| Seagate   | ST9160823ASG       | 160 GB | 2       | 400   | 506   | 0.59   |
| Seagate   | ST2000NM0011       | 2 TB   | 3       | 1091  | 19    | 0.58   |
| Seagate   | ST500LM012 HN-M... | 500 GB | 134     | 295   | 50    | 0.57   |
| Seagate   | ST3750528AS        | 752 GB | 37      | 715   | 358   | 0.57   |
| Seagate   | ST1000LM024 HN-... | 1 TB   | 357     | 290   | 57    | 0.57   |
| Seagate   | ST9120822AS        | 120 GB | 36      | 444   | 451   | 0.57   |
| Seagate   | ST500NM0011        | 500 GB | 9       | 750   | 68    | 0.56   |
| Seagate   | ST96812AS          | 64 GB  | 4       | 327   | 213   | 0.55   |
| Seagate   | ST360014A          | 64 GB  | 2       | 757   | 21    | 0.55   |
| Seagate   | ST4000VN000-2AH166 | 4 TB   | 1       | 200   | 0     | 0.55   |
| Seagate   | ST980813ASG        | 80 GB  | 2       | 436   | 6     | 0.55   |
| Seagate   | ST3500312CS        | 500 GB | 12      | 613   | 366   | 0.55   |
| Seagate   | STM3250318AS       | 250 GB | 19      | 478   | 212   | 0.54   |
| Seagate   | ST2000DL001-9VT156 | 2 TB   | 3       | 647   | 673   | 0.54   |
| Seagate   | ST94011A           | 40 GB  | 1       | 194   | 0     | 0.53   |
| Seagate   | ST320414A          | 20 GB  | 1       | 194   | 0     | 0.53   |
| Seagate   | ST1000LM014-1EJ164 | 1 TB   | 36      | 308   | 142   | 0.53   |
| Seagate   | ST9500423AS        | 500 GB | 18      | 242   | 7     | 0.52   |
| Seagate   | ST96812A           | 64 GB  | 3       | 379   | 758   | 0.52   |
| Seagate   | ST9500420AS        | 500 GB | 57      | 547   | 430   | 0.51   |
| Seagate   | ST3160812AS 41N... | 160 GB | 1       | 927   | 4     | 0.51   |
| Seagate   | ST2000NM0055-1V... | 2 TB   | 1       | 184   | 0     | 0.51   |
| Seagate   | ST3000DM008-2DM166 | 3 TB   | 10      | 231   | 69    | 0.50   |
| Seagate   | ST31000523AS       | 1 TB   | 2       | 2094  | 11    | 0.50   |
| Seagate   | ST2000DX001-1CM164 | 2 TB   | 12      | 287   | 301   | 0.50   |
| Seagate   | ST3000VN000-1H4167 | 3 TB   | 1       | 179   | 0     | 0.49   |
| Seagate   | ST3300822AS        | 304 GB | 1       | 176   | 0     | 0.48   |
| Seagate   | ST980210AS         | 80 GB  | 1       | 175   | 0     | 0.48   |
| Seagate   | ST4000DM005-2DP166 | 4 TB   | 5       | 173   | 0     | 0.48   |
| Seagate   | ST9160827AS        | 160 GB | 29      | 476   | 326   | 0.47   |
| Seagate   | ST500LM000-1EJ1... | 500 GB | 4       | 170   | 0     | 0.47   |
| Seagate   | ST1000DM000-9TS15E | 1 TB   | 1       | 168   | 0     | 0.46   |
| Seagate   | ST9500325ASG       | 500 GB | 2       | 432   | 58    | 0.46   |
| Seagate   | ST3250820NS        | 250 GB | 2       | 695   | 1050  | 0.46   |
| Seagate   | ST9100824A         | 100 GB | 1       | 165   | 0     | 0.45   |
| Seagate   | ST2000LM015-2E8174 | 2 TB   | 7       | 165   | 0     | 0.45   |
| Seagate   | ST2000DX002-2DV164 | 2 TB   | 8       | 162   | 0     | 0.45   |
| Seagate   | ST920217AS         | 20 GB  | 1       | 162   | 0     | 0.44   |
| Seagate   | ST2000DM006-2DM164 | 2 TB   | 19      | 161   | 0     | 0.44   |
| Seagate   | ST8000DM004-2CX188 | 8 TB   | 2       | 161   | 0     | 0.44   |
| Seagate   | ST98823AS          | 80 GB  | 8       | 364   | 520   | 0.44   |
| Seagate   | ST380211AS         | 80 GB  | 7       | 618   | 634   | 0.44   |
| Seagate   | STM9120817AS       | 120 GB | 1       | 155   | 0     | 0.43   |
| Seagate   | ST1000DM003-1SB102 | 1 TB   | 23      | 155   | 0     | 0.42   |
| Seagate   | ST9250827AS        | 250 GB | 22      | 443   | 297   | 0.42   |
| Seagate   | ST9100824AS        | 100 GB | 3       | 195   | 2     | 0.42   |
| Seagate   | ST3160815SV        | 160 GB | 5       | 681   | 1240  | 0.41   |
| Seagate   | ST750LM030-1KKG62  | 752 GB | 2       | 146   | 0     | 0.40   |
| Seagate   | ST1000VM002-1SD102 | 1 TB   | 1       | 144   | 0     | 0.40   |
| Seagate   | ST1000VX001-1HH162 | 1 TB   | 5       | 144   | 0     | 0.40   |
| Seagate   | ST3750330NS        | 752 GB | 3       | 560   | 337   | 0.39   |
| Seagate   | ST3000NM0033-9Z... | 3 TB   | 2       | 577   | 9     | 0.39   |
| Seagate   | ST1500DM003-9YN16G | 1.5 TB | 15      | 468   | 456   | 0.39   |
| Seagate   | ST1000DM003-1SB10C | 1 TB   | 40      | 141   | 1     | 0.38   |
| Seagate   | ST320LT022-1AE142  | 320 GB | 1       | 138   | 0     | 0.38   |
| Seagate   | ST9320325AS        | 320 GB | 185     | 431   | 446   | 0.37   |
| Seagate   | ST320LT007-9ZV142  | 320 GB | 18      | 426   | 810   | 0.37   |
| Seagate   | ST3120811AS        | 120 GB | 13      | 517   | 396   | 0.37   |
| Seagate   | ST9750423AS        | 752 GB | 8       | 317   | 253   | 0.37   |
| Seagate   | ST3200822A         | 200 GB | 3       | 1664  | 14    | 0.37   |
| Seagate   | ST9120822A         | 120 GB | 2       | 273   | 53    | 0.36   |
| Seagate   | ST500DM002-9YN14C  | 500 GB | 3       | 464   | 339   | 0.35   |
| Seagate   | ST95005620AS       | 500 GB | 7       | 374   | 299   | 0.35   |
| Seagate   | ST3500830SCE       | 500 GB | 1       | 126   | 0     | 0.35   |
| Seagate   | ST9120821AS        | 120 GB | 5       | 274   | 1101  | 0.35   |
| Seagate   | ST3250312CS        | 250 GB | 11      | 317   | 230   | 0.34   |
| Seagate   | ST500LT015-1DJ142  | 500 GB | 1       | 124   | 0     | 0.34   |
| Seagate   | ST1000VX000-1ES162 | 1 TB   | 15      | 124   | 0     | 0.34   |
| Seagate   | ST9250315AS        | 250 GB | 150     | 470   | 439   | 0.34   |
| Seagate   | ST320DM001 HD322GJ | 320 GB | 4       | 187   | 108   | 0.33   |
| Seagate   | ST9100827AS        | 100 GB | 1       | 838   | 6     | 0.33   |
| Seagate   | ST910021AS         | 100 GB | 6       | 552   | 547   | 0.33   |
| Seagate   | ST960821A          | 64 GB  | 1       | 358   | 2     | 0.33   |
| Seagate   | ST9320310AS        | 320 GB | 6       | 465   | 468   | 0.33   |
| Seagate   | ST340810A          | 40 GB  | 8       | 468   | 38    | 0.33   |
| Seagate   | ST640LM001 HN-M... | 640 GB | 1       | 116   | 0     | 0.32   |
| Seagate   | ST9250315ASG       | 250 GB | 1       | 115   | 0     | 0.32   |
| Seagate   | ST500LT012-1DG142  | 500 GB | 290     | 167   | 61    | 0.31   |
| Seagate   | ST4000DX001-1CE168 | 4 TB   | 5       | 130   | 5     | 0.31   |
| Seagate   | ST3320311CS        | 320 GB | 4       | 194   | 252   | 0.31   |
| Seagate   | ST9100823A         | 96 GB  | 1       | 1007  | 8     | 0.31   |
| Seagate   | ST9500325AS        | 500 GB | 280     | 448   | 596   | 0.30   |
| Seagate   | ST980811AS         | 80 GB  | 15      | 418   | 319   | 0.29   |
| Seagate   | ST9160314AS        | 160 GB | 25      | 292   | 190   | 0.29   |
| Seagate   | ST320LT020-9YG142  | 320 GB | 107     | 333   | 569   | 0.29   |
| Seagate   | ST3500410AS        | 500 GB | 23      | 1001  | 367   | 0.29   |
| Seagate   | ST3500412AS        | 500 GB | 17      | 858   | 589   | 0.29   |
| Seagate   | ST3400620NS        | 400 GB | 1       | 2185  | 21    | 0.27   |
| Seagate   | ST320LT012-1DG14C  | 320 GB | 18      | 196   | 64    | 0.27   |
| Seagate   | ST500LM000-SSHD... | 500 GB | 16      | 179   | 142   | 0.27   |
| Seagate   | ST380023A          | 80 GB  | 1       | 2897  | 29    | 0.26   |
| Seagate   | ST320LM010-1KJ15C  | 320 GB | 3       | 496   | 6     | 0.26   |
| Seagate   | ST9160821AS        | 160 GB | 42      | 453   | 675   | 0.26   |
| Seagate   | ST1000DM010-2DM162 | 1 TB   | 1       | 95    | 0     | 0.26   |
| Seagate   | ST3000DM001-9YN166 | 3 TB   | 13      | 474   | 1318  | 0.26   |
| Seagate   | ST1000UM000-1EK164 | 1 TB   | 1       | 94    | 0     | 0.26   |
| Seagate   | ST9160823AS        | 160 GB | 5       | 1026  | 670   | 0.25   |
| Seagate   | ST3320613AS        | 320 GB | 81      | 791   | 245   | 0.25   |
| Seagate   | ST31000340SV       | 1 TB   | 1       | 1616  | 17    | 0.25   |
| Seagate   | ST340014AS         | 40 GB  | 3       | 848   | 678   | 0.24   |
| Seagate   | ST8000VN0022-2E... | 8 TB   | 1       | 85    | 0     | 0.23   |
| Seagate   | ST1000LM014-SSH... | 1 TB   | 9       | 266   | 179   | 0.23   |
| Seagate   | ST9250410ASG       | 250 GB | 1       | 409   | 4     | 0.22   |
| Seagate   | ST500DM002-1SB10A  | 500 GB | 17      | 84    | 1     | 0.22   |
| Seagate   | ST9160310AS        | 160 GB | 27      | 276   | 202   | 0.22   |
| Seagate   | ST250LM004 HN-M... | 250 GB | 4       | 245   | 9     | 0.22   |
| Seagate   | ST1000DM010-2EP102 | 1 TB   | 73      | 84    | 3     | 0.22   |
| Seagate   | ST500LM000-1EJ162  | 500 GB | 36      | 164   | 52    | 0.21   |
| Seagate   | ST9320320AS        | 320 GB | 18      | 746   | 140   | 0.20   |
| Seagate   | ST9120411ASG       | 120 GB | 1       | 361   | 4     | 0.20   |
| Seagate   | ST9120817AS        | 120 GB | 4       | 433   | 595   | 0.20   |
| Seagate   | ST9160301AS        | 160 GB | 5       | 153   | 403   | 0.19   |
| Seagate   | ST1000LM025 HN-... | 1 TB   | 6       | 69    | 0     | 0.19   |
| Seagate   | ST380021A          | 80 GB  | 8       | 725   | 38    | 0.19   |
| Seagate   | ST2000LM005 HN-... | 2 TB   | 1       | 67    | 0     | 0.19   |
| Seagate   | ST91608220AS       | 160 GB | 1       | 135   | 1     | 0.19   |
| Seagate   | ST3402111A         | 40 GB  | 2       | 117   | 36    | 0.18   |
| Seagate   | ST3120213A         | 120 GB | 9       | 606   | 708   | 0.18   |
| Seagate   | ST4000DM004-2CV104 | 4 TB   | 13      | 93    | 1     | 0.17   |
| Seagate   | ST9402115AS        | 40 GB  | 1       | 60    | 0     | 0.17   |
| Seagate   | ST500LM021-1KJ152  | 500 GB | 36      | 93    | 110   | 0.16   |
| Seagate   | ST9250311CS        | 250 GB | 1       | 2074  | 35    | 0.16   |
| Seagate   | ST1000LM014-1EJ... | 1 TB   | 4       | 145   | 1     | 0.16   |
| Seagate   | ST6000NM0115-1Y... | 6 TB   | 3       | 56    | 0     | 0.15   |
| Seagate   | ST2000LM007-1R8174 | 2 TB   | 13      | 59    | 6     | 0.15   |
| Seagate   | ST3200826A         | 200 GB | 2       | 561   | 8     | 0.15   |
| Seagate   | ST250LT007-9ZV14C  | 250 GB | 1       | 54    | 0     | 0.15   |
| Seagate   | ST1000LM048-2E7172 | 1 TB   | 25      | 66    | 5     | 0.14   |
| Seagate   | ST960812A          | 64 GB  | 1       | 151   | 2     | 0.14   |
| Seagate   | ST96023AS          | 64 GB  | 1       | 554   | 10    | 0.14   |
| Seagate   | ST2000LX001-1RG174 | 2 TB   | 4       | 223   | 253   | 0.14   |
| Seagate   | ST4000NM0035-1V... | 4 TB   | 1       | 48    | 0     | 0.13   |
| Seagate   | ST9160412AS        | 160 GB | 6       | 534   | 447   | 0.13   |
| Seagate   | ST10000DM0004-1... | 10 TB  | 1       | 425   | 8     | 0.13   |
| Seagate   | ST500LM030-2E717D  | 500 GB | 17      | 46    | 0     | 0.13   |
| Seagate   | ST1000LX015-1U7172 | 1 TB   | 11      | 60    | 92    | 0.12   |
| Seagate   | ST31500341AS       | 1.5 TB | 49      | 784   | 301   | 0.12   |
| Seagate   | ST1000DX002-2DV162 | 1 TB   | 7       | 43    | 0     | 0.12   |
| Seagate   | ST3750330AS        | 752 GB | 10      | 783   | 357   | 0.12   |
| Seagate   | ST2000VX000-9YW164 | 2 TB   | 7       | 700   | 892   | 0.12   |
| Seagate   | ST1000LM035-1RK172 | 1 TB   | 56      | 48    | 78    | 0.11   |
| Seagate   | ST1000NM0008-2F... | 1 TB   | 1       | 39    | 0     | 0.11   |
| Seagate   | ST500LM030-1RK17D  | 500 GB | 5       | 37    | 0     | 0.10   |
| Seagate   | ST1000VM002-1ET162 | 1 TB   | 1       | 32    | 0     | 0.09   |
| Seagate   | ST38410A           | 8 GB   | 1       | 754   | 24    | 0.08   |
| Seagate   | ST3500320AS        | 500 GB | 64      | 824   | 511   | 0.08   |
| Seagate   | ST750LM028-1KK162  | 752 GB | 1       | 29    | 0     | 0.08   |
| Seagate   | ST3640323AS        | 640 GB | 7       | 1126  | 533   | 0.08   |
| Seagate   | ST500LM034-2GH17A  | 500 GB | 1       | 28    | 0     | 0.08   |
| Seagate   | ST500DM009-2F110A  | 500 GB | 8       | 46    | 4     | 0.08   |
| Seagate   | ST9250320AS        | 250 GB | 14      | 287   | 254   | 0.07   |
| Seagate   | ST500LM016 HN-M... | 500 GB | 1       | 24    | 0     | 0.07   |
| Seagate   | ST3250310CS        | 250 GB | 1       | 1067  | 42    | 0.07   |
| Seagate   | ST4000VX000-2AG166 | 4 TB   | 1       | 23    | 0     | 0.06   |
| Seagate   | ST2000VM003-1ET164 | 2 TB   | 1       | 21    | 0     | 0.06   |
| Seagate   | ST3300831A         | 304 GB | 1       | 347   | 15    | 0.06   |
| Seagate   | ST3250824SCE       | 250 GB | 1       | 21    | 0     | 0.06   |
| Seagate   | ST1000LM010-9YH146 | 1 TB   | 7       | 110   | 1022  | 0.06   |
| Seagate   | ST3160215SCE       | 160 GB | 1       | 98    | 4     | 0.05   |
| Seagate   | ST9250610NS        | 250 GB | 2       | 19    | 0     | 0.05   |
| Seagate   | ST31000340AS       | 1 TB   | 6       | 1025  | 364   | 0.05   |
| Seagate   | ST31000340NS       | 1 TB   | 9       | 1183  | 456   | 0.05   |
| Seagate   | ST3320310CS        | 320 GB | 3       | 576   | 32    | 0.05   |
| Seagate   | ST500VM000-1SD101  | 500 GB | 1       | 15    | 0     | 0.04   |
| Seagate   | ST1000LM049-2GH172 | 1 TB   | 13      | 15    | 0     | 0.04   |
| Seagate   | OOS500G            | 500 GB | 1       | 14    | 0     | 0.04   |
| Seagate   | ST9120310AS        | 120 GB | 1       | 14    | 0     | 0.04   |
| Seagate   | ST91000640NS       | 1 TB   | 1       | 13    | 0     | 0.04   |
| Seagate   | ST310212A          | 10 GB  | 1       | 380   | 27    | 0.04   |
| Seagate   | ST3250412CS        | 250 GB | 1       | 12    | 0     | 0.03   |
| Seagate   | ST31000333AS       | 1 TB   | 18      | 688   | 490   | 0.03   |
| Seagate   | STM31000528AS      | 1 TB   | 2       | 10    | 0     | 0.03   |
| Seagate   | ST9808210A         | 80 GB  | 1       | 344   | 37    | 0.02   |
| Seagate   | ST9320421AS        | 320 GB | 2       | 709   | 98    | 0.02   |
| Seagate   | ST500LT012-9WS142  | 500 GB | 148     | 289   | 1071  | 0.02   |
| Seagate   | ST3500320SV        | 500 GB | 1       | 45    | 7     | 0.02   |
| Seagate   | ST3000VX010-2E3166 | 3 TB   | 1       | 5     | 0     | 0.01   |
| Seagate   | ST1000LM002-9VQ14L | 1 TB   | 1       | 25    | 4     | 0.01   |
| Seagate   | ST320LT012-9WS14C  | 320 GB | 42      | 214   | 975   | 0.01   |
| Seagate   | ST2000DM009-2G4100 | 2 TB   | 2       | 3     | 0     | 0.01   |
| Seagate   | ST2000DM008-2FR102 | 2 TB   | 4       | 3     | 0     | 0.01   |
| Seagate   | ST3250310SV        | 250 GB | 1       | 287   | 79    | 0.01   |
| Seagate   | ST3500620AS        | 500 GB | 2       | 429   | 996   | 0.01   |
| Seagate   | ST3320820A         | 320 GB | 1       | 641   | 210   | 0.01   |
| Seagate   | ST3500820AS        | 500 GB | 3       | 187   | 61    | 0.01   |
| Seagate   | ST500LM020-1G1162  | 500 GB | 3       | 2     | 0     | 0.01   |
| Seagate   | ST1000VX005-2EZ102 | 1 TB   | 1       | 2     | 0     | 0.01   |
| Seagate   | ST3160212SCE       | 160 GB | 1       | 2510  | 1028  | 0.01   |
| Seagate   | ST9320423ASG       | 320 GB | 1       | 270   | 145   | 0.01   |
| Seagate   | ST3750630AS        | 752 GB | 1       | 1122  | 686   | 0.00   |
| Seagate   | ST3120023AS        | 120 GB | 1       | 152   | 101   | 0.00   |
| Seagate   | ST500VT000-1BS142  | 500 GB | 1       | 1     | 0     | 0.00   |
| Seagate   | ST3250824A         | 250 GB | 1       | 978   | 1007  | 0.00   |
| Seagate   | ST1000VN002-2EY102 | 1 TB   | 1       | 0     | 0     | 0.00   |
| Seagate   | ST3320620SV        | 320 GB | 1       | 564   | 835   | 0.00   |
| Seagate   | ST500DM009-2DM14C  | 500 GB | 1       | 0     | 0     | 0.00   |
| Seagate   | ST3500321CS        | 500 GB | 1       | 48    | 137   | 0.00   |
| Seagate   | ST250LT012-9WS141  | 250 GB | 4       | 337   | 1077  | 0.00   |
| Seagate   | ST980817AS         | 80 GB  | 1       | 310   | 1009  | 0.00   |
| Seagate   | ST9500424AS        | 500 GB | 1       | 200   | 1009  | 0.00   |
| Seagate   | ST3160316CS        | 160 GB | 1       | 0     | 0     | 0.00   |
| Seagate   | ST980829A          | 80 GB  | 1       | 222   | 2022  | 0.00   |
| Seagate   | ST9320312AS        | 320 GB | 1       | 69    | 1193  | 0.00   |
| Seagate   | ST3000VX010-2H916L | 3 TB   | 2       | 0     | 0     | 0.00   |
| Seagate   | ST380020A          | 80 GB  | 1       | 2     | 225   | 0.00   |

HDD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | Rating |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Seagate   | Barracuda ES+          | 1      | 1       | 2169  | 0     | 5.94   |
| Seagate   | Constellation.2        | 2      | 2       | 983   | 0     | 2.69   |
| Seagate   | U5                     | 2      | 2       | 875   | 0     | 2.40   |
| Seagate   | Enterprise NAS HDD     | 1      | 1       | 836   | 0     | 2.29   |
| Seagate   | NAS HDD                | 7      | 22      | 802   | 92    | 2.02   |
| Seagate   | Archive HDD            | 1      | 1       | 718   | 0     | 1.97   |
| Seagate   | Barracuda 7200.8       | 8      | 33      | 1086  | 118   | 1.88   |
| Seagate   | Desktop HDD.15         | 4      | 24      | 648   | 1     | 1.75   |
| Seagate   | Barracuda ES           | 6      | 32      | 1057  | 400   | 1.72   |
| Seagate   | SV35.5                 | 1      | 2       | 938   | 2     | 1.56   |
| Seagate   | Constellation ES.2 ... | 1      | 4       | 553   | 0     | 1.52   |
| Seagate   | Barracuda Green (AF)   | 4      | 112     | 830   | 195   | 1.48   |
| Seagate   | Barracuda XT           | 2      | 13      | 636   | 124   | 1.41   |
| Seagate   | Video 3.5 HDD          | 8      | 17      | 547   | 1     | 1.40   |
| Seagate   | SpinPoint M7E          | 2      | 11      | 518   | 2     | 1.37   |
| Seagate   | U9                     | 2      | 5       | 490   | 0     | 1.34   |
| Seagate   | Barracuda 7200.10      | 29     | 955     | 830   | 280   | 1.33   |
| Seagate   | Barracuda 5400.1       | 1      | 5       | 835   | 33    | 1.32   |
| Seagate   | Constellation ES.2     | 1      | 1       | 467   | 0     | 1.28   |
| Seagate   | Barracuda 7200.7 an... | 18     | 473     | 818   | 98    | 1.26   |
| Seagate   | DB35.3                 | 6      | 11      | 584   | 211   | 1.26   |
| Seagate   | Constellation ES (S... | 3      | 12      | 1028  | 201   | 1.15   |
| Seagate   | Momentus               | 4      | 8       | 710   | 190   | 1.11   |
| Seagate   | Constellation ES.3     | 4      | 30      | 432   | 35    | 1.09   |
| Seagate   | SpinPoint M9T          | 2      | 18      | 425   | 3     | 1.08   |
| Seagate   | Barracuda 7200.12      | 14     | 968     | 696   | 171   | 1.06   |
| Seagate   | Barracuda 7200.9       | 29     | 399     | 746   | 437   | 1.06   |
| Seagate   | Barracuda 7200.7       | 1      | 2       | 1049  | 105   | 1.04   |
| Seagate   | Constellation CS       | 2      | 8       | 373   | 0     | 1.02   |
| Seagate   | SpinPoint D8X          | 1      | 2       | 559   | 404   | 1.02   |
| Seagate   | Barracuda              | 2      | 7       | 369   | 0     | 1.01   |
| Seagate   | Barracuda SpinPoint F3 | 2      | 41      | 462   | 9     | 1.01   |
| Seagate   | Pipeline HD Mini       | 3      | 7       | 877   | 249   | 0.93   |
| Seagate   | Barracuda LP           | 4      | 55      | 883   | 435   | 0.89   |
| Seagate   | Barracuda ES.2         | 4      | 28      | 946   | 465   | 0.86   |
| Seagate   | Momentus XT (AF)       | 1      | 10      | 371   | 106   | 0.85   |
| Seagate   | Barracuda ATA V        | 3      | 4       | 1510  | 35    | 0.82   |
| Seagate   | SV35                   | 12     | 74      | 385   | 169   | 0.79   |
| Seagate   | Barracuda 7200.14 (AF) | 23     | 1307    | 412   | 108   | 0.79   |
| Seagate   | Constellation ES (S... | 3      | 18      | 792   | 52    | 0.79   |
| Seagate   | UX                     | 1      | 5       | 629   | 16    | 0.78   |
| Seagate   | Barracuda ATA IV       | 4      | 42      | 617   | 25    | 0.77   |
| Seagate   | Surveillance           | 5      | 10      | 260   | 0     | 0.71   |
| Seagate   | Barracuda V            | 1      | 1       | 259   | 0     | 0.71   |
| Seagate   | Pipeline HD 5900.2     | 6      | 37      | 472   | 245   | 0.71   |
| Seagate   | Momentus 7200.3        | 5      | 9       | 638   | 28    | 0.70   |
| Seagate   | Video 2.5              | 2      | 3       | 236   | 0     | 0.65   |
| Seagate   | LD25.2                 | 2      | 2       | 383   | 1     | 0.65   |
| Seagate   | Momentus 5400.2        | 13     | 38      | 426   | 400   | 0.64   |
| Seagate   | Maxtor DiamondMax 23   | 4      | 49      | 613   | 265   | 0.64   |
| Seagate   | Desktop SSHD           | 5      | 46      | 311   | 109   | 0.64   |
| Seagate   | Momentus 7200.2        | 5      | 12      | 688   | 364   | 0.63   |
| Seagate   | Momentus 7200.4        | 7      | 129     | 504   | 338   | 0.62   |
| Seagate   | Momentus 7200.5        | 4      | 60      | 359   | 78    | 0.62   |
| Seagate   | SpinPoint M8 (AF)      | 6      | 604     | 296   | 49    | 0.59   |
| Seagate   | Momentus 5400 PSD      | 2      | 3       | 352   | 338   | 0.53   |
| Seagate   | Barracuda ATA III      | 1      | 1       | 194   | 0     | 0.53   |
| Seagate   | Momentus 5400.3        | 7      | 100     | 454   | 495   | 0.45   |
| Seagate   | Barracuda Compute      | 1      | 2       | 161   | 0     | 0.44   |
| Seagate   | Momentus 5400.4        | 3      | 55      | 460   | 334   | 0.43   |
| Seagate   | MobileMax-2            | 1      | 1       | 155   | 0     | 0.43   |
| Seagate   | U6                     | 3      | 12      | 567   | 48    | 0.38   |
| Seagate   | Momentus XT            | 1      | 7       | 374   | 299   | 0.35   |
| Seagate   | Laptop HDD             | 1      | 1       | 124   | 0     | 0.34   |
| Seagate   | SV35.2                 | 2      | 6       | 662   | 1173  | 0.34   |
| Seagate   | Momentus 5400.6        | 10     | 658     | 440   | 493   | 0.34   |
| Seagate   | SpinPoint F4           | 1      | 4       | 187   | 108   | 0.33   |
| Seagate   | Laptop SSHD            | 7      | 108     | 218   | 101   | 0.33   |
| Seagate   | Momentus Thin          | 5      | 130     | 347   | 580   | 0.33   |
| Seagate   | Momentus 5400.7 (AF)   | 2      | 9       | 289   | 358   | 0.33   |
| Seagate   | Momentus 5400.7        | 1      | 6       | 465   | 468   | 0.33   |
| Seagate   | LD25 Series            | 2      | 2       | 111   | 0     | 0.30   |
| Seagate   | Momentus 7200.1        | 2      | 7       | 552   | 471   | 0.30   |
| Seagate   | Momentus 4200.2        | 4      | 4       | 458   | 516   | 0.30   |
| Seagate   | Ultra Mobile HDD       | 2      | 3       | 107   | 0     | 0.30   |
| Seagate   | FireCuda 3.5           | 2      | 15      | 107   | 0     | 0.29   |
| Seagate   | IronWolf               | 3      | 3       | 103   | 0     | 0.28   |
| Seagate   | Barracuda 3.5          | 8      | 130     | 108   | 7     | 0.27   |
| Seagate   | Laptop Thin SSHD       | 1      | 1       | 94    | 0     | 0.26   |
| Seagate   | Enterprise Capacity... | 4      | 6       | 73    | 0     | 0.20   |
| Seagate   | Barracuda 2.5 5400     | 6      | 56      | 80    | 2     | 0.20   |
| Seagate   | Laptop Thin HDD        | 7      | 541     | 203   | 419   | 0.19   |
| Seagate   | SpinPoint M8U (USB)    | 1      | 6       | 69    | 0     | 0.19   |
| Seagate   | SpinPoint M9TU (USB)   | 1      | 1       | 67    | 0     | 0.19   |
| Seagate   | Barracuda 7200.11      | 11     | 266     | 799   | 349   | 0.19   |
| Seagate   | Momentus 5400.5        | 4      | 60      | 415   | 192   | 0.18   |
| Seagate   | SV35.3                 | 2      | 2       | 831   | 12    | 0.13   |
| Seagate   | FireCuda 2.5           | 2      | 15      | 103   | 135   | 0.13   |
| Seagate   | Mobile HDD             | 2      | 69      | 50    | 65    | 0.12   |
| Seagate   | U8                     | 1      | 1       | 754   | 24    | 0.08   |
| Seagate   | Mobile USB Momentus    | 1      | 1       | 24    | 0     | 0.07   |
| Seagate   | DB35.4                 | 1      | 1       | 1067  | 42    | 0.07   |
| Seagate   | FreePlay               | 2      | 8       | 99    | 895   | 0.05   |
| Seagate   | BarraCuda Pro          | 3      | 15      | 43    | 1     | 0.05   |
| Seagate   | Constellation.2 (SATA) | 2      | 3       | 17    | 0     | 0.05   |
| Seagate   | Video 3.5              | 1      | 1       | 15    | 0     | 0.04   |
| Seagate   | Unknown                | 1      | 1       | 14    | 0     | 0.04   |
| Seagate   | U10                    | 1      | 1       | 380   | 27    | 0.04   |
| Seagate   | Pipeline HD 5900.1     | 2      | 4       | 444   | 58    | 0.03   |
| Seagate   | DB35.2                 | 2      | 2       | 1265  | 514   | 0.03   |
| Seagate   | Barracuda Pro          | 1      | 2       | 3     | 0     | 0.01   |
| Seagate   | BarraCuda              | 1      | 4       | 3     | 0     | 0.01   |
| Seagate   | SkyHawk                | 3      | 4       | 1     | 0     | 0.01   |

Team Solid State Drives
=======================

This is a list of all tested Team solid state drive models and their ratings. See
more info on reliability test in the [README](https://github.com/linuxhw/SMART).

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | Rating |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Team      | L3 SSD             | 120 GB | 2       | 221   | 0     | 0.61   |
| Team      | L3 EVO SSD         | 120 GB | 2       | 139   | 0     | 0.38   |
| Team      | L5 LITE SSD        | 120 GB | 2       | 110   | 0     | 0.30   |
| Team      | L5 LITE SSD        | 64 GB  | 1       | 52    | 0     | 0.14   |
| Team      | L7 EVO SSD         | 64 GB  | 1       | 51    | 0     | 0.14   |
| Team      | TEAML5Lite3D120G   | 120 GB | 1       | 27    | 0     | 0.07   |
| Team      | T2535T120G         | 120 GB | 1       | 11    | 0     | 0.03   |
| Team      | M8PS5 SSD          | 128 GB | 1       | 1     | 0     | 0.00   |

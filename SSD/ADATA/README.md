ADATA Solid State Drives
========================

This is a list of all tested ADATA solid state drive models and their ratings. See
more info on reliability test in the [README](https://github.com/linuxhw/SMART).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | Rating |
|-----------|--------------------|--------|---------|-------|-------|--------|
| ADATA     | SSD S510           | 120 GB | 4       | 824   | 0     | 2.26   |
| ADATA     | SP600              | 256 GB | 2       | 477   | 0     | 1.31   |
| ADATA     | SSD S396           | 32 GB  | 1       | 462   | 0     | 1.27   |
| ADATA     | SSD S511           | 120 GB | 1       | 430   | 0     | 1.18   |
| ADATA     | SP900              | 256 GB | 8       | 371   | 2     | 0.75   |
| ADATA     | SX900              | 64 GB  | 3       | 500   | 339   | 0.68   |
| ADATA     | SP900              | 128 GB | 22      | 319   | 16    | 0.68   |
| ADATA     | SP600              | 128 GB | 3       | 203   | 0     | 0.56   |
| ADATA     | SP900              | 64 GB  | 12      | 249   | 2     | 0.51   |
| ADATA     | SX900              | 128 GB | 7       | 705   | 584   | 0.49   |
| ADATA     | SSD S599           | 64 GB  | 1       | 178   | 0     | 0.49   |
| ADATA     | SX930              | 120 GB | 1       | 138   | 0     | 0.38   |
| ADATA     | SP600              | 32 GB  | 4       | 127   | 0     | 0.35   |
| ADATA     | SSD S511           | 64 GB  | 3       | 450   | 679   | 0.33   |
| ADATA     | SP310              | 128 GB | 1       | 120   | 0     | 0.33   |
| ADATA     | SSD SX900 512GB... | 512 GB | 1       | 120   | 0     | 0.33   |
| ADATA     | XM13               | 32 GB  | 1       | 116   | 0     | 0.32   |
| ADATA     | SP920SS            | 128 GB | 4       | 306   | 4     | 0.30   |
| ADATA     | SP600              | 64 GB  | 7       | 113   | 2     | 0.28   |
| ADATA     | SU650              | 480 GB | 1       | 77    | 0     | 0.21   |
| ADATA     | SP580              | 120 GB | 6       | 74    | 0     | 0.20   |
| ADATA     | S596               | 128 GB | 1       | 68    | 0     | 0.19   |
| ADATA     | SP900              | 512 GB | 2       | 66    | 0     | 0.18   |
| ADATA     | SP610              | 128 GB | 2       | 57    | 0     | 0.16   |
| ADATA     | SP900NS38          | 128 GB | 3       | 124   | 339   | 0.16   |
| ADATA     | SU700              | 120 GB | 3       | 44    | 0     | 0.12   |
| ADATA     | SP920SS            | 256 GB | 4       | 138   | 8     | 0.11   |
| ADATA     | SP550              | 240 GB | 6       | 39    | 0     | 0.11   |
| ADATA     | SX950              | 240 GB | 1       | 34    | 0     | 0.09   |
| ADATA     | SP900NS34          | 128 GB | 1       | 30    | 0     | 0.08   |
| ADATA     | SU800NS38          | 256 GB | 1       | 29    | 0     | 0.08   |
| ADATA     | SU800              | 128 GB | 18      | 30    | 5     | 0.08   |
| ADATA     | SU650              | 120 GB | 7       | 34    | 2     | 0.07   |
| ADATA     | SU900              | 256 GB | 4       | 21    | 1     | 0.06   |
| ADATA     | SP610              | 256 GB | 1       | 18    | 0     | 0.05   |
| ADATA     | SU800              | 1 TB   | 1       | 13    | 0     | 0.04   |
| ADATA     | IM2S3138E-128GM-B  | 128 GB | 1       | 11    | 0     | 0.03   |
| ADATA     | SP550              | 120 GB | 18      | 12    | 1     | 0.03   |
| ADATA     | SU650              | 240 GB | 6       | 10    | 0     | 0.03   |
| ADATA     | SX300              | 128 GB | 1       | 10    | 0     | 0.03   |
| ADATA     | SU900              | 128 GB | 1       | 9     | 0     | 0.02   |
| ADATA     | SU800              | 256 GB | 2       | 37    | 6     | 0.02   |
| ADATA     | SU655              | 120 GB | 1       | 1     | 0     | 0.00   |
| ADATA     | SP900NS38          | 256 GB | 1       | 942   | 1023  | 0.00   |
| ADATA     | AXM13S2-24GM-B     | 24 GB  | 1       | 628   | 1020  | 0.00   |
| ADATA     | SX900              | 256 GB | 2       | 580   | 1114  | 0.00   |
| ADATA     | SU800              | 512 GB | 1       | 0     | 0     | 0.00   |
| ADATA     | XM11 128GB-V3      | 128 GB | 1       | 77    | 1016  | 0.00   |
| ADATA     | SX300              | 64 GB  | 1       | 32    | 1020  | 0.00   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | Rating |
|-----------|------------------------|--------|---------|-------|-------|--------|
| ADATA     | SandForce Driven SSDs  | 8      | 51      | 336   | 8     | 0.76   |
| ADATA     | JMicron based SSDs     | 5      | 17      | 176   | 1     | 0.47   |
| ADATA     | Unknown                | 34     | 93      | 166   | 158   | 0.17   |
| ADATA     | SiliconMotion based... | 2      | 24      | 18    | 1     | 0.05   |

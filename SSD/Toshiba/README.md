Toshiba Solid State Drives
==========================

This is a list of all tested Toshiba solid state drive models and their ratings. See
more info on reliability test in the [README](https://github.com/linuxhw/SMART).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | Rating |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Toshiba   | THNS064GG2BNAA     | 64 GB  | 2       | 1215  | 0     | 3.33   |
| Toshiba   | THNSNH060GMCT      | 64 GB  | 1       | 787   | 0     | 2.16   |
| Toshiba   | THNSNJ120PCSZ      | 120 GB | 2       | 758   | 0     | 2.08   |
| Toshiba   | THNSNH256GMCT      | 256 GB | 1       | 612   | 0     | 1.68   |
| Toshiba   | THNSNH128GCST      | 128 GB | 1       | 549   | 0     | 1.51   |
| Toshiba   | THNSFC256GAMJ      | 256 GB | 1       | 539   | 0     | 1.48   |
| Toshiba   | Q300 Pro           | 256 GB | 2       | 478   | 0     | 1.31   |
| Toshiba   | THNSNF128GMCS      | 128 GB | 2       | 471   | 0     | 1.29   |
| Toshiba   | THNSNH060GCST      | 64 GB  | 1       | 467   | 0     | 1.28   |
| Toshiba   | Q300 Pro           | 512 GB | 1       | 411   | 0     | 1.13   |
| Toshiba   | THNSNH060GBST      | 64 GB  | 2       | 400   | 0     | 1.10   |
| Toshiba   | THNSFJ256GDNU A    | 256 GB | 1       | 390   | 0     | 1.07   |
| Toshiba   | THNSNC128GCSJ      | 128 GB | 1       | 367   | 0     | 1.01   |
| Toshiba   | THNSNX024GMNT      | 24 GB  | 2       | 346   | 0     | 0.95   |
| Toshiba   | THNS064GE4BBDC     | 64 GB  | 1       | 338   | 0     | 0.93   |
| Toshiba   | THNSNC128GBSJ      | 128 GB | 1       | 303   | 0     | 0.83   |
| Toshiba   | THNSNJ128G8NU      | 128 GB | 2       | 274   | 0     | 0.75   |
| Toshiba   | TR150              | 120 GB | 3       | 263   | 0     | 0.72   |
| Toshiba   | A100               | 240 GB | 2       | 230   | 0     | 0.63   |
| Toshiba   | THNSNJ128GCST      | 128 GB | 3       | 208   | 0     | 0.57   |
| Toshiba   | THNSNH128GBST      | 128 GB | 2       | 207   | 0     | 0.57   |
| Toshiba   | THNSNJ128GCSU      | 128 GB | 3       | 202   | 0     | 0.56   |
| Toshiba   | THNSNJ256GCSU      | 256 GB | 3       | 188   | 0     | 0.52   |
| Toshiba   | THNSNH128GMCT      | 128 GB | 2       | 186   | 0     | 0.51   |
| Toshiba   | VX500              | 256 GB | 1       | 134   | 0     | 0.37   |
| Toshiba   | THNSNF064GMCS      | 64 GB  | 2       | 129   | 0     | 0.35   |
| Toshiba   | Q300               | 120 GB | 2       | 112   | 0     | 0.31   |
| Toshiba   | TR200              | 480 GB | 2       | 104   | 0     | 0.29   |
| Toshiba   | THNSNH256GBST      | 256 GB | 1       | 102   | 0     | 0.28   |
| Toshiba   | THNSNJ128G8NY      | 128 GB | 1       | 98    | 0     | 0.27   |
| Toshiba   | THNSNJ512GDNU A    | 512 GB | 1       | 96    | 0     | 0.26   |
| Toshiba   | Q300 Pro           | 128 GB | 1       | 79    | 0     | 0.22   |
| Toshiba   | TR150              | 480 GB | 2       | 70    | 0     | 0.19   |
| Toshiba   | TR150              | 240 GB | 7       | 67    | 0     | 0.19   |
| Toshiba   | Q200 EX            | 240 GB | 1       | 61    | 0     | 0.17   |
| Toshiba   | THNSNK128GVN8      | 128 GB | 2       | 58    | 0     | 0.16   |
| Toshiba   | THNSNB062GMCJ      | 64 GB  | 1       | 32    | 0     | 0.09   |
| Toshiba   | VT180              | 480 GB | 1       | 30    | 0     | 0.08   |
| Toshiba   | THNSNF256GMCS      | 256 GB | 1       | 28    | 0     | 0.08   |
| Toshiba   | TR200              | 240 GB | 9       | 27    | 0     | 0.08   |
| Toshiba   | THNSNH128G8NT      | 128 GB | 1       | 25    | 0     | 0.07   |
| Toshiba   | Q300               | 480 GB | 1       | 24    | 0     | 0.07   |
| Toshiba   | THNSNS060GBSP      | 64 GB  | 1       | 22    | 0     | 0.06   |
| Toshiba   | TL100              | 240 GB | 2       | 9     | 0     | 0.03   |
| Toshiba   | THNSNS128GMCP      | 128 GB | 1       | 7     | 0     | 0.02   |
| Toshiba   | A100               | 120 GB | 2       | 6     | 0     | 0.02   |
| Toshiba   | Q300               | 240 GB | 1       | 32    | 8     | 0.01   |
| Toshiba   | KSG60ZSE512G SATA  | 512 GB | 2       | 97    | 100   | 0.00   |
| Toshiba   | THNSNK256GVN8 M... | 256 GB | 2       | 49    | 100   | 0.00   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | Rating |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Toshiba   | HG2 Series             | 2      | 3       | 922   | 0     | 2.53   |
| Toshiba   | HG5 Series             | 1      | 2       | 471   | 0     | 1.29   |
| Toshiba   | HG3 Series             | 3      | 3       | 403   | 0     | 1.11   |
| Toshiba   | HG5d Series            | 7      | 10      | 273   | 0     | 0.75   |
| Toshiba   | HG6 Series SSD         | 6      | 13      | 218   | 0     | 0.60   |
| Toshiba   | Unknown                | 22     | 41      | 172   | 10    | 0.45   |
| Toshiba   | OCZ                    | 5      | 14      | 103   | 0     | 0.28   |
| Toshiba   | HG6 Series             | 1      | 1       | 98    | 0     | 0.27   |
| Toshiba   | OCZ/Toshiba Trion SSDs | 1      | 2       | 70    | 0     | 0.19   |
| Toshiba   | SG2 Series             | 1      | 1       | 32    | 0     | 0.09   |

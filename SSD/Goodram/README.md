Goodram Solid State Drives
==========================

This is a list of all tested Goodram solid state drive models and their ratings. See
more info on reliability test in the [README](https://github.com/linuxhw/SMART).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | Rating |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Goodram   | C50                | 64 GB  | 1       | 313   | 0     | 0.86   |
| Goodram   | GOODRAM SSD        | 120 GB | 2       | 269   | 0     | 0.74   |
| Goodram   | GOODRAM C40        | 120 GB | 3       | 266   | 0     | 0.73   |
| Goodram   | GOODRAM C100       | 120 GB | 1       | 214   | 0     | 0.59   |
| Goodram   | GOODRAM C50        | 64 GB  | 1       | 204   | 0     | 0.56   |
| Goodram   | GOODRAM CX100      | 120 GB | 6       | 152   | 2     | 0.40   |
| Goodram   | GOODRAM            | 120 GB | 17      | 126   | 0     | 0.35   |
| Goodram   | GOODRAM IR-SSDP... | 120 GB | 2       | 59    | 0     | 0.16   |
| Goodram   | GOODRAM IR-SSDP... | 240 GB | 1       | 44    | 0     | 0.12   |
| Goodram   | GOODRAM SSDPR-C... | 128 GB | 2       | 14    | 0     | 0.04   |
| Goodram   | GOODRAM SSDPR-C... | 120 GB | 1       | 11    | 0     | 0.03   |
| Goodram   | GOODRAM SSDPR_C... | 120 GB | 1       | 6     | 0     | 0.02   |
| Goodram   | GOODRAM IR_SSDP... | 120 GB | 1       | 2     | 0     | 0.01   |
| Goodram   | GOODRAM SSDPR-C... | 240 GB | 1       | 2     | 0     | 0.01   |
| Goodram   | GOODRAM SSDPR-C... | 120 GB | 1       | 1     | 0     | 0.00   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | Rating |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Goodram   | Unknown                | 14     | 24      | 133   | 1     | 0.36   |
| Goodram   | Phison Driven SSDs     | 1      | 17      | 126   | 0     | 0.35   |

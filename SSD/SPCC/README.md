SPCC Solid State Drives
=======================

This is a list of all tested SPCC solid state drive models and their ratings. See
more info on reliability test in the [README](https://github.com/linuxhw/SMART).

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | Rating |
|-----------|--------------------|--------|---------|-------|-------|--------|
| SPCC      | SSD110             | 64 GB  | 4       | 482   | 0     | 1.32   |
| SPCC      | SSD110             | 120 GB | 4       | 459   | 254   | 0.96   |
| SPCC      | SSD B29            | 32 GB  | 1       | 204   | 0     | 0.56   |
| SPCC      | SSD162             | 120 GB | 7       | 406   | 436   | 0.51   |
| SPCC      | SSD                | 128 GB | 4       | 179   | 0     | 0.49   |
| SPCC      | SSD                | 64 GB  | 63      | 169   | 49    | 0.43   |
| SPCC      | SSD                | 120 GB | 90      | 182   | 114   | 0.40   |
| SPCC      | SSD A20            | 64 GB  | 1       | 97    | 0     | 0.27   |
| SPCC      | SSD170             | 120 GB | 3       | 245   | 341   | 0.26   |
| SPCC      | SSD                | 55 GB  | 7       | 236   | 436   | 0.20   |
| SPCC      | SSD                | 240 GB | 26      | 156   | 340   | 0.18   |
| SPCC      | M.2 SSD            | 120 GB | 3       | 107   | 2     | 0.13   |
| SPCC      | SSD                | 256 GB | 4       | 72    | 26    | 0.09   |
| SPCC      | SSD                | 480 GB | 1       | 15    | 0     | 0.04   |
| SPCC      | SSD162             | 240 GB | 1       | 203   | 20    | 0.03   |
| SPCC      | SSD                | 512 GB | 2       | 8     | 0     | 0.02   |
| SPCC      | SSDB27             | 32 GB  | 1       | 23    | 6     | 0.01   |
| SPCC      | SSD                | 32 GB  | 1       | 919   | 1029  | 0.00   |
| SPCC      | SSD170             | 55 GB  | 2       | 837   | 1016  | 0.00   |
| SPCC      | SSD170             | 64 GB  | 1       | 358   | 1016  | 0.00   |

Patriot Solid State Drives
==========================

This is a list of all tested Patriot solid state drive models and their ratings. See
more info on reliability test in the [README](https://github.com/linuxhw/SMART).

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both rating
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
Rating — avg. rating per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | Rating |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Patriot   | Torch LE           | 240 GB | 1       | 558   | 0     | 1.53   |
| Patriot   | Blaze              | 240 GB | 2       | 231   | 0     | 0.63   |
| Patriot   | Spark              | 128 GB | 6       | 181   | 0     | 0.50   |
| Patriot   | Flare              | 64 GB  | 1       | 157   | 0     | 0.43   |
| Patriot   | Blast              | 120 GB | 4       | 152   | 0     | 0.42   |
| Patriot   | Blast              | 240 GB | 4       | 143   | 0     | 0.39   |
| Patriot   | Blaze              | 64 GB  | 4       | 94    | 0     | 0.26   |
| Patriot   | Burst              | 480 GB | 3       | 59    | 0     | 0.16   |
| Patriot   | Burst              | 240 GB | 3       | 34    | 0     | 0.10   |
| Patriot   | Spark              | 256 GB | 2       | 28    | 0     | 0.08   |
| Patriot   | Blaze              | 120 GB | 3       | 9     | 0     | 0.03   |
| Patriot   | Ignite             | 240 GB | 1       | 6     | 0     | 0.02   |
| Patriot   | Burst              | 120 GB | 6       | 6     | 0     | 0.02   |
| Patriot   | Torch LE           | 120 GB | 1       | 0     | 0     | 0.00   |
| Patriot   | Pyro SE            | 240 GB | 1       | 180   | 1016  | 0.00   |
